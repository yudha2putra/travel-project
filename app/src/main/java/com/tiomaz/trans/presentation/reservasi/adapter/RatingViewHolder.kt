package com.tiomaz.trans.presentation.reservasi.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.domain.entities.LoketEntity
import com.tiomaz.trans.domain.entities.PesananEntity
import kotlinx.android.synthetic.main.list_item_loket.view.*
import kotlinx.android.synthetic.main.list_item_pesanan.view.*
import kotlinx.android.synthetic.main.list_item_pesanan.view.tvTanggal
import kotlinx.android.synthetic.main.list_item_rating.view.*

class RatingViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(
        pesanan: PesananEntity) {
        itemView.tvTanggal.text = pesanan.tanggal
        itemView.tvDari.text = pesanan.dari
        itemView.tvKe.text = pesanan.tujuan
    }
}