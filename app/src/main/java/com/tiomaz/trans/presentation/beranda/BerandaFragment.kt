package com.tiomaz.trans.presentation.beranda

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseFragment
import com.tiomaz.trans.common.CustomInfinitePagerAdapter
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.domain.entities.ArtikelEntity
import com.tiomaz.trans.domain.entities.BannerEntity
import com.tiomaz.trans.presentation.adapter.BannerPagerAdapter
import com.tiomaz.trans.presentation.adapter.IndicatorAdapter
import com.tiomaz.trans.presentation.nearby.LoketActivity
import com.tiomaz.trans.presentation.order.OrderActivity
import com.tiomaz.trans.presentation.rating.RatingActivity
import com.tiomaz.trans.presentation.reservasi.PesanTiketActivity
import com.tiomaz.trans.presentation.reservasi.adapter.ArtikelAdapter
import com.tiomaz.trans.presentation.verification.LoginActivity
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import com.tiomaz.trans.utils.extentions.onSingleClickListener
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.activity_order.pb
import kotlinx.android.synthetic.main.activity_tagihan.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat


class BerandaFragment : BaseFragment(), ViewPager.OnPageChangeListener {

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, BerandaFragment::class.java)
            context.startActivity(starter)
        }
    }
    private var banner: MutableList<BannerEntity> = mutableListOf()
    private var artikel: MutableList<ArtikelEntity> = mutableListOf()
    private lateinit var adapterIndicator: IndicatorAdapter
    lateinit var artikelAdapter: ArtikelAdapter

    override val layout: Int = R.layout.activity_home


    override fun onPreparation() {

    }

    override fun onIntent() {

    }

    override fun onUi() {

    }

    override fun onResume() {
        super.onResume()
        context?.let {
            setupAdapterBanner(it)
            setupAdapterArtikel(it)
        }

        val sharedPreference = activity?.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
        if (sharedPreference != null) {
            val name = sharedPreference.getString("name","")
            val partner = sharedPreference.getInt("partner",0)
            Log.d("print",name.toString())
            if(name!=""){
                tvName.text="Halo, "+name
                tvReward.visibility= View.VISIBLE
                tvPoin.visibility= View.VISIBLE
                doPoint(partner)
            }
            else{
                tvName.text="Halo, Pelanggan"
                tvReward.visibility= View.GONE
                tvPoin.visibility= View.GONE
            }
        }
    }

    override fun onAction() {
        val sharedPreference = activity?.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
        val name = sharedPreference?.getString("name","")

        clPesan.onSingleClickListener {
            if(name!=""){
                val intent = Intent(context, PesanTiketActivity::class.java)
                startActivity(intent)
            }
            else{
                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        ivNotif.onSingleClickListener {
            if (name != "") {
                val intent = Intent(context, NotificationActivity::class.java)
                startActivity(intent)
            }
            else{
                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        clPesanan.onSingleClickListener {
            if (name != "") {
                val intent = Intent(context, OrderActivity::class.java)
                intent.putExtra("from","Pesanan Saya")
                startActivity(intent)
            }
            else{
                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        clDriver.onSingleClickListener {
            if (name != "") {
                val intent = Intent(context, RatingActivity::class.java)
                startActivity(intent)
            }
            else{
                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        clLoket.onSingleClickListener {
            if (name != "") {
                val intent = Intent(context, LoketActivity::class.java)
                startActivity(intent)
            }
            else{
                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
            }
        }
    }


    override fun onObserver() {

        doBanner()
        doArtikel()

    }

//    fun onErrorResponse(error: VolleyError) {
//        val body: String
//        //get status code here
//        val statusCode = error.networkResponse.statusCode.toString()
//        //get response body and parse with appropriate encoding
//        if (error.networkResponse.data != null) {
//            try {
//                body = String(error.networkResponse.data, "UTF-8")
//            } catch (e: UnsupportedEncodingException) {
//                e.printStackTrace()
//            }
//        }
//        //do stuff with the body...
//    }

    override fun onPageScrolled(
        position: Int,
        positionOffset: Float,
        positionOffsetPixels: Int
    ) {
    }

    override fun onPageSelected(position: Int) {
        if (banner.isNotEmpty()) {
            val positionChanged = (position) % banner.size
            adapterIndicator.notifyPageChanged(positionChanged)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    private fun setupAdapterBanner(context: Context) {
        banner = mutableListOf()
        adapterIndicator = IndicatorAdapter(banner, context)
        rvIndicator.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter = adapterIndicator
        }
    }
    private fun setupAdapterArtikel(context: Context) {
        artikel = mutableListOf()
        artikelAdapter = ArtikelAdapter(context,artikel)
        rvArtikel.apply {
            adapter = artikelAdapter
            layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false)
        }
    }

    fun doBanner(){
        val url = "https://system.tiomaz.com/master_banner"
        val json = JSONObject()
        Log.d("print",json.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("result")
                        val arrayData = JSONArray(jsonResult2)
                        if (arrayData.length()!=0){
//                            Toast.makeText(context,"Banner Sukses", Toast.LENGTH_SHORT).show()
                            for (i in 0 until arrayData.length()){
                                val id = arrayData.getJSONObject(i).getString("id")
                                val name = arrayData.getJSONObject(i).getString("name")
                                val image = arrayData.getJSONObject(i).getString("image")
                                banner.add(BannerEntity(id = id,name=name,image=image))
                            }
                            Log.d("print",banner[0].image)
                            val bannerPagerAdapter = BannerPagerAdapter(
                                requireContext(),
                                banner
                            )
                            val infiniteBannerAdapter = CustomInfinitePagerAdapter(
                                bannerPagerAdapter
                            )
                            bannerPagerAdapter.notifyViewChanged(false)
                            adapterIndicator.notifyDataSetChanged()
                            vpBanner?.apply {
                                adapter = infiniteBannerAdapter
                                interval = 5000
                                startAutoScroll()
                                isStopScrollWhenTouch
                                addOnPageChangeListener(this@BerandaFragment)
                            }
                        }
                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
//                Log.d("print",it.networkResponse.statusCode.toString())
//                Log.d("print",String(it.networkResponse.data, StandardCharsets.UTF_8))
            })



        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(20000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context!!).addToRequestQueue(request)
    }

    fun doArtikel(){
        val url = "https://system.tiomaz.com/masterdata_list_web_article"
        val json = JSONObject()
        Log.d("print",json.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("result")
                        val arrayData = JSONArray(jsonResult2)
                        if (arrayData.length()!=0){
//                            Toast.makeText(context,"Artikel Sukses", Toast.LENGTH_SHORT).show()
                            for (i in 0 until arrayData.length()){
                                val id = arrayData.getJSONObject(i).getInt("id")
                                val name = arrayData.getJSONObject(i).getString("name")
                                val image = arrayData.getJSONObject(i).getString("image")
                                val content = arrayData.getJSONObject(i).getString("article_content")
                                artikel.add(ArtikelEntity(id,name,image,content))
                                artikelAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
//                Log.d("print",it.networkResponse.statusCode.toString())
//                Log.d("print",String(it.networkResponse.data, StandardCharsets.UTF_8))
            })



        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(20000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context!!).addToRequestQueue(request)
    }

    private fun doPoint(partner:Int) {
        val url = "https://system.tiomaz.com/point_yang_dimiliki"
        val params = JSONObject()
        params.put("partner_id", partner)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {

                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if(arrayData.length()!=0){
                            val point = arrayData.getJSONObject(0).getInt("point_yang_dimiliki")
                            tvPoin.text = point.toString()
                        }
                    }
                    else{
                        Toast.makeText(context,"Email atau Password Salah",Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context!!).addToRequestQueue(request)

    }
}