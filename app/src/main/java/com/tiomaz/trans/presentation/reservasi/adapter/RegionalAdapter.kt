package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapter
import com.tiomaz.trans.common.ActionAdapterEntity
import com.tiomaz.trans.domain.entities.RegionalEntity
import java.util.ArrayList

class RegionalAdapter(val context: Context, private val items: MutableList<RegionalEntity>) :
    RecyclerView.Adapter<RegionalViewHolder>() {

    lateinit var regional: ArrayList<String>
    lateinit var action:ActionAdapterEntity<RegionalEntity>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegionalViewHolder {
        regional = ArrayList()
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_kota,parent,false)
        return RegionalViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RegionalViewHolder, position: Int) {
        holder.bindTo(items[position], position, this)

        holder.itemView.setOnClickListener {
            action.onClick(items[position])
            notifyDataSetChanged()
        }
    }

    fun setOnClick(actionAdapter: ActionAdapterEntity<RegionalEntity>){
        this.action = actionAdapter
    }

}