package com.tiomaz.trans.presentation.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.BannerEntity

class IndicatorAdapter(private val listItems: List<BannerEntity>, val context: Context) :
    RecyclerView.Adapter<BannerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_page_indicator,parent,false)
        return BannerViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun onBindViewHolder(holder: BannerViewHolder, position: Int) {
        holder.bindTo(listItems[position],context)
    }

    fun notifyPageChanged(position: Int){
        listItems.forEachIndexed { index, bannerEntity ->
            bannerEntity.isSelected = index == position
        }
        notifyDataSetChanged()
    }
}