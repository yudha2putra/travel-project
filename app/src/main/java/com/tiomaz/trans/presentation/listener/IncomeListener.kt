package com.tiomaz.trans.presentation.listener

import com.tiomaz.trans.domain.entities.Income

interface IncomeListener {
    fun onIncomeDateClicked(income:Income)
    fun onDailyIncomeClicked(income:Income)
}