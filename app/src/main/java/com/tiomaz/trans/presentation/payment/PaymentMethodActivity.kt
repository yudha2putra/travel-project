package com.tiomaz.trans.presentation.payment

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.common.makeVisible
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PaymentEntity
import com.tiomaz.trans.presentation.main.MainActivity
import com.tiomaz.trans.presentation.order.OrderActivity
import com.tiomaz.trans.presentation.reservasi.adapter.PaymentAdapter
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.activity_payment_method.*
import kotlinx.android.synthetic.main.activity_payment_method.pb
import kotlinx.android.synthetic.main.activity_time_reservasi.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat

class PaymentMethodActivity : AppCompatActivity() {

    lateinit var booking: BookingEntity
    lateinit var payment: MutableList<PaymentEntity>
    lateinit var paymentAdapter: PaymentAdapter
    var time:Int=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_method)
        booking = intent.getSerializableExtra("booking") as BookingEntity
        payment = mutableListOf()
        val decim = DecimalFormat("Rp#,###")
        tvJumlah.setText(decim.format(booking.total).toString())
        tvKode.text="Kode Pemesanan : " + booking.kodeBook
        paymentAdapter = PaymentAdapter(this, payment,booking)
        rvPayment.apply {
            adapter = paymentAdapter
            layoutManager = LinearLayoutManager(context)
        }

        getPayment()
//        doTime()


//        ivBack.setOnClickListener {
//            finish()
//        }

    }

    fun getPayment(){
        val url = "https://system.tiomaz.com/masterdata_list_paymentgateway"

        val params = JSONObject()

        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("result")
                    val arrayData = JSONArray(jsonResult2)
                    if(arrayData.length()!=0){
//                        Toast.makeText(this,"Payment Method Sukses", Toast.LENGTH_SHORT).show()
                        for (i in 0 until arrayData.length()){
                            val id = arrayData.getJSONObject(i).getInt("id")
                            val name = arrayData.getJSONObject(i).getString("name")
                            val image = arrayData.getJSONObject(i).getString("image")
                            val explain = arrayData.getJSONObject(i).getString("type_payment_method_line")
                            val explainArray = JSONArray(explain)
                            var explaination = ""
                            var idPayment = 0

                            if(explainArray.length()!=0){
                                Log.d("print2",explainArray.toString())
                                explaination = explainArray.getJSONObject(0).getString("explanation")
                                idPayment = explainArray.getJSONObject(0).getInt("id")
                            }
                            payment.add(PaymentEntity(id,name,image,explaination,idPayment,""))
                            paymentAdapter.notifyDataSetChanged()
                        }
                        Log.d("print",payment.toString())
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }

                }catch (e:Exception){
                    Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
                }

            }, {
                // Error in request
                Toast.makeText(this,it.toString(), Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(20000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    fun reverseTimer(Seconds: Int, tv: TextView) {
        object : CountDownTimer((Seconds * 1000 + 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var seconds = (millisUntilFinished / 1000).toInt()
                val hours = seconds / (60 * 60)
                val tempMint = seconds - hours * 60 * 60
                val minutes = tempMint / 60
                seconds = tempMint - minutes * 60
                tv.text = String.format("%02d",  minutes) + " Menit : " + String.format("%02d", seconds)+ " Detik"
            }

            override fun onFinish() {
                tv.text = "Waktu Habis"
            }
        }.start()
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setTitle("Apakah anda ingin melanjutkan pembayaran?")
            .setPositiveButton("Iya",
                DialogInterface.OnClickListener { dialog, which ->

                })
            .setNegativeButton("Tidak",
                DialogInterface.OnClickListener { dialog, which ->
                    doBatal()
                    finish()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                })
            .show()
    }

    private fun doBatal() {
        val url = "https://system.tiomaz.com/cancel_booking_line"
        val params = JSONObject()
        params.put("kode_booking", booking.kodeBook)
        params.put("status_pembayaran","false")
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
//                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if(arrayData.length()!=0){
                            Toast.makeText(this,"Booking Dibatalkan", Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        Toast.makeText(this,"Booking Gagal Dibatalkan",Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
//        pb.makeVisible()
    }
    fun doTime(){

        val url = "https://system.tiomaz.com/res_company"

        val json = JSONObject()
        Log.d("print",json.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("result")
                        val arrayData = JSONArray(jsonResult2)
                        time = arrayData.getJSONObject(0).getInt("batas_proses_pemesanan")
                        reverseTimer(30*60,tvWaktu)
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
//                Log.d("print",it.networkResponse.statusCode.toString())
//                Log.d("print",String(it.networkResponse.data, StandardCharsets.UTF_8))
            })



        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(20000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }
}

//$param = array(
//'booking_line_id'   => array(intval($post['id_booking_line'])),
//'subroute_id' 		=> array(intval($post['sub_route_id'])),
//'jarak_jemput_km'	=>  $km