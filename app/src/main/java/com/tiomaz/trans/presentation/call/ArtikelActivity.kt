package com.tiomaz.trans.presentation.call

import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Base64
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.ArtikelEntity
import kotlinx.android.synthetic.main.activity_artikel.*
import kotlinx.android.synthetic.main.list_item_artikel.view.*

class ArtikelActivity : AppCompatActivity() {
    lateinit var artikel: ArtikelEntity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artikel)
        artikel = intent.getSerializableExtra("artikel") as ArtikelEntity

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvSubtitle.setText(Html.fromHtml(artikel.content, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvSubtitle.setText(Html.fromHtml(artikel.content));
        }
        tvTitle.text = artikel.name
        val imageBytes = Base64.decode(artikel.image, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        ivBerita.setImageBitmap(decodedImage)

        ivBack.setOnClickListener {
            finish()
        }
    }
}