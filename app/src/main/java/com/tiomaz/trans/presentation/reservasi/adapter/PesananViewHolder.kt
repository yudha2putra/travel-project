package com.tiomaz.trans.presentation.reservasi.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.domain.entities.LoketEntity
import com.tiomaz.trans.domain.entities.PesananEntity
import kotlinx.android.synthetic.main.list_item_loket.view.*
import kotlinx.android.synthetic.main.list_item_pesanan.view.*

class PesananViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(
        pesanan: PesananEntity) {
        itemView.tvStatus.text = pesanan.state
        itemView.tvNomor.text = pesanan.kodeBooking
        itemView.tvKeberangkatan.text = pesanan.dari
        itemView.tvTujuan.text = pesanan.tujuan
        itemView.tvTanggal.text = pesanan.tanggal
    }
}