package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.domain.entities.PaymentEntity
import com.tiomaz.trans.domain.entities.RegionalEntity
import kotlinx.android.synthetic.main.list_item_artikel.view.*
import kotlinx.android.synthetic.main.list_item_kota.view.*
import kotlinx.android.synthetic.main.list_item_payment.view.*

class PaymentViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(
        payment: PaymentEntity) {
        val imageBytes = Base64.decode(payment.image, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        itemView.ivPayment.setImageBitmap(decodedImage)
        itemView.namaPayment.text = payment.name
    }
}