package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.domain.entities.RegionalEntity
import kotlinx.android.synthetic.main.list_item_kota.view.*

class RegionalViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(
        regional: RegionalEntity,
        position: Int,
        adapter: RegionalAdapter) {
        itemView.tvKota.text = regional.regional

    }
}