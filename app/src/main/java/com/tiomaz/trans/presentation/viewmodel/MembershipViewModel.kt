package com.tiomaz.trans.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tiomaz.trans.domain.entities.User
import com.tiomaz.trans.domain.entities.request.UserRequest
import com.tiomaz.trans.domain.usecase.contract.MembershipUseCase
import com.tiomaz.trans.utils.common.ResultState
import kotlinx.coroutines.launch
import java.util.*

class MembershipViewModel (private val useCase: MembershipUseCase): ViewModel() {

    fun postLogin(email:String, password:String, db:String): MutableLiveData<ResultState<Objects>>{
        val postLogin = MutableLiveData<ResultState<Objects>>()
        postLogin.value = ResultState.Loading()
        viewModelScope.launch {
            val cityResponse = useCase.postLogin(email, password, db)
            postLogin.value = cityResponse
        }
        return postLogin
    }

    fun postRegister(email:String, password:String, telp:String, nama:String): MutableLiveData<ResultState<Objects>>{
        val postRegister = MutableLiveData<ResultState<Objects>>()
        postRegister.value = ResultState.Loading()
        viewModelScope.launch {
            val registerResponse = useCase.postRegister(email, password, telp, nama)
            postRegister.value = registerResponse
        }
        return postRegister
    }

    fun getKota(jsonrpc:String): MutableLiveData<ResultState<Objects>>{
        val getKota = MutableLiveData<ResultState<Objects>>()
        getKota.value = ResultState.Loading()
        viewModelScope.launch {
            val getKotaResponse = useCase.getKota(jsonrpc)
            getKota.value = getKotaResponse
        }
        return getKota
    }

}