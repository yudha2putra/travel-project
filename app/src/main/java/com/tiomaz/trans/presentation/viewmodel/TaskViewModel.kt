package com.tiomaz.trans.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tiomaz.trans.domain.entities.Reason
import com.tiomaz.trans.domain.entities.Task
import com.tiomaz.trans.domain.usecase.contract.TaskUseCase
import com.tiomaz.trans.utils.common.ResultState
import com.tiomaz.trans.utils.extentions.emptyString
import kotlinx.coroutines.launch
import java.io.File

class TaskViewModel(private val useCase: TaskUseCase) : ViewModel() {
    fun fetchReasonDeclineTasks(): MutableLiveData<ResultState<List<Reason>>> {
        val fetchReasonDeclineTasks = MutableLiveData<ResultState<List<Reason>>>()
        fetchReasonDeclineTasks.value = ResultState.Loading()
        viewModelScope.launch {
            val fetchReasonDeclineTasksResponse = useCase.fetchReasonDeclineTasks()
            fetchReasonDeclineTasks.value = fetchReasonDeclineTasksResponse
        }

        return fetchReasonDeclineTasks
    }

    fun fetchUnconfirmedTasks(): MutableLiveData<ResultState<List<Task>>> {
        val fetchUnconfirmedTasks = MutableLiveData<ResultState<List<Task>>>()
        fetchUnconfirmedTasks.value = ResultState.Loading()
        viewModelScope.launch {
            val unconfirmedTasksResponse = useCase.fetchUnconfirmedTasks()
            fetchUnconfirmedTasks.value = unconfirmedTasksResponse
        }

        return fetchUnconfirmedTasks
    }

    fun postConfirmTask(
        taskId: Int,
        status: String,
        location: List<String>,
        reason: String = emptyString()
    ): MutableLiveData<ResultState<Boolean>> {
        val postConfirmTask = MutableLiveData<ResultState<Boolean>>()
        postConfirmTask.value = ResultState.Loading()
        viewModelScope.launch {
            val postConfirmTaskResponse = useCase.postConfirmTask(taskId, status, location, reason)
            postConfirmTask.value = postConfirmTaskResponse
        }

        return postConfirmTask
    }

    fun fetchAcceptedTasks(): MutableLiveData<ResultState<List<Task>>> {
        val fetchAcceptedTasks = MutableLiveData<ResultState<List<Task>>>()
        fetchAcceptedTasks.value = ResultState.Loading()
        viewModelScope.launch {
            val acceptedTaskResponse = useCase.fetchAcceptedTasks()
            fetchAcceptedTasks.value = acceptedTaskResponse
        }

        return fetchAcceptedTasks
    }

    fun fetchTask(taskId: Int): MutableLiveData<ResultState<Task>> {
        val fetchTask = MutableLiveData<ResultState<Task>>()
        fetchTask.value = ResultState.Loading()
        viewModelScope.launch {
            val taskResponse = useCase.fetchTask(taskId.toString())
            fetchTask.value = taskResponse
        }

        return fetchTask
    }

    fun postCompletedTask(
        taskId: Int,
        status: String,
        location: List<String>,
        image: File
    ): MutableLiveData<ResultState<Boolean>> {
        val postCompletedTask = MutableLiveData<ResultState<Boolean>>()
        postCompletedTask.value = ResultState.Loading()
        viewModelScope.launch {
            val completedTaskResponse = useCase.postCompletedTask(taskId, status, location, image)
            postCompletedTask.value = completedTaskResponse
        }

        return postCompletedTask
    }
}