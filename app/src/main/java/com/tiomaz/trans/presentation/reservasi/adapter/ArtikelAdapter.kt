package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapter
import com.tiomaz.trans.domain.entities.ArtikelEntity
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.JamEntity
import com.tiomaz.trans.domain.entities.LoketEntity
import com.tiomaz.trans.presentation.call.ArtikelActivity
import com.tiomaz.trans.presentation.seat.SeatActivity
import kotlinx.android.synthetic.main.activity_reservasi.*
import java.util.ArrayList

class ArtikelAdapter(val context: Context, private val items: MutableList<ArtikelEntity>) :
    RecyclerView.Adapter<ArtikelViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtikelViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_artikel,parent,false)
        return ArtikelViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ArtikelViewHolder, position: Int) {
        holder.bindTo(items[position])
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ArtikelActivity::class.java)
            intent.putExtra("artikel",items[position])
            context.startActivity(intent)
        }

    }


}