package com.tiomaz.trans.presentation.listener

import com.tiomaz.trans.domain.entities.Task

interface UnconfirmedTaskListener{
    fun onDeclineClicked(data: Task, position:Int)
    fun onAcceptClicked(data:Task, position:Int)
}

interface ConfirmedTaskListener{
    fun onTaskClicked(data: Task)
}