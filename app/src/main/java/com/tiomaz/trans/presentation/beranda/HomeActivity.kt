package com.tiomaz.trans.presentation.beranda

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import com.tiomaz.trans.presentation.nearby.NearbyActivity
import com.tiomaz.trans.presentation.call.CallFragment
import com.tiomaz.trans.presentation.order.OrderActivity
import com.tiomaz.trans.presentation.packet.PacketActivity
import com.tiomaz.trans.presentation.reservasi.PesanTiketActivity
import com.tiomaz.trans.presentation.tracking.TrackingActivity
import com.tiomaz.trans.utils.extentions.onSingleClickListener
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity :AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        ivNotif.onSingleClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        }

    }
}