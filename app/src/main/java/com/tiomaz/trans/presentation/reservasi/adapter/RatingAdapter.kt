package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapter
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.JamEntity
import com.tiomaz.trans.domain.entities.LoketEntity
import com.tiomaz.trans.domain.entities.PesananEntity
import com.tiomaz.trans.presentation.rating.DetailRatingActivity
import com.tiomaz.trans.presentation.seat.SeatActivity
import kotlinx.android.synthetic.main.activity_reservasi.*
import java.util.ArrayList

class RatingAdapter(val context: Context, private val items: MutableList<PesananEntity>) :
    RecyclerView.Adapter<RatingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatingViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_rating,parent,false)
        return RatingViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RatingViewHolder, position: Int) {
        holder.bindTo(items[position])
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailRatingActivity::class.java)
            intent.putExtra("rating",items[position])
            context.startActivity(intent)
        }
    }


}