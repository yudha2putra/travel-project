package com.tiomaz.trans.presentation.packet

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_packet_receiver.*

class PacketReceiverActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packet_receiver)

        btnNext.setOnClickListener {
            val intent = Intent(this, PacketConfirmationActivity::class.java)
            startActivity(intent)
        }
    }
}