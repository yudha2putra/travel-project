package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.PassengerEntity
import kotlinx.android.synthetic.main.list_item_name.view.*


class ConfirmationAdapter(val context: Context, private val items: MutableList<PassengerEntity>) :
    RecyclerView.Adapter<ConfirmationViewHolder>() {

    var nama = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConfirmationViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_confirmation,parent,false)
        return ConfirmationViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ConfirmationViewHolder, position: Int) {
        holder.bindTo(items[position])
    }

}