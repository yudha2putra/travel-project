package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapter
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.JamEntity
import com.tiomaz.trans.domain.entities.LoketEntity
import com.tiomaz.trans.presentation.seat.SeatActivity
import kotlinx.android.synthetic.main.activity_reservasi.*
import java.util.ArrayList

class LoketAdapter(val context: Context, private val items: MutableList<LoketEntity>) :
    RecyclerView.Adapter<LoketViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LoketViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_loket,parent,false)
        return LoketViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: LoketViewHolder, position: Int) {
        holder.bindTo(items[position])

    }


}