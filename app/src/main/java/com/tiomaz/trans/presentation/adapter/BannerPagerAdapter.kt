package com.tiomaz.trans.presentation.adapter

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.BannerEntity
import kotlinx.android.synthetic.main.list_item_banner.view.*

class BannerPagerAdapter (val context: Context, val banners: List<BannerEntity>): PagerAdapter() {

    private var isLoading = false

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return banners.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_banner, container, false)
        if (!isLoading){
            with(view){
                if(banners[position].image!="false"){
                    val imageBytes = Base64.decode(banners[position].image, Base64.DEFAULT)
                    val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                    ivPromo.setImageBitmap(decodedImage)
                }
            }
        }
        container.addView(view)
        return view
    }

    fun notifyViewChanged(isLoading:Boolean){
        this.isLoading = isLoading
        notifyDataSetChanged()
    }

}