package com.tiomaz.trans.presentation.rating

import android.content.Context
import android.media.Rating
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.domain.entities.PesananEntity
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import kotlinx.android.synthetic.main.activity_detail_rating.*
import kotlinx.android.synthetic.main.activity_detail_rating.btnNext
import kotlinx.android.synthetic.main.activity_detail_rating.pb
import kotlinx.android.synthetic.main.activity_detail_rating.tvKeberangkatan
import kotlinx.android.synthetic.main.activity_detail_rating.tvTanggal
import kotlinx.android.synthetic.main.activity_detail_rating.tvTujuan
import kotlinx.android.synthetic.main.activity_seat.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat

class DetailRatingActivity : AppCompatActivity() {
    lateinit var rating: PesananEntity
    var bintang=""
    var idRating=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_rating)
        rating = intent.getSerializableExtra("rating") as PesananEntity
        doDriver()
        doViewRating()
        tvKeberangkatan.text = rating.dari
        tvTujuan.text = rating.tujuan
        tvTanggal.text = changeDate(rating.tanggal)
        tvKode.text = rating.kodeBooking
        tvWaktu.text = getTime(rating.tanggal)




    }

    fun getTime(time:String):String{
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val outputPattern = "HH:mm"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        val date = inputFormat.parse(time);
        val str = outputFormat.format(date);
        return str
    }

    fun changeDate(time:String):String{
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val outputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        val date = inputFormat.parse(time);
        val str = outputFormat.format(date);
        return str
    }

    fun doViewRating(){
        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val partner = sharedPreference.getInt("partner",0)
        val url = "https://system.tiomaz.com/view_ratings"
        val params = JSONObject()
        params.put("customer_id", partner)
        params.put("booking_id", rating.id)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("result")
                    val arrayData = JSONArray(jsonResult2)
                    if (arrayData.length()!=0){
                        idRating = arrayData.getJSONObject(0).getInt("id")
                        val rating = arrayData.getJSONObject(0).getString("ratings")
                        if(rating!="false"){
                            if(rating=="1"){
                                rating1()
                            }
                            else if(rating=="2"){
                                rating2()
                            }
                            else if(rating=="3"){
                                rating3()
                            }
                            else if(rating=="4"){
                                rating4()
                            }
                        }
                        else{
                            ivRating1.setOnClickListener {
                                rating1()
                                bintang="1"
                            }
                            ivRating2.setOnClickListener {
                                rating2()
                                bintang="2"
                            }
                            ivRating3.setOnClickListener {
                                rating3()
                                bintang="3"

                            }
                            ivRating4.setOnClickListener {
                                rating4()
                                bintang="4"
                            }
                            btnNext.setOnClickListener {
                                doRating()
                            }
                        }
                        val message = arrayData.getJSONObject(0).getString("message")
                        if(message!="false"){
                            etReview.setText(message)
                            etReview.setFocusable(false)
                        }
                        val driver = arrayData.getJSONObject(0).getString("vendor_id")
                        val arrayDriver = JSONArray(driver)
                        val namaDriver = arrayDriver.getJSONObject(0).getString("name")
                        etDriver.setText(namaDriver)
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    fun doDriver(){
        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val partner = sharedPreference.getInt("partner",0)
        val url = "https://system.tiomaz.com/customer_rating"
        val params = JSONObject()
        params.put("customer_id", partner)
        params.put("booking_id", rating.id)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("data")
                    val arrayData = JSONArray(jsonResult2)
                    if (arrayData.length()!=0){
                        val driver = arrayData.getJSONObject(0).getString("driver")
                        etDriver.setText(driver)
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    fun doRating(){
        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val partner = sharedPreference.getInt("partner",0)
        val url = "https://system.tiomaz.com/customer_review_without_message_and_ratings"
        val params = JSONObject()
        params.put("id_customer_ratings", idRating)
        params.put("ratings", bintang)
        params.put("message", etReview.text)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("data")
                    val arrayData = JSONArray(jsonResult2)
                    if (arrayData.length()!=0){
                        val rating = arrayData.getJSONObject(0).getString("response_desc")
                        Toast.makeText(this,rating.toString(), Toast.LENGTH_SHORT).show()
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    fun rating1(){
        ivRating1.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating2.setBackgroundResource(R.drawable.ic_rating_off)
        ivRating3.setBackgroundResource(R.drawable.ic_rating_off)
        ivRating4.setBackgroundResource(R.drawable.ic_rating_off)
    }

    fun rating2(){
        ivRating1.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating2.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating3.setBackgroundResource(R.drawable.ic_rating_off)
        ivRating4.setBackgroundResource(R.drawable.ic_rating_off)
    }
    fun rating3(){
        ivRating1.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating2.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating3.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating4.setBackgroundResource(R.drawable.ic_rating_off)
    }
    fun rating4(){
        ivRating1.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating2.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating3.setBackgroundResource(R.drawable.ic_rating_on)
        ivRating4.setBackgroundResource(R.drawable.ic_rating_on)
    }



}