package com.tiomaz.trans.presentation.packet

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_packet.*

class PacketActivity : AppCompatActivity() {

    val REQUEST_TYPE = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packet)

        etJenisPaket.setOnClickListener {
            val intent = Intent(this, PacketTypeActivity::class.java)
            startActivityForResult(intent,REQUEST_TYPE)
        }

        etPengambilan.setOnClickListener {
            val intent = Intent(this, PacketDestinationActivity::class.java)
            intent.putExtra("title","Pengambilan Paket")
            startActivity(intent)
        }
        etPengiriman.setOnClickListener {
            val intent = Intent(this, PacketDestinationActivity::class.java)
            intent.putExtra("title","Pengiriman Paket")
            startActivity(intent)
        }
        btnNext.setOnClickListener{
            val intent = Intent(this, PacketReceiverActivity::class.java)
            startActivity(intent)
        }
        ivBack.setOnClickListener {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_TYPE){
            val model = data?.getStringExtra("type")?:""
            if (model!=""){
                etJenisPaket.setText(data?.getStringExtra("type"))
            }
            return
        }
    }
}