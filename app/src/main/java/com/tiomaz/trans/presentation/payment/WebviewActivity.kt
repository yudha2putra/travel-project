package com.tiomaz.trans.presentation.payment

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.print.PrintAttributes
import android.print.PrintManager
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import android.print.PrintJob
import com.tiomaz.trans.presentation.main.MainActivity
import com.tiomaz.trans.presentation.order.OrderActivity
import kotlinx.android.synthetic.main.activity_payment_howto.*
import kotlinx.android.synthetic.main.activity_webview.*
import kotlinx.android.synthetic.main.activity_webview.btnHome
import kotlinx.android.synthetic.main.activity_webview.btnPesanan


class WebviewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        val url = intent.getStringExtra("url")
        if (url != null) {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(url)
        }
        btnPesanan.setOnClickListener {
            val intent = Intent(this, OrderActivity::class.java)
            intent.putExtra("from","Pesanan Saya")
            startActivity(intent)
        }
        btnHome.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        MainActivity.start(this)
        super.onBackPressed()
    }

}