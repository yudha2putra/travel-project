package com.tiomaz.trans.presentation.profile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat.finishAffinity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.base.BaseFragment
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.presentation.main.MainActivity
import com.tiomaz.trans.presentation.verification.LoginActivity
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import com.tiomaz.trans.utils.extentions.onSingleClickListener
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile.btnNext
import kotlinx.android.synthetic.main.activity_profile.cbSetuju
import kotlinx.android.synthetic.main.activity_profile.etEmail
import org.json.JSONArray
import org.json.JSONObject

class ProfileFragment : BaseFragment() {


    override val layout: Int = R.layout.activity_profile

    override fun onPreparation() {
        val sharedPreference = activity?.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val name = sharedPreference?.getString("name","")
        if(name==""){
            val intent = Intent(context, LoginActivity::class.java)
            startActivity(intent)
        }
        doGetProfile()
//        val email = sharedPreference?.getString("email","")
//        val phone = sharedPreference?.getString("phone","")
//        etNama.setText(name)
//        etEmail.setText(email)
//        etNomor.setText(phone)
    }

    override fun onIntent() {
    }

    override fun onUi() {
    }

    override fun onAction() {
        btnNext.onSingleClickListener {
            if(cbSetuju.isChecked){
                doProfile()
            }
            else{
                checklistInfo.makeVisible()
            }

        }
        btnLogout.onSingleClickListener {
            val sharedPreference =  context?.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
            var editor = sharedPreference?.edit()
            editor?.remove("partner")?.commit()
            editor?.remove("name")?.commit()
            editor?.remove("email")?.commit()
            editor?.remove("phone")?.commit()
            editor?.remove("uid")?.commit()
            context?.let { MainActivity.start(it) }
            finishAffinity(context as Activity)

        }
    }

    override fun onObserver() {
    }

    private fun doProfile() {
        val sharedPreference = activity?.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val uid = sharedPreference?.getInt("uid",0)

        val url = "https://system.tiomaz.com/edit_profile"
        val params = JSONObject()
        params.put("user_id", uid)
        params.put("nama_lengkap", etNama.text)
        params.put("no_telp", etNomor.text)
        params.put("email", etEmail.text)
        params.put("alamat_utama", etAlamat.text)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if (arrayData.length()!=0) {
                            Toast.makeText(context,"Profile Sukses", Toast.LENGTH_SHORT).show()
                            val sharedPreference =  activity?.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
                            var editor = sharedPreference?.edit()
                            editor?.putString("name",etNama.text.toString())
                            editor?.putString("email",etEmail.text.toString())
                            editor?.putString("phone",etNomor.text.toString())
                            editor?.commit()
                            checklistInfo.makeGone()
                        }

                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        context?.let { VolleySingleton.getInstance(it).addToRequestQueue(request) }
        pb.makeVisible()
    }

    private fun doGetProfile() {
        val sharedPreference = activity?.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val uid = sharedPreference?.getInt("uid",0)

        val url = "https://system.tiomaz.com/view_profile"
        val params = JSONObject()
        params.put("user_id", uid)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if (arrayData.length()!=0) {
//                            Toast.makeText(context,"Profile Sukses", Toast.LENGTH_SHORT).show()
                            val name = arrayData.getJSONObject(0).getString("name")
                            val email = arrayData.getJSONObject(0).getString("email")
                            val phone = arrayData.getJSONObject(0).getString("phone")
                            val alamat = arrayData.getJSONObject(0).getString("alamat_utama")
                            etNama.setText(name)
                            etEmail.setText(email)
                            etNomor.setText(phone)
                            etAlamat.setText(alamat)
                        }

                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        context?.let { VolleySingleton.getInstance(it).addToRequestQueue(request) }
        pb.makeVisible()
    }


}