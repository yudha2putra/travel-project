package com.tiomaz.trans.presentation.tracking

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_detail_tracking.*

class DetailTrackingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_tracking)

        ivBack.setOnClickListener {
            finish()
        }
    }
}