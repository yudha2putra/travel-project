package com.tiomaz.trans.presentation.reservasi.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.domain.entities.JamEntity
import kotlinx.android.synthetic.main.activity_seat.*
import kotlinx.android.synthetic.main.list_item_jam.view.*

class JamViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(
        jamEntity: JamEntity) {
        itemView.tvJam.text = jamEntity.jam
        itemView.tvKursi.text = jamEntity.kursi
        itemView.kode.text = jamEntity.defaultCode
        if(jamEntity.kursi=="Tersedia 0 Kursi"){
            itemView.setBackgroundResource(R.drawable.round_grey_button)
        }
        if(jamEntity.etd==0){
            itemView.tvEtd.makeGone()
        }
        else{
            itemView.tvEtd.text = "+"+jamEntity.etd.toString()+" Menit"
        }

        if (jamEntity.keberangkatanMain=="true"&&jamEntity.tujuanMain=="true"&&jamEntity.lessthan){
            itemView.icon.setBackgroundResource(R.drawable.ic_jemputantar_white)
        }
        else if(jamEntity.keberangkatanMain=="true"&&jamEntity.tujuanMain=="" ){
            itemView.icon.setBackgroundResource(R.drawable.ic_jemput_white)
        }
        else if(jamEntity.keberangkatanMain==""&&jamEntity.tujuanMain=="true"|| !jamEntity.lessthan){
            itemView.icon.setBackgroundResource(R.drawable.ic_antar_white)
        }
        else{
            itemView.icon.makeGone()
        }
    }
}