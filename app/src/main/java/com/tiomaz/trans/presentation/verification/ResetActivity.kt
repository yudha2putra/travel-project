package com.tiomaz.trans.presentation.verification

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseActivity
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.data.preference.KecipirPreference
import com.tiomaz.trans.presentation.profile.CreateProfileActivity
import com.tiomaz.trans.presentation.viewmodel.MembershipViewModel
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import kotlinx.android.synthetic.main.activity_reset.*
import org.json.JSONArray
import org.json.JSONObject

class ResetActivity : BaseActivity(){
    companion object {
        fun start(context: Context) {
            val starter = Intent(context, ResetActivity::class.java)
            context.startActivity(starter)
        }
    }

    override val layout: Int = R.layout.activity_reset
    override fun onPreparation() {
    }

    override fun onIntent() {
    }

    override fun onObserver() {
    }
    override fun onUi() {

    }

    override fun onAction() {
        btnReset.setOnClickListener {
            doReset()
        }
        daftar.setOnClickListener {
            CreateProfileActivity.start(this)
        }
        login.setOnClickListener {
            LoginActivity.start(this)
        }
    }

    private fun doReset() {
        val url = "https://system.tiomaz.com/reset_password"
        val params = JSONObject()
        params.put("email", tvEmail.text)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if (arrayData.length()!=0){
                            val respon = arrayData.getJSONObject(0).getString("response_code")
                            val desc = arrayData.getJSONObject(0).getString("response_desc")
                            if (respon=="00"){
                                Toast.makeText(this,desc, Toast.LENGTH_SHORT).show()
                                LoginActivity.start(this)
                            }
                            else{
                                Toast.makeText(this,desc, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
                }

            }, {
                // Error in request
                Toast.makeText(this,it.toString(), Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

}