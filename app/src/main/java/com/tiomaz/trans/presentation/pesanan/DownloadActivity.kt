package com.tiomaz.trans.presentation.pesanan

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.print.PrintAttributes
import android.print.PrintManager
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import android.print.PrintJob
import com.tiomaz.trans.domain.entities.PesananEntity


class DownloadActivity : AppCompatActivity() {
    // creating object of WebView
    var printWeb: WebView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_download)

        val id = intent.getSerializableExtra("id")
        Toast.makeText(
            this,
            id.toString(),
            Toast.LENGTH_SHORT
        ).show()
        // Initializing the WebView
        val webView = findViewById<View>(R.id.webViewMain) as WebView

        // Initializing the Button
        val savePdfBtn: Button = findViewById<View>(R.id.savePdfBtn) as Button

        // Setting we View Client
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                // initializing the printWeb Object
                printWeb = webView
            }
        }

        // loading the URL
        webView.loadUrl("https://theteamtheteam.com/project/tiomaz/cek_pesanan/bukti_pembayaran/8233")

        // setting clickListener for Save Pdf Button
        savePdfBtn.setOnClickListener{

            if (printWeb != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    // Calling createWebPrintJob()
                    PrintTheWebPage(printWeb!!)
                } else {
                    // Showing Toast message to user
                    Toast.makeText(
                        this,
                        "Not available for device below Android LOLLIPOP",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                // Showing Toast message to user
                Toast.makeText(
                    this,
                    "WebPage not fully loaded",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }

    // object of print job
    var printJob: PrintJob? = null

    // a boolean to check the status of printing
    var printBtnPressed = false
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun PrintTheWebPage(webView: WebView) {

        // set printBtnPressed true
        printBtnPressed = true

        // Creating  PrintManager instance
        val printManager = this
            .getSystemService(Context.PRINT_SERVICE) as PrintManager

        // setting the name of job
        val jobName = "Pesanan" + intent.getSerializableExtra("id")

        // Creating  PrintDocumentAdapter instance
        val printAdapter = webView.createPrintDocumentAdapter(jobName)
        assert(printManager != null)
        printJob = printManager.print(
            jobName, printAdapter,
            PrintAttributes.Builder().build()
        )
    }

    override fun onResume() {
        super.onResume()
        if (printJob != null && printBtnPressed) {
            if (printJob!!.isCompleted()) {
                // Showing Toast Message
                Toast.makeText(this, "Completed", Toast.LENGTH_SHORT).show()
            } else if (printJob!!.isStarted()) {
                // Showing Toast Message
                Toast.makeText(this, "isStarted", Toast.LENGTH_SHORT).show()
            } else if (printJob!!.isBlocked()) {
                // Showing Toast Message
                Toast.makeText(this, "isBlocked", Toast.LENGTH_SHORT).show()
            } else if (printJob!!.isCancelled()) {
                // Showing Toast Message
                Toast.makeText(this, "isCancelled", Toast.LENGTH_SHORT).show()
            } else if (printJob!!.isFailed()) {
                // Showing Toast Message
                Toast.makeText(this, "isFailed", Toast.LENGTH_SHORT).show()
            } else if (printJob!!.isQueued()) {
                // Showing Toast Message
                Toast.makeText(this, "isQueued", Toast.LENGTH_SHORT).show()
            }
            // set printBtnPressed false
            printBtnPressed = false
        }
    }
}