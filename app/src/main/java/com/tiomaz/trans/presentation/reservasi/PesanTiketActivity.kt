package com.tiomaz.trans.presentation.reservasi

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.text.InputFilter
import android.util.Log
import android.widget.Toast
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseActivity
import com.tiomaz.trans.common.ActionAdapterEntity
import com.tiomaz.trans.common.InputFilterMinMax
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.KotaEntity
import com.tiomaz.trans.domain.entities.RegionalEntity
import com.tiomaz.trans.presentation.reservasi.dialog.KotaDialog
import com.tiomaz.trans.presentation.reservasi.dialog.KotaDialogTujuan
import com.tiomaz.trans.presentation.reservasi.dialog.RegionalDialog
import com.tiomaz.trans.utils.extentions.onSingleClickListener
import kotlinx.android.synthetic.main.activity_reservasi.*
import java.text.SimpleDateFormat
import java.util.*


class PesanTiketActivity : BaseActivity(){

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, PesanTiketActivity::class.java)
            context.startActivity(starter)
        }
    }
    lateinit var names:MutableList<String>
    var regional:Int=0

    var keberangkatanTime:String=""
    var keberangkatanLat:String=""
    var keberangkatanLong:String=""
    var keberangkatanMain:String=""
    var keberangkatanRadius:String=""
    var tujuanLat:String=""
    var tujuanLong:String=""
    var tujuanMain:String=""
    var tujuanRadius:String=""

    private lateinit var dialogKeberangkatan: KotaDialog
    private lateinit var dialogTujuan: KotaDialogTujuan
    private lateinit var dialogRegional: RegionalDialog
    lateinit var booking: BookingEntity

    override val layout: Int = R.layout.activity_reservasi
    val REQUEST_DEPARTURE = 1
    val REQUEST_ARRIVAL = 2
    val REQUEST_DATE = 3
    val REQUEST_TIME = 4
    val REQUEST_NAME = 5


    override fun onPreparation() {
        doKota()
        dialogRegional = RegionalDialog(this)


    }

    override fun onIntent() {
    }

    override fun onObserver() {
    }

    override fun onAction() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        ivBack.setOnClickListener {
            finish()
        }

        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val currentDateandTime: String = sdf.format(Date())
        etTanggal.setText(currentDateandTime)
        etJumlah.setText("1")

        etTanggal.onSingleClickListener {
            val dpd = DatePickerDialog(
                this,
                R.style.DialogTheme,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                    // Display Selected date in textbox
                    val calendar = Calendar.getInstance()
                    calendar.set(year, monthOfYear, dayOfMonth);
                    val format = SimpleDateFormat("dd-MM-yyyy")
                    val dateString = format.format(calendar.time)
                    etTanggal.setText(dateString)

                },
                year,
                month,
                day
            )
            dpd.getDatePicker().setMinDate(System.currentTimeMillis() - 1000)
            Toast.makeText(this, "oke", Toast.LENGTH_SHORT)
            dpd.show()
        }

        etJumlah.setFilters(arrayOf<InputFilter>(InputFilterMinMax(1, 11)))

            btnNext.onSingleClickListener {
            booking = BookingEntity(
                keberangkatan = etKeberangkatan.text.toString(),
                tujuan = etTujuan.text.toString(),
                tanggal = etTanggal.text.toString(),
                penumpang = etJumlah.text.toString(),
                jam = "",
                idJam = 0,
                kodeBook = "",
                total = 0,
                keberangkatanTime = keberangkatanTime,
                keberangkatanLat = keberangkatanLat,
                keberangkatanLong = keberangkatanLong,
                keberangkatanMain = keberangkatanMain,
                keberangkatanRadius = keberangkatanRadius,
                tujuanLat = tujuanLat,
                tujuanLong = tujuanLong,
                tujuanMain = tujuanMain,
                tujuanRadius = tujuanRadius,
                lessthan = true
            )
                Log.d("print",booking.toString())
            val intent = Intent(this, TimeReservasiActivity::class.java)
            intent.putExtra("booking", booking)
            startActivity(intent)
        }

        etRegional.onSingleClickListener {
            dialogRegional.show()
            dialogRegional.setOnClickDialog(object : ActionAdapterEntity<RegionalEntity> {
                override fun onClick(regionalEntity: RegionalEntity) {
                    etRegional.setText(regionalEntity.regional)
                    regional=regionalEntity.id
                }
            })
        }

        etKeberangkatan.onSingleClickListener {
            dialogKeberangkatan = KotaDialog(this)
            dialogKeberangkatan.setRegional(regional)
            dialogKeberangkatan.show()
            dialogKeberangkatan.setOnClickDialog(object : ActionAdapterEntity<KotaEntity> {
                override fun onClick(kotaEntity: KotaEntity) {
                    etKeberangkatan.setText(kotaEntity.name)
                    if (kotaEntity.isMain=="true"){
                        keberangkatanLat=kotaEntity.latitude
                        keberangkatanLong=kotaEntity.longitude
                        keberangkatanMain=kotaEntity.isMain
                        keberangkatanRadius=kotaEntity.radius
                        keberangkatanTime=kotaEntity.time
                    }
                    else{
                        keberangkatanLat=""
                        keberangkatanLong=""
                        keberangkatanMain=""
                        keberangkatanRadius=""
                        keberangkatanTime=""
                    }
                }
            })
        }

        etTujuan.onSingleClickListener {
            dialogTujuan = KotaDialogTujuan(this)
            dialogTujuan.setRegional(regional)
            dialogTujuan.show()
            dialogTujuan.setOnClickDialog(object : ActionAdapterEntity<KotaEntity> {
                override fun onClick(kotaEntity: KotaEntity) {
                    etTujuan.setText(kotaEntity.name)
                    if (kotaEntity.isMain=="true"){
                        tujuanLat=kotaEntity.latitude
                        tujuanLong=kotaEntity.longitude
                        tujuanMain=kotaEntity.isMain
                        tujuanRadius=kotaEntity.radius
                    }
                    else{
                        tujuanLat=""
                        tujuanLong=""
                        tujuanMain=""
                        tujuanRadius=""
                    }
                }
            })
        }
    }

    override fun onUi() {

    }

    private fun doKota() {


    }

    override fun onResume() {
        super.onResume()
//        etRegional.text.clear()
//        etKeberangkatan.text.clear()
//        etTujuan.text.clear()
//        keberangkatanLat = ""
//        keberangkatanLong = ""
//        keberangkatanMain = ""
//        tujuanLat = ""
//        tujuanLong = ""
//        tujuanMain = ""
    }


}