package com.tiomaz.trans.presentation.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.BannerEntity
import kotlinx.android.synthetic.main.item_page_indicator.view.*

class BannerViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindTo(bannerPromo: BannerEntity, context: Context) {
        with(itemView){
            imgIndicator.setImageResource(if (bannerPromo.isSelected) R.drawable.circle_indicator_selected else R.drawable.circle_indicator_unselected )
        }
    }
}