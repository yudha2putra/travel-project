package com.tiomaz.trans.presentation.nearby

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.common.makeVisible
import com.tiomaz.trans.domain.entities.BannerEntity
import com.tiomaz.trans.domain.entities.JamEntity
import com.tiomaz.trans.domain.entities.LoketEntity
import com.tiomaz.trans.presentation.reservasi.adapter.JamAdapter
import com.tiomaz.trans.presentation.reservasi.adapter.LoketAdapter
import kotlinx.android.synthetic.main.activity_loket.*
import kotlinx.android.synthetic.main.activity_rating.pb
import org.json.JSONArray
import org.json.JSONObject

class LoketActivity : AppCompatActivity() {

    lateinit var loketAdapter: LoketAdapter
    lateinit var loket: MutableList<LoketEntity>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loket)
        loket = mutableListOf()
        loketAdapter = LoketAdapter(this,loket)
        rvLoket.apply {
            adapter = loketAdapter
            layoutManager = GridLayoutManager(context,1)
        }
        doListingLoket()
        ivClose.setOnClickListener {
            finish()
        }
    }

    fun doListingLoket(){

        val url = "https://system.tiomaz.com/location_loket"
        val json = JSONObject()
        Log.d("print",json.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("result")
                        val arrayData = JSONArray(jsonResult2)
                        if (arrayData.length()!=0){
                            Toast.makeText(this,"Loket Sukses", Toast.LENGTH_SHORT).show()
                            for (i in 0 until arrayData.length()){
                                val id = arrayData.getJSONObject(i).getInt("id")
                                val name = arrayData.getJSONObject(i).getString("name")
                                val address = arrayData.getJSONObject(i).getString("address")
                                val telp = arrayData.getJSONObject(i).getString("no_telp")
                                loket.add(LoketEntity(id,name,address,telp))
                                loketAdapter.notifyDataSetChanged()
                            }
                        }
                        else{
                            Toast.makeText(this,"Loket Kosong", Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
//                Log.d("print",it.networkResponse.statusCode.toString())
//                Log.d("print",String(it.networkResponse.data, StandardCharsets.UTF_8))
            })



        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(20000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }
}