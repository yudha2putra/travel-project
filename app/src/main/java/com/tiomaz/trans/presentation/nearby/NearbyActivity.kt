package com.tiomaz.trans.presentation.nearby

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.tiomaz.trans.R
import com.tiomaz.trans.common.AESUtils
import kotlinx.android.synthetic.main.activity_nearby.*
import java.nio.charset.Charset
import java.util.*

class NearbyActivity : FragmentActivity(), OnMapReadyCallback, LocationListener {

    private var mMap: GoogleMap? = null
    private var marker: Marker? = null
    lateinit var currentLocation:Location
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var REQUEST_CODE = 101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nearby)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        val dec = hexStringToByteArray(resources.getString(R.string.google_maps_encrypt))
        val enc = dec?.let { AESUtils.decrypt(it) }
        Places.initialize(getApplicationContext(), enc?.toString(Charset.defaultCharset())?:"")
        val autocompleteFragment: AutocompleteSupportFragment? =
            supportFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as AutocompleteSupportFragment?
        autocompleteFragment?.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME))

        fetchLocation()


        ivBack.setOnClickListener {
            onBackPressed()
        }

    }

    private fun fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE
            )
            return
        }
        val task: Task<Location> = fusedLocationProviderClient.getLastLocation()
        task.addOnSuccessListener { location ->
            if (location != null) {
                Log.d("hasilnya","hasil")
                currentLocation = location
                Toast.makeText(
                    applicationContext,
                    currentLocation.latitude
                        .toString() + "" + currentLocation.longitude,
                    Toast.LENGTH_SHORT
                ).show()
                val supportMapFragment =
                    (supportFragmentManager.findFragmentById(R.id.mymap) as SupportMapFragment?)!!
                supportMapFragment.getMapAsync(this@NearbyActivity)

            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap): Unit {
        mMap = googleMap
        val latLng = LatLng(currentLocation.latitude, currentLocation.longitude)
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,15f))
        val center = mMap?.cameraPosition?.target
        marker = mMap?.addMarker(MarkerOptions().position(center!!).icon(bitmapDescriptorFromVector(applicationContext,R.drawable.ic_pin_map)))


        mMap?.setOnCameraMoveListener{
            val currentCenter = mMap?.cameraPosition?.target
            if (currentCenter != null) {
                marker?.position = currentCenter
            }

        }
//        mMap?.setOnCameraIdleListener {
//            getAddressName()
//        }

    }

    override fun onLocationChanged(p0: Location) {
        TODO("Not yet implemented")
    }


    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        // TODO Auto-generated method stub
    }


    fun hexStringToByteArray(s: String): ByteArray? {
        val len = s.length
        val data = ByteArray(len / 2)
        var i = 0
        while (i < len) {
            data[i / 2] = ((Character.digit(
                s[i],
                16
            ) shl 4) + Character.digit(s[i + 1], 16)).toByte()
            i += 2
        }
        return data
    }

    private fun bitmapDescriptorFromVector(
        context: Context,
        vectorResId: Int
    ): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }
}

