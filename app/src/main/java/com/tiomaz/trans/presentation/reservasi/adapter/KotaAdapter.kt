package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapter
import com.tiomaz.trans.common.ActionAdapterEntity
import com.tiomaz.trans.domain.entities.KotaEntity
import java.util.ArrayList

class KotaAdapter(val context: Context, private val items: MutableList<KotaEntity>) :
    RecyclerView.Adapter<KotaViewHolder>() {

    lateinit var kota: ArrayList<String>
    lateinit var action:ActionAdapterEntity<KotaEntity>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KotaViewHolder {
        kota = ArrayList()
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_kota,parent,false)
        return KotaViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: KotaViewHolder, position: Int) {
        holder.bindTo(items[position], position, this)

        holder.itemView.setOnClickListener {
            action.onClick(items[position])
            notifyDataSetChanged()
        }
    }

    fun setOnClick(actionAdapter: ActionAdapterEntity<KotaEntity>){
        this.action = actionAdapter
    }

}