package com.tiomaz.trans.presentation.packet

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_packet_type.*

class PacketTypeActivity : AppCompatActivity() {

    private var type=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packet_type)

        ivClose.setOnClickListener{
            var intent = Intent()
            intent.putExtra("type",type)
            setResult(Activity.RESULT_OK,intent)
            finish()
        }

        tvSedang.setOnClickListener {
            tvSedang.background = ContextCompat.getDrawable(this, R.drawable.round_blue_button)
            tvSedang.setTextColor(ContextCompat.getColor(this,R.color.textWhite))
            tvBerat.background = ContextCompat.getDrawable(this, R.drawable.round_border_black)
            tvBerat.setTextColor(ContextCompat.getColor(this,R.color.textBlack))
            tvPecah.background = ContextCompat.getDrawable(this, R.drawable.round_border_black)
            tvPecah.setTextColor(ContextCompat.getColor(this,R.color.textBlack))
            type = "Barang Sedang"
        }
        tvBerat.setOnClickListener {
            tvSedang.background = ContextCompat.getDrawable(this, R.drawable.round_border_black)
            tvSedang.setTextColor(ContextCompat.getColor(this,R.color.textBlack))
            tvBerat.background = ContextCompat.getDrawable(this, R.drawable.round_blue_button)
            tvBerat.setTextColor(ContextCompat.getColor(this,R.color.textWhite))
            tvPecah.background = ContextCompat.getDrawable(this, R.drawable.round_border_black)
            tvPecah.setTextColor(ContextCompat.getColor(this,R.color.textBlack))
            type = "Barang Berat"
        }
        tvPecah.setOnClickListener {
            tvSedang.background = ContextCompat.getDrawable(this, R.drawable.round_border_black)
            tvSedang.setTextColor(ContextCompat.getColor(this,R.color.textBlack))
            tvBerat.background = ContextCompat.getDrawable(this, R.drawable.round_border_black)
            tvBerat.setTextColor(ContextCompat.getColor(this,R.color.textBlack))
            tvPecah.background = ContextCompat.getDrawable(this, R.drawable.round_blue_button)
            tvPecah.setTextColor(ContextCompat.getColor(this,R.color.textWhite))
            type = "Barang Pecah Belah"
        }
    }
}