package com.tiomaz.trans.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tiomaz.trans.domain.entities.Vehicle
import com.tiomaz.trans.domain.usecase.contract.MasterDataUseCase
import com.tiomaz.trans.utils.common.ResultState
import kotlinx.coroutines.launch

class MasterDataViewModel(private val useCase: MasterDataUseCase) : ViewModel() {
    fun fetchVehicles(): MutableLiveData<ResultState<List<Vehicle>>> {
        val fetchVehicles = MutableLiveData<ResultState<List<Vehicle>>>()
        fetchVehicles.value = ResultState.Loading()
        viewModelScope.launch {
            val fetchVehiclesResponse = useCase.fetchVehicles()
            fetchVehicles.value = fetchVehiclesResponse
        }
        return fetchVehicles
    }
}