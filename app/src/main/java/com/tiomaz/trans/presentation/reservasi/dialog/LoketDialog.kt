package com.tiomaz.trans.presentation.reservasi.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.*
import com.tiomaz.trans.domain.entities.PassengerEntity
import com.tiomaz.trans.domain.entities.RegionalEntity
import com.tiomaz.trans.presentation.reservasi.adapter.KotaAdapter
import com.tiomaz.trans.presentation.reservasi.adapter.RegionalAdapter
import kotlinx.android.synthetic.main.kota_dialog.rvKota
import kotlinx.android.synthetic.main.regional_dialog.*
import org.json.JSONArray
import org.json.JSONObject
import java.nio.charset.StandardCharsets

class LoketDialog(context: Context, idRoute: Int, kota:String) : Dialog(context), ActionAdapterEntity<RegionalEntity> {

    lateinit var actionAdapter: ActionAdapterEntity<RegionalEntity>
    lateinit var regionalAdapter: RegionalAdapter
    lateinit var regional: MutableList<RegionalEntity>
    var idRoute = idRoute
    var kota = kota

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loket_dialog)
        val width = (context.resources.displayMetrics.widthPixels * 0.70).toInt()
        window?.setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        regional = mutableListOf()
        regionalAdapter = RegionalAdapter(context,regional)
        rvKota.apply {
            adapter = regionalAdapter
            layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL,false)
        }


        val url = "https://system.tiomaz.com/masterdata_list_drop_pick_point_location"
        val json = JSONObject()
        json.put("subroute",idRoute)

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("result")
                    val arrayData = JSONArray(jsonResult2)
                    if(arrayData.length()!=0){
//                        Toast.makeText(context,"Regional Sukses", Toast.LENGTH_SHORT).show()
                        for (i in 0 until arrayData.length()){
                            val location = arrayData.getJSONObject(i).getString("location_id")
                            val arrayLocation = JSONArray(location)
                            val nameKota = arrayLocation.getJSONObject(0).getString("name")
                            if(nameKota==kota){
                                val name = arrayData.getJSONObject(i).getString("name")
                                val id = arrayData.getJSONObject(i).getInt("id")
                                regional.add(RegionalEntity(id = id, regional = name))
                                regionalAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.networkResponse.statusCode.toString())
                Log.d("print",String(it.networkResponse.data, StandardCharsets.UTF_8))
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            10000,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context).addToRequestQueue(request)
        pb.makeVisible()


        regionalAdapter.setOnClick(this)
    }

    override fun onClick(regional: RegionalEntity) {
        dismiss()
        actionAdapter.onClick(regional)
    }



    fun setOnClickDialog(actionAdapter: ActionAdapterEntity<RegionalEntity>){
        this.actionAdapter = actionAdapter
    }


}