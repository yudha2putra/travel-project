package com.tiomaz.trans.presentation.payment

import android.content.*
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PaymentEntity
import com.tiomaz.trans.presentation.main.MainActivity
import com.tiomaz.trans.presentation.order.OrderActivity
import kotlinx.android.synthetic.main.activity_payment_howto.*
import kotlinx.android.synthetic.main.activity_payment_howto.tvWaktu
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ThreadLocalRandom.current

class PaymentHowTo : AppCompatActivity() {
    lateinit var payment: PaymentEntity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_howto)
        payment = intent.getSerializableExtra("payment") as PaymentEntity
        val time = intent!!.getStringExtra("time")
        val kodeBook = intent!!.getStringExtra("kode_book")


        tvVA.text = "No.VA : " + payment.trxID
        tvKode.text = "Kode Pemesanan : " +kodeBook
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvCaraBayar.setText(Html.fromHtml(payment.explanation, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvCaraBayar.setText(Html.fromHtml(payment.explanation));
        }

        salin.setOnClickListener {
            copyToClipboard(payment.trxID)
            Toast.makeText(this,"Nomor VA Berhasil di Copy", Toast.LENGTH_SHORT).show()
        }

        btnPesanan.setOnClickListener {
            val intent = Intent(this, OrderActivity::class.java)
            intent.putExtra("from","Pesanan Saya")
            startActivity(intent)
        }
        btnHome.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        time?.let { getDifferences(it) }?.let { reverseTimer(it,tvWaktu) }
    }

    fun Context.copyToClipboard(text: CharSequence){
        val clipboard = ContextCompat.getSystemService(this,ClipboardManager::class.java)
        clipboard?.setPrimaryClip(ClipData.newPlainText("",text))
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setTitle("Apakah anda ingin melanjutkan pembayaran?")
            .setPositiveButton("Iya",
                DialogInterface.OnClickListener { dialog, which ->

                })
            .setNegativeButton("Tidak",
                DialogInterface.OnClickListener { dialog, which ->
                    finish()
                })
            .show()
    }

    fun reverseTimer(Seconds: Int, tv: TextView) {
        object : CountDownTimer((Seconds * 1000 + 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var seconds = (millisUntilFinished / 1000).toInt()
                val hours = seconds / (60 * 60)
                val tempMint = seconds - hours * 60 * 60
                val minutes = tempMint / 60
                seconds = tempMint - minutes * 60
                tv.text = String.format("%02d",  minutes) + " Menit : " + String.format("%02d", seconds)+ " Detik"
            }

            override fun onFinish() {
                tv.text = "Waktu Habis"
            }
        }.start()
    }

    fun getDifferences(batas:String):Int{
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val currentDateandTime: String = sdf.format(Date())
        val fmt = SimpleDateFormat(inputPattern, Locale.getDefault())
        val bgn = fmt.parse(currentDateandTime)
        val end = fmt.parse(batas)
        val milliseconds = end.time - bgn.time
        val second = milliseconds / 1000
        return  second.toInt()
    }
}