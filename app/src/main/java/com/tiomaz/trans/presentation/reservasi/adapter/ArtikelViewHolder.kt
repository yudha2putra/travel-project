package com.tiomaz.trans.presentation.reservasi.adapter

import android.graphics.BitmapFactory
import android.util.Base64
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.domain.entities.ArtikelEntity
import kotlinx.android.synthetic.main.list_item_artikel.view.*

class ArtikelViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(
        artikel: ArtikelEntity) {
        if(artikel.image!="false"){
            val imageBytes = Base64.decode(artikel.image, Base64.DEFAULT)
            val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            itemView.ivBerita.setImageBitmap(decodedImage)
        }
        itemView.tvJudul.text = artikel.name
    }
}