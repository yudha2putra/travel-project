package com.tiomaz.trans.presentation.listener

import com.tiomaz.trans.domain.entities.Reason

interface ReasonListener {
    fun onReasonClicked(data:Reason)
}