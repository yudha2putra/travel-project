package com.tiomaz.trans.presentation.reservasi.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.domain.entities.LoketEntity
import kotlinx.android.synthetic.main.list_item_loket.view.*

class LoketViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(
        loket: LoketEntity) {
        itemView.tvNama.text = loket.name
        itemView.tvAlamat.text = loket.alamat
        itemView.tvTelp.text = loket.telp
    }
}