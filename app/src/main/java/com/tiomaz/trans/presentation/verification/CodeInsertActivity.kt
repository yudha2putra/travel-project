package com.tiomaz.trans.presentation.verification

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.mukesh.OtpView
import com.tiomaz.trans.R
import com.tiomaz.trans.presentation.profile.CreateProfileActivity
import kotlinx.android.synthetic.main.activity_code_insert.*


class CodeInsertActivity : AppCompatActivity(){
    private lateinit var otpView:OtpView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code_insert)

        btnNext.setOnClickListener {
            val intent = Intent(this, CreateProfileActivity::class.java)
            startActivity(intent)
        }
        otpView = findViewById(R.id.otp_view)
        otpView.setOtpCompletionListener {
            Log.d("onOtpCompleted", it)
        }
    }
}