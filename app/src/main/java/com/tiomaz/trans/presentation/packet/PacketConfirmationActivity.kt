package com.tiomaz.trans.presentation.packet

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_packet_confirmation.*

class PacketConfirmationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packet_confirmation)

        btnNext.setOnClickListener {
            val intent = Intent(this, PacketStatusActivity::class.java)
            startActivity(intent)
        }

        ivBack.setOnClickListener {
            finish()
        }
    }
}