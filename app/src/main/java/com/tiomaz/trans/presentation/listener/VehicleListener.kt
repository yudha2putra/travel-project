package com.tiomaz.trans.presentation.listener

import com.tiomaz.trans.domain.entities.Vehicle

interface VehicleListener {
    fun onVehicleClicked(data:Vehicle)
}