package com.tiomaz.trans.presentation.reservasi

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.*
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.tiomaz.trans.R
import com.tiomaz.trans.common.AESUtils
import com.tiomaz.trans.common.BaseFragmentActivity
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.common.makeVisible
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PassengerEntity
import com.tiomaz.trans.utils.extentions.emptyString
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.list_item_name.*
import org.json.JSONArray
import org.json.JSONObject
import java.nio.charset.Charset
import java.util.*
import kotlin.math.roundToInt

const val REQUEST_CODE = 101
const val AUTOCOMPLETE_REQUEST_CODE = 102




class MapsActivity : BaseFragmentActivity(), OnMapReadyCallback, LocationListener {
    private var mMap: GoogleMap? = null
    private var marker: Marker? = null
    private lateinit var currentLocation: Location
    private var addressGps = emptyString()
    private var lokasi = emptyString()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var schoolLocation = LatLng(-6.3335736, 106.715938)
    lateinit var booking: BookingEntity
    lateinit var passenger: PassengerEntity
    private var arrayke = 0
    private var radius = 0
    private var text = "Lokasi km Dari Loket, Charge jemput Rp"
    private var pickupPrice = 0.00
    private var deliverPrice = 0.00

    override val layout: Int = R.layout.activity_map

    override fun onPreparation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        val dec = hexStringToByteArray(resources.getString(R.string.google_maps_encrypt))
        val enc = dec.let { AESUtils.decrypt(it) }
        Places.initialize(applicationContext, enc.toString(Charset.defaultCharset()))
        val autocompleteFragment: AutocompleteSupportFragment? =
            supportFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as AutocompleteSupportFragment?
        autocompleteFragment?.setPlaceFields(listOf(Place.Field.ID, Place.Field.NAME))
    }

    override fun onIntent() {
        booking = intent.getSerializableExtra("booking") as BookingEntity
        passenger = intent.getSerializableExtra("passenger") as PassengerEntity
        arrayke = intent.getIntExtra("arrayke",0)
        lokasi = intent.getStringExtra("lokasi").toString()

        Log.d("print",lokasi.toString())
        if (lokasi=="jemput"){
            schoolLocation = LatLng(booking.keberangkatanLat.toDouble(),booking.keberangkatanLong.toDouble())
            if (booking.keberangkatanRadius!=""){
                radius = booking.keberangkatanRadius.toInt()*1000
            }
        }
        else{
            schoolLocation = LatLng(booking.tujuanLat.toDouble(),booking.tujuanLong.toDouble())
            if (booking.tujuanRadius!=""){
                radius = booking.tujuanRadius.toInt()*1000
            }
        }


        val latitude = intent.getStringExtra("latitude")
        val longitude = intent.getStringExtra("longitude")
        if (latitude != null && longitude != null) {
            val latLng = LatLng(latitude.toDouble(), longitude.toDouble())
            mMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
            val center = mMap?.cameraPosition?.target
            if (center != null) {
                marker?.position = center
            }
        }
    }

    override fun onUi() {
    }

    override fun onAction() {
        btnSimpan.setOnClickListener {

            if(lokasi=="jemput"){

                val LocA = Location(LocationManager.GPS_PROVIDER).apply {
                    latitude = schoolLocation.latitude
                    longitude = schoolLocation.longitude
                }
                val LocB = Location(LocationManager.GPS_PROVIDER).apply {
                    latitude = marker?.position?.latitude!!
                    longitude = marker?.position?.longitude!!
                }
                text = "Lokasi " + String.format("%.1f",LocA.distanceTo(LocB)/1000) +" km Dari Loket, Charge jemput Rp"
                Log.d("print",booking.keberangkatanRadius.toString())
                if (LocA.distanceTo(LocB)/1000<booking.keberangkatanRadius.toInt()){
                    doJemput(LocA.distanceTo(LocB)/1000)
                }
                else{
                    Toast.makeText(applicationContext,"Lokasi jemput terlalu jauh",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            else{

                val LocA = Location(LocationManager.GPS_PROVIDER).apply {
                    latitude = schoolLocation.latitude
                    longitude = schoolLocation.longitude
                }
                val LocB = Location(LocationManager.GPS_PROVIDER).apply {
                    latitude = marker?.position?.latitude!!
                    longitude = marker?.position?.longitude!!
                }
                Log.d("print",booking.tujuanRadius.toString())
                text = "Lokasi " + String.format("%.1f",LocA.distanceTo(LocB)/1000) +" km Dari Loket, Charge jemput Rp"
                if (LocA.distanceTo(LocB)/1000<booking.tujuanRadius.toInt()){
                    doAntar(LocA.distanceTo(LocB)/1000)

                }
                else{
                    Toast.makeText(applicationContext,"Lokasi antar terlalu jauh",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

//            intent.putExtra("latitude", marker?.position?.latitude.toString())
//            intent.putExtra("longitude", marker?.position?.longitude.toString())

//            intent.putExtra("addressGps", addressGps)

        }

        ivBack.setOnClickListener {
            onBackPressed()
        }

        etSearch.setOnClickListener {
            onSearchCalled()
        }

        ivClear.setOnClickListener {
            etSearch.setText("")
        }
    }

    override fun onObserver() {
        fetchLocation()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        val latLng = LatLng(currentLocation.latitude, currentLocation.longitude)
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(schoolLocation))
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(schoolLocation, 15f))
        val center = mMap?.cameraPosition?.target
        marker = mMap?.addMarker(
            MarkerOptions().position(center!!)
                .icon(bitmapDescriptorFromVector())
        )


        mMap?.setOnCameraMoveListener {
            val currentCenter = mMap?.cameraPosition?.target
            if (currentCenter != null) {
                marker?.position = currentCenter
            }

        }
        mMap?.setOnCameraIdleListener {
            getAddressName()
        }

        showSchoolLocation()

    }

    private fun bitmapDescriptorFromVector(
    ): BitmapDescriptor? {
        val vectorDrawable =
            ContextCompat.getDrawable(applicationContext, R.drawable.ic_map_pin_small)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onLocationChanged(location: Location) {
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    private fun fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE
            )
            return
        }
        val task: Task<Location> = fusedLocationProviderClient.lastLocation
        task.addOnSuccessListener { location ->
            if (location != null) {
                currentLocation = location
                Toast.makeText(applicationContext,currentLocation.latitude.toString() + "" + currentLocation.longitude,
                    Toast.LENGTH_SHORT
                ).show()
                val supportMapFragment =
                    (supportFragmentManager.findFragmentById(R.id.mymap) as SupportMapFragment?)!!
                supportMapFragment.getMapAsync(this@MapsActivity)

            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchLocation()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    Toast.makeText(
                        applicationContext,
                        "ID: " + place.id + "address:" + place.address + "Name:" + place.name + " latlong: " + place.latLng,
                        Toast.LENGTH_LONG
                    ).show()
                    mMap?.moveCamera(CameraUpdateFactory.newLatLng(place.latLng))
                    marker?.position = place.latLng
                    etSearch.setText(place.address)
                }
            }
        }
    }

    private fun onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        val fields = listOf(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.ADDRESS,
            Place.Field.LAT_LNG
        )
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        ).setCountry("ID") //INDONESIA
            .build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    private fun getAddressName() {
        val geo = Geocoder(this.applicationContext, Locale.getDefault())
        val latitude = ((mMap?.cameraPosition?.target?.latitude!! * 1E4).roundToInt()) / 1E4
        val longitude = ((mMap?.cameraPosition?.target?.longitude!! * 1E4).roundToInt()) / 1E4
        val addresses: List<Address> = geo.getFromLocation(latitude, longitude, 1)
        tvAddress.makeVisible()
        if (addresses.isEmpty()) {
            tvAddress.text = "Waiting for location"
        } else {
            if (addresses.isNotEmpty()) {
                addressGps = addresses[0].getAddressLine(0)
                tvAddress.text = addressGps
            }
        }
    }

    private fun hexStringToByteArray(s: String): ByteArray {
        val len = s.length
        val data = ByteArray(len / 2)
        var i = 0
        while (i < len) {
            data[i / 2] = ((Character.digit(
                s[i],
                16
            ) shl 4) + Character.digit(s[i + 1], 16)).toByte()
            i += 2
        }
        return data
    }

    private fun showSchoolLocation(){
        var circle: Circle? = null
        val markerOptions = MarkerOptions().position(schoolLocation).title("Ini Loket")
        mMap?.addMarker(markerOptions)
        val circleOptions = CircleOptions()
            .center(schoolLocation)
            .radius(radius.toDouble())
            .strokeWidth(0F)
            .fillColor(0x220000FF)
        circle?.remove() // Remove old circle.
        mMap?.addCircle(circleOptions) // Draw new circle.

    }

    private fun doJemput(jarak:Float) {

        val url = "https://system.tiomaz.com/harga_jemput"
        val params = JSONObject()
        params.put("booking_line_id", JSONArray().put(passenger.idSeat))
        params.put("subroute_id", JSONArray().put(passenger.routeID))
        params.put("jarak_jemput_km", jarak)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    if(response.has("result")) {
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if(arrayData.getJSONObject(0).has("pickup_price")) {
                            pickupPrice = arrayData.getJSONObject(0).getDouble("pickup_price")
                            text += String.format("%.0f",pickupPrice)
                            val intent = Intent()
                            intent.putExtra("longitude", addressGps)
                            intent.putExtra("jarakJemput",text)
                            intent.putExtra("pickupPrice",pickupPrice)
                            intent.putExtra("arrayke", arrayke)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                        else{
                            text += 0
                            val intent = Intent()
                            intent.putExtra("longitude", addressGps)
                            intent.putExtra("jarakJemput",text)
                            intent.putExtra("pickupPrice",pickupPrice)
                            intent.putExtra("arrayke", arrayke)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }

                    }

                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }

    private fun doAntar(jarak:Float) {

        val url = "https://system.tiomaz.com/harga_antar"
        val params = JSONObject()
        params.put("booking_line_id", JSONArray().put(passenger.idSeat))
        params.put("subroute_id", JSONArray().put(passenger.routeID))
        params.put("jarak_antar_km", jarak)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    if(response.has("result")) {
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if(arrayData.getJSONObject(0).has("deliver_price")) {
                            deliverPrice = arrayData.getJSONObject(0).getDouble("deliver_price")
                            text += String.format("%.0f", deliverPrice)
                            val intent = Intent()
                            intent.putExtra("latitude", addressGps)
                            intent.putExtra("jarakAntar", text)
                            intent.putExtra("deliverPrice",deliverPrice)
                            intent.putExtra("arrayke", arrayke)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                        else{
                            text += 0
                            val intent = Intent()
                            intent.putExtra("latitude", addressGps)
                            intent.putExtra("jarakAntar", text)
                            intent.putExtra("deliverPrice",deliverPrice)
                            intent.putExtra("arrayke", arrayke)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                    }

                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }

}