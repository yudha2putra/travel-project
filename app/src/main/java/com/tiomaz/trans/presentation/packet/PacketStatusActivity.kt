package com.tiomaz.trans.presentation.packet

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import com.tiomaz.trans.presentation.beranda.HomeActivity
import kotlinx.android.synthetic.main.activity_packet_status.*

class PacketStatusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packet_status)

        btnHome.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
        }

    }
}