package com.tiomaz.trans.presentation.reservasi

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.tiomaz.trans.R
import com.tiomaz.trans.common.GetLocation
import com.tiomaz.trans.domain.entities.BookingEntity
import kotlinx.android.synthetic.main.activity_maps.*

class MapsActivity2 : AppCompatActivity(), OnMapReadyCallback, LocationListener {
    private var map: GoogleMap? = null
    private var cameraPosition: CameraPosition? = null
    private var defaultLocation = LatLng(-6.3352927, 106.7129034)
    private var schoolLocation = LatLng(-6.3335736, 106.715938)
    private var lastKnownLocation: Location? = null
    private var locationPermissionGranted = false
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var locationManager: LocationManager? = null
    private var getLocation : GetLocation? = null
    private lateinit var context : Context
    private var provider: String? = ""
    private var push_notification: String? = null
    lateinit var booking: BookingEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context = applicationContext
        booking = intent.getSerializableExtra("booking") as BookingEntity
        schoolLocation = LatLng(booking.keberangkatanLat.toDouble(),booking.keberangkatanLong.toDouble())
        defaultLocation = LatLng(booking.keberangkatanLat.toDouble(),booking.keberangkatanLong.toDouble())

        if (savedInstanceState != null) {
            lastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION)
            cameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
        }

        setContentView(R.layout.activity_maps)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        ivBack.setOnClickListener {
            onBackPressed()
        }

        etSearch.setOnClickListener {
            onSearchCalled()
        }

        ivClear.setOnClickListener {
            etSearch.setText("")
        }

    }

    override fun onMapReady(map: GoogleMap) {
        this.map = map

        setMapStyle()

        getLocationPermission()

        updateLocationUI()

        getDeviceLocation()

        showSchoolLocation()

    }

    @SuppressLint("ResourceType")
    private fun setMapStyle(){
        try {
            val success: Boolean? = map?.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,R.raw.style_json))
            if (!success!!) {
                Log.e(TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(TAG, "Can't find style. Error: ", e)
        }
    }

    override fun onLocationChanged(location: Location) {

        Log.e("Location", location.getProvider() + "==" + location.getAccuracy() + "==" + location.getAltitude() + "==" + location.getLatitude() + "==" + location.getLongitude());
        getLocation?.onLocationChanged(location)
        lastKnownLocation = location
    }

    private fun showSchoolLocation(){
        var circle: Circle? = null
        val markerOptions = MarkerOptions().position(schoolLocation).title("Ini Loket")
        map?.addMarker(markerOptions)
        val circleOptions = CircleOptions()
            .center(schoolLocation)
            .radius(radius.toDouble())
            .strokeWidth(0F)
            .fillColor(0x220000FF)
        circle?.remove() // Remove old circle.
        map?.addCircle(circleOptions) // Draw new circle.

    }

    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            map?.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(defaultLocation, DEFAULT_ZOOM.toFloat()))
                        }

                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.")
                        Log.e(TAG, "Exception: %s", task.exception)
                        map?.moveCamera(CameraUpdateFactory
                            .newLatLngZoom(defaultLocation, DEFAULT_ZOOM.toFloat()))
                        map?.uiSettings?.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun updateLocationUI() {
        if (map == null) {
            return
        }
        try {
            if (locationPermissionGranted) {
                map!!.isMyLocationEnabled = true
                map!!.uiSettings?.isMyLocationButtonEnabled = true
            } else {
                map?.isMyLocationEnabled = false
                map?.uiSettings?.isMyLocationButtonEnabled = false
                lastKnownLocation = null
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true

            if(locationPermissionGranted==true){
                locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager
                val criteria = Criteria()
                provider = locationManager!!.getBestProvider(criteria, false)

            }
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }


        locationManager!!.requestLocationUpdates(provider!!, 5000, 1.0F, this)


    }

    private fun onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        val fields = listOf(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.ADDRESS,
            Place.Field.LAT_LNG
        )
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(
            AutocompleteActivityMode.OVERLAY, fields
        ).setCountry("ID") //INDONESIA
            .build(this)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
    }

    companion object {
        const val AUTOCOMPLETE_REQUEST_CODE = 102
        private val TAG = MapsActivity2::class.java.simpleName
        private const val DEFAULT_ZOOM = 15
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
        private const val radius = 15000
        private const val KEY_CAMERA_POSITION = "camera_position"
        private const val KEY_LOCATION = "location"
    }
}