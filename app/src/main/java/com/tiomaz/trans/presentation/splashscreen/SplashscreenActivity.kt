package com.tiomaz.trans.presentation.splashscreen

import android.os.Handler
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseActivity
import com.tiomaz.trans.presentation.main.MainActivity

class SplashscreenActivity : BaseActivity(){

    private var handler: Handler = Handler()

    override val layout: Int = R.layout.activity_splashscreen

    override fun onPreparation() {
    }

    override fun onIntent() {
    }

    override fun onUi() {
        handler = Handler()
        handler.postDelayed({
            toNextActivity()
        }, 3000)
    }

    override fun onAction() {
    }

    override fun onObserver() {
    }

    private fun toNextActivity() {
//        PesanTiketActivity.start(this)
//        finish()
        MainActivity.start(this)
        finish()
    }
}