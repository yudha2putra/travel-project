package com.tiomaz.trans.presentation.confirmation

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.common.makeVisible
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PassengerEntity
import com.tiomaz.trans.presentation.payment.PaymentMethodActivity
import com.tiomaz.trans.presentation.payment.TagihanActivity
import com.tiomaz.trans.presentation.reservasi.adapter.ConfirmationAdapter
import com.tiomaz.trans.presentation.reservasi.adapter.PassengerNameAdapter
import kotlinx.android.synthetic.main.activity_confirmation.*
import kotlinx.android.synthetic.main.activity_confirmation.btnNext
import kotlinx.android.synthetic.main.activity_confirmation.tvKeberangkatan
import kotlinx.android.synthetic.main.activity_confirmation.tvPenumpang
import kotlinx.android.synthetic.main.activity_confirmation.tvTanggal
import kotlinx.android.synthetic.main.activity_confirmation.tvTujuan
import kotlinx.android.synthetic.main.activity_confirmation.tvjam
import org.json.JSONArray
import org.json.JSONObject

class ConfirmationActivity : AppCompatActivity() {

    lateinit var booking: BookingEntity
    lateinit var passenger: MutableList<PassengerEntity>
    lateinit var confirmationAdapter: ConfirmationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        booking = intent.getSerializableExtra("booking") as BookingEntity
        passenger = intent.getSerializableExtra("passenger") as ArrayList<PassengerEntity>
        confirmationAdapter = ConfirmationAdapter(this, passenger)
        confirmationAdapter.notifyDataSetChanged()

        tvKeberangkatan.setText(booking.keberangkatan)
        tvjam.setText("Pukul " + booking.jam)
        tvTujuan.setText(booking.tujuan)
        tvTanggal.setText(booking.tanggal)
        tvPenumpang.setText(booking.penumpang + " Penumpang")

        rvPenumpang.apply {
            adapter = confirmationAdapter
            layoutManager = LinearLayoutManager(context)
        }


        btnNext.setOnClickListener {
            val url = "https://system.tiomaz.com/update_transaction_bookingline"

            val array = JSONArray()
            for (i in 0 until passenger.size) {
                val bookingLine = JSONObject()
                bookingLine.put("id", passenger[i].idSeat)
                bookingLine.put("nama_penumpang", passenger[i].name)
                bookingLine.put("partner_id", passenger[i].partnerID)
                bookingLine.put("sub_route", passenger[i].routeID)
                bookingLine.put("jemput_ok", passenger[i].jemput)
                bookingLine.put("antar_ok", passenger[i].antar)
                bookingLine.put("longitude", passenger[i].longitude)
                bookingLine.put("latitude", passenger[i].latitude)
                bookingLine.put("pickup_price", passenger[i].pickupPrice)
                bookingLine.put("deliver_price", passenger[i].deliverPrice)
                array.put(bookingLine)
            }

            val params = JSONObject()
            params.put("booking_order_id", booking.idJam)
            params.put("booking_line", array)

            Log.d("print",params.toString())

            val request = JsonObjectRequest(
                Request.Method.POST,url,params,
                { response ->
                    // Process the json
                    try {
                        pb.makeGone()
                        Log.d("print",response.toString())
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("result")
                        val arrayData = JSONArray(jsonResult2)
                        if(arrayData.length()!=0){
//                            Toast.makeText(this,"Booking Sukses", Toast.LENGTH_SHORT).show()
                            val kodeBook = arrayData.getJSONObject(0).getString("kode_book")
                            booking.kodeBook=kodeBook
                            val intent = Intent(this, TagihanActivity::class.java)
                            intent.putExtra("passenger", passenger as ArrayList<PassengerEntity>)
                            intent.putExtra("booking", booking)
                            startActivity(intent)
                        }
                        else{
                            Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                        }

                    }catch (e:Exception){
                        Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
                    }

                }, {
                    // Error in request
                    Toast.makeText(this,it.toString(), Toast.LENGTH_SHORT).show()
                })


            // Volley request policy, only one time request to avoid duplicate transaction
            request.retryPolicy = DefaultRetryPolicy(10000,0,1f)

            // Add the volley post request to the request queue
            VolleySingleton.getInstance(this).addToRequestQueue(request)
            pb.makeVisible()

        }

        btnBack.setOnClickListener {
            finish()
        }


    }
}