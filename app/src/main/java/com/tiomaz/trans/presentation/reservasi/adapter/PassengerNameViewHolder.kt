package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapterEntity
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.RegionalEntity
import com.tiomaz.trans.presentation.reservasi.dialog.LoketDialog
import kotlinx.android.synthetic.main.list_item_kota.view.*
import kotlinx.android.synthetic.main.list_item_name.view.*

class PassengerNameViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(seat:Int,berangkat:String,tiba:String,tibaInfo:String,berangkatInfo:String) {

        itemView.noKursi.text = "Kursi Nomor "+seat
        itemView.etBerangkat.setText(berangkat)
        itemView.etTiba.setText(tiba)
        if(berangkatInfo!=""){
            itemView.berangkatInfo.text = berangkatInfo
        }
        if(tibaInfo!=""){
            itemView.tibaInfo.text = tibaInfo
        }

    }

}