package com.tiomaz.trans.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tiomaz.trans.domain.entities.Task
import com.tiomaz.trans.domain.usecase.contract.HistoryUseCase
import com.tiomaz.trans.utils.common.ResultState
import kotlinx.coroutines.launch
import java.io.File

class HistoryViewModel(private val useCase: HistoryUseCase) : ViewModel() {
    fun fetchDailyHistory(taskDate: String): MutableLiveData<ResultState<List<Task>>> {
        val fetchDailyHistory = MutableLiveData<ResultState<List<Task>>>()
        fetchDailyHistory.value = ResultState.Loading()
        viewModelScope.launch {
            val fetchDailyHistoryResponse = useCase.fetchDailyHistory(taskDate)
            fetchDailyHistory.value = fetchDailyHistoryResponse
        }
        return fetchDailyHistory
    }


    fun fetchDetailHistory(taskId: Int): MutableLiveData<ResultState<Task>> {
        val fetchDetailHistory = MutableLiveData<ResultState<Task>>()
        fetchDetailHistory.value = ResultState.Loading()
        viewModelScope.launch {
            val fetchDetailHistoryResponse = useCase.fetchDetailHistory(taskId.toString())
            fetchDetailHistory.value = fetchDetailHistoryResponse
        }
        return fetchDetailHistory
    }

    fun postParkingTicket(taskId: Int, image:File): MutableLiveData<ResultState<Boolean>> {
        val postParkingTicket = MutableLiveData<ResultState<Boolean>>()
        postParkingTicket.value = ResultState.Loading()
        viewModelScope.launch {
            val postParkingTicketResponse = useCase.postParkingTicket(taskId.toString(),image)
            postParkingTicket.value = postParkingTicketResponse
        }
        return postParkingTicket
    }
}