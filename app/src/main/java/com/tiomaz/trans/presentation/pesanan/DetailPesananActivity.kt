package com.tiomaz.trans.presentation.pesanan

import android.content.*
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.gkemon.XMLtoPDF.PdfGenerator
import com.gkemon.XMLtoPDF.PdfGeneratorListener
import com.gkemon.XMLtoPDF.model.FailureResponse
import com.gkemon.XMLtoPDF.model.SuccessResponse
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.domain.entities.PesananEntity
import com.tiomaz.trans.presentation.order.OrderActivity
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import kotlinx.android.synthetic.main.activity_order_detail.*
import kotlinx.android.synthetic.main.activity_order_detail.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


class DetailPesananActivity :AppCompatActivity(){
    lateinit var pesanan: PesananEntity
    lateinit var content:View
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        content = getLayoutInflater().inflate(R.layout.activity_order_detail, null)
        setContentView(content)
        pesanan = intent.getSerializableExtra("pesanan") as PesananEntity
        val from = intent.getStringExtra("from")
        if(from=="RIWAYAT"){
            content.btnBatal.makeGone()
        }
        content.tvKodeBooking.text = pesanan.kodeBooking
        content.tvSeat.text = "SEAT "+ pesanan.seat
        content.tvNama.text = pesanan.name
        content.tvKeberangkatan.text = pesanan.dari
        content.tvTujuan.text = pesanan.tujuan
        content.tvTanggal.text = changeDate(pesanan.tanggal)
        content.tvjam.text = getTime(pesanan.tanggal)

        val decim = DecimalFormat("Rp#,###")
        content.tvHargaTiket.text = decim.format(pesanan.hargaTiket.toFloat().toInt())
        content.tvJumlahPenumpang.text = pesanan.jumlahPenumpang
        content.tvChargeJemput.text = decim.format(pesanan.chargeJemput.toFloat().toInt())
        content.tvChargeAntar.text = decim.format(pesanan.chargeAntar.toFloat().toInt())
        content.tvSubtotal.text = decim.format(pesanan.subtotal.toFloat().toInt())
        content.tvDiskon.text = decim.format(pesanan.diskon.toFloat().toInt())
        content.tvTotal.text = decim.format(pesanan.total.toFloat().toInt())

        getDifferences()

        if (pesanan.statusBayar=="true"){
            content.lunas.makeVisible()
            content.menunggu.makeGone()
            content.VA.makeGone()
            if (getDifferences()<24){
                content.btnBatal.makeGone()
            }
        }
        else{
            content.lunas.makeGone()
            content.menunggu.makeVisible()
            if(pesanan.trx!="false"){
                content.tvVA.text = "No. VA :"+pesanan.trx
            }
            else{
                content.tvVA.text = "No. VA : -"
            }
        }

        btnBatal.setOnClickListener {
            AlertDialog.Builder(this)
                .setTitle("Apakah anda ingin membatalkan pesanan?")
                .setPositiveButton("Iya",
                    DialogInterface.OnClickListener { dialog, which ->
                        doBatal()
                    })
                .setNegativeButton("Tidak",
                    DialogInterface.OnClickListener { dialog, which ->

                    })
                .show()

        }
        ivClose.setOnClickListener {
            if(from=="RIWAYAT"){
                finish()
            }
            else{
                finish()
                val intent = Intent(this, OrderActivity::class.java)
                intent.putExtra("from","Pesanan Saya")
                startActivity(intent)
            }

        }

        salin.setOnClickListener {
            copyToClipboard(pesanan.trx)
            Toast.makeText(this,"Nomor VA Berhasil di Copy", Toast.LENGTH_SHORT).show()
        }

        download.setOnClickListener {
            createPdf()
        }

    }

    fun changeDate(time:String):String{
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val outputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        val date = inputFormat.parse(time);
        val str = outputFormat.format(date);
        return str
    }
    fun getTime(time:String):String{
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val outputPattern = "HH:mm"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        val date = inputFormat.parse(time);
        val str = outputFormat.format(date);
        return str
    }
    fun getDifferences():Int{
        val inputPattern = "yyyy-MM-dd HH:mm:ss"
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val currentDateandTime: String = sdf.format(Date())
        val fmt = SimpleDateFormat(inputPattern, Locale.getDefault())
        val bgn = fmt.parse(currentDateandTime)
        val end = fmt.parse(pesanan.tanggal)
        val milliseconds = end.time - bgn.time
        val hours = milliseconds / 1000 / 3600
        return  hours.toInt()
    }

    private fun createPdf() {

        PdfGenerator.getBuilder()
            .setContext(this)
            .fromViewSource()
            .fromView(content)
            .setFileName(pesanan.kodeBooking)
            .setFolderNameOrPath("tiomaz/")
            .actionAfterPDFGeneration(PdfGenerator.ActionAfterPDFGeneration.OPEN)
            .build(object : PdfGeneratorListener() {
                override fun onFailure(failureResponse: FailureResponse) {
                    super.onFailure(failureResponse)
                }

                override fun showLog(log: String) {
                    super.showLog(log)
                }

                override fun onStartPDFGeneration() {
                    /*When PDF generation begins to start*/
                }

                override fun onFinishPDFGeneration() {
                    /*When PDF generation is finished*/
                }

                override fun onSuccess(response: SuccessResponse) {
                    super.onSuccess(response)
                }
            })
    }

    private fun doBatal() {
        val url = "https://system.tiomaz.com/cancel_booking_line"
        val params = JSONObject()
        params.put("kode_booking", pesanan.kodeBooking)
        params.put("status_pembayaran",pesanan.statusBayar)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if(arrayData.length()!=0){
                            Toast.makeText(this,"Booking Dibatalkan", Toast.LENGTH_SHORT).show()
                            finish()
                            val intent = Intent(this, OrderActivity::class.java)
                            intent.putExtra("from","Pesanan Saya")
                            startActivity(intent)
                        }
                    }
                    else{
                        Toast.makeText(this,"Booking Gagal Dibatalkan",Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    override fun onBackPressed() {
        val from = intent.getStringExtra("from")
        if(from=="RIWAYAT"){
            content.btnBatal.makeGone()
            finish()
        }
        else{
            finish()
            val intent = Intent(this, OrderActivity::class.java)
            intent.putExtra("from","Pesanan Saya")
            startActivity(intent)
        }

        super.onBackPressed()
    }

    fun Context.copyToClipboard(text: CharSequence){
        val clipboard = ContextCompat.getSystemService(this, ClipboardManager::class.java)
        clipboard?.setPrimaryClip(ClipData.newPlainText("",text))
    }

}