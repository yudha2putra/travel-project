package com.tiomaz.trans.presentation.call

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseFragment
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.common.makeVisible
import kotlinx.android.synthetic.main.activity_notification.*
import org.json.JSONArray
import org.json.JSONObject


class CallFragment : BaseFragment() {
    var phone=""
    override val layout: Int = R.layout.activity_notification

    override fun onPreparation() {
        doBantuan()
    }

    override fun onIntent() {
    }

    override fun onUi() {
    }

    override fun onAction() {
        btnHubungi.setOnClickListener {
            Log.d("print",phone.toString())
            val url = "https://api.whatsapp.com/send?phone=+62" + phone
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }

        ig.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/tiomaztrans/?hl=id"))
            startActivity(browserIntent)
        }
        twitter.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/tiomaztrans/?hl=id"))
            startActivity(browserIntent)
        }
        facebook.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/tiomaztrans"))
            startActivity(browserIntent)
        }

    }

    override fun onObserver() {
    }

    fun doBantuan(){

        val url = "https://system.tiomaz.com/res_company"

        val json = JSONObject()
        Log.d("print",json.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("result")
                        val arrayData = JSONArray(jsonResult2)
                        phone = arrayData.getJSONObject(0).getString("whatshapp")
                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
//                Log.d("print",it.networkResponse.statusCode.toString())
//                Log.d("print",String(it.networkResponse.data, StandardCharsets.UTF_8))
            })



        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(20000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context!!).addToRequestQueue(request)
        pb.makeVisible()
    }
}