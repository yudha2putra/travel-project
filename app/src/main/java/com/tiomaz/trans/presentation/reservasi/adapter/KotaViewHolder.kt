package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.domain.entities.KotaEntity
import kotlinx.android.synthetic.main.list_item_kota.view.*

class KotaViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(
        kota: KotaEntity,
        position: Int,
        adapter: KotaAdapter) {
        itemView.tvKota.text = kota.name


    }
}