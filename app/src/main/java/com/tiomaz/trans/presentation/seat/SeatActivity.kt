package com.tiomaz.trans.presentation.seat

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.common.makeVisible
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PassengerEntity
import com.tiomaz.trans.presentation.confirmation.ConfirmationActivity
import com.tiomaz.trans.presentation.payment.TagihanActivity
import com.tiomaz.trans.presentation.reservasi.PassengerActivity
import com.tiomaz.trans.utils.extentions.onSingleClickListener
import kotlinx.android.synthetic.main.activity_confirmation.*
import kotlinx.android.synthetic.main.activity_seat.*
import kotlinx.android.synthetic.main.activity_seat.btnNext
import kotlinx.android.synthetic.main.activity_seat.pb
import kotlinx.android.synthetic.main.activity_seat.tvKeberangkatan
import kotlinx.android.synthetic.main.activity_seat.tvPenumpang
import kotlinx.android.synthetic.main.activity_seat.tvTanggal
import kotlinx.android.synthetic.main.activity_seat.tvTujuan
import kotlinx.android.synthetic.main.activity_seat.tvjam
import org.json.JSONArray
import org.json.JSONObject


class SeatActivity : AppCompatActivity() {

    lateinit var booking: BookingEntity
    lateinit var idSeatSelected: MutableList<Int>
    var selected = intArrayOf(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
    var idSeat = intArrayOf(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seat)

        var selectedCounter = 0
        idSeatSelected = mutableListOf()

        booking = intent.getSerializableExtra("booking") as BookingEntity
        val penumpang = booking.penumpang.toInt()
        sisa.setText(intent.getStringExtra("kapasitas").toString()+" seat yang dipilih")
        tvKeberangkatan.setText(booking.keberangkatan)
        tvjam.setText("Pukul "+booking.jam)
        tvTujuan.setText(booking.tujuan)
        tvTanggal.setText(booking.tanggal)
        tvPenumpang.setText(booking.penumpang+" Penumpang")

        btnNext.setOnClickListener {
            idSeatSelected.clear()
            for (i in 0 until selected.size) {
                if (selected[i]==1){
                    idSeatSelected.add(idSeat[i])
                }
            }
//            doCheckSeat()
            doUpdate()
            val passengerActivity = Intent(this, PassengerActivity::class.java)
            passengerActivity.putExtra("booking", booking)
            passengerActivity.putExtra("seat",selected)
            passengerActivity.putExtra("idSeat",idSeat)
            startActivity(passengerActivity)

        }
        ivBack.setOnClickListener {
            finish()
        }

        val sharedPreference =  getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()
        editor.putInt("idJam",booking.idJam)
        editor.commit()

        val url = "https://system.tiomaz.com/list_transaction_bookingorder"
        val params = JSONObject()
        params.put("id", booking.idJam)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("result")
                    val arrayData = JSONArray(jsonResult2).getJSONObject(0)
                    val bookID = JSONArray(arrayData.getString("book_id"))
                    Log.d("print",bookID.toString())
                    if(bookID.length()!=0) {
//                        Toast.makeText(this, "Seat Sukses", Toast.LENGTH_SHORT).show()

                        for (i in 1 until bookID.length()+1) {
                            val statusSeat = bookID.getJSONObject(i-1).getBoolean("status_seat")
                            idSeat[i] = bookID.getJSONObject(i-1).getInt("id")
                            val resourceName = "seat$i"
                            Log.d("print", resourceName.toString())
                            val resourceID = resources.getIdentifier(resourceName, "id",packageName)
                            val seat = findViewById<View>(resourceID) as TextView
                            if(statusSeat==true){
                                seat.makeVisible()
                                selected[i]=2
                                seat.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
                            }
                            else{
                                seat.makeVisible()
                                selected[i]=0
                                seat.setBackgroundResource(R.drawable.ic_buttonseat_available)
                            }
                        }

                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
                }

            }, {
                // Error in request
                Toast.makeText(this,it.toString(), Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()

        seat1.onSingleClickListener {
            if(selected[1]==1){
                seat1.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[1] = 0
                selectedCounter -= 1
            }
            else if(selected[1]==0 && selectedCounter < penumpang!!){
                seat1.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[1] = 1
                selectedCounter += 1
            }
            else if(selected[1]==2){
                seat1.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat2.onSingleClickListener {
            if(selected[2]==1){
                seat2.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[2] = 0
                selectedCounter -= 1
            }
            else if(selected[2]==0 && selectedCounter< penumpang!!){
                seat2.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[2] = 1
                selectedCounter += 1
            }
            else if(selected[2]==2){
                seat2.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat3.onSingleClickListener {
            if(selected[3]==1){
                seat3.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[3] = 0
                selectedCounter -= 1
            }
            else if(selected[3]==0 && selectedCounter< penumpang!!){
                seat3.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[3] = 1
                selectedCounter += 1
            }
            else if(selected[3]==2){
                seat3.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat4.onSingleClickListener {
            if(selected[4]==1){
                seat4.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[4] = 0
                selectedCounter -= 1
            }
            else if(selected[4]==0 && selectedCounter< penumpang!!){
                seat4.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[4] = 1
                selectedCounter += 1
            }
            else if(selected[4]==2){
                seat4.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat5.onSingleClickListener {
            if(selected[5]==1){
                seat5.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[5] = 0
                selectedCounter -= 1
            }
            else if(selected[5]==0 && selectedCounter< penumpang!!){
                seat5.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[5] = 1
                selectedCounter += 1
            }
            else if(selected[5]==2){
                seat5.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat6.onSingleClickListener {
            if(selected[6]==1){
                seat6.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[6] = 0
                selectedCounter -= 1
            }
            else if(selected[6]==0 && selectedCounter< penumpang!!){
                seat6.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[6] = 1
                selectedCounter += 1
            }
            else if(selected[6]==2){
                seat6.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat7.onSingleClickListener {
            if(selected[7]==1){
                seat7.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[7] = 0
                selectedCounter -= 1
            }
            else if(selected[7]==0 && selectedCounter< penumpang!!){
                seat7.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[7] = 1
                selectedCounter += 1
            }
            else if(selected[7]==2){
                seat7.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat8.onSingleClickListener {
            if(selected[8]==1){
                seat8.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[8] = 0
                selectedCounter -= 1
            }
            else if(selected[8]==0 && selectedCounter< penumpang!!){
                seat8.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[8] = 1
                selectedCounter += 1
            }
            else if(selected[8]==2){
                seat8.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat9.onSingleClickListener {
            if(selected[9]==1){
                seat9.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[9] = 0
                selectedCounter -= 1
            }
            else if(selected[9]==0 && selectedCounter< penumpang!!){
                seat9.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[9] = 1
                selectedCounter += 1
            }
            else if(selected[9]==2){
                seat9.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat10.onSingleClickListener {
            if(selected[10]==1){
                seat10.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[10] = 0
                selectedCounter -= 1
            }
            else if(selected[10]==0 && selectedCounter< penumpang!!){
                seat10.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[10] = 1
                selectedCounter += 1
            }
            else if(selected[10]==2){
                seat10.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat11.onSingleClickListener {
            if(selected[11]==1){
                seat11.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[11] = 0
                selectedCounter -= 1
            }
            else if(selected[11]==0 && selectedCounter< penumpang!!){
                seat11.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[11] = 1
                selectedCounter += 1
            }
            else if(selected[11]==2){
                seat11.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat12.onSingleClickListener {
            if(selected[12]==1){
                seat12.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[12] = 0
                selectedCounter -= 1
            }
            else if(selected[12]==0 && selectedCounter< penumpang!!){
                seat12.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[12] = 1
                selectedCounter += 1
            }
            else if(selected[12]==2){
                seat12.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat13.onSingleClickListener {
            if(selected[13]==1){
                seat13.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[13] = 0
                selectedCounter -= 1
            }
            else if(selected[13]==0 && selectedCounter< penumpang!!){
                seat13.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[13] = 1
                selectedCounter += 1
            }
            else if(selected[13]==2){
                seat13.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat14.onSingleClickListener {
            if(selected[14]==1){
                seat14.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[14] = 0
                selectedCounter -= 1
            }
            else if(selected[14]==0 && selectedCounter< penumpang!!){
                seat14.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[14] = 1
                selectedCounter += 1
            }
            else if(selected[14]==2){
                seat14.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
        seat15.onSingleClickListener {
            if(selected[15]==1){
                seat15.setBackgroundResource(R.drawable.ic_buttonseat_available)
                selected[15] = 0
                selectedCounter -= 1
            }
            else if(selected[15]==0 && selectedCounter< penumpang!!){
                seat15.setBackgroundResource(R.drawable.ic_buttonseat_selected)
                selected[15] = 1
                selectedCounter += 1
            }
            else if(selected[15]==2){
                seat15.setBackgroundResource(R.drawable.ic_buttonseat_unavailable)
            }
        }
    }

    fun doUpdate(){
        val url = "https://system.tiomaz.com/update_transaction_bookingline"
        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val partner = sharedPreference.getInt("partner",0)
        val routeID = sharedPreference.getInt("routeID",0)

        val array = JSONArray()
        for (i in 0 until booking.penumpang.toInt()) {
            val bookingLine = JSONObject()
            bookingLine.put("id", idSeatSelected[i])
            bookingLine.put("nama_penumpang", "test")
            bookingLine.put("partner_id", partner)
            bookingLine.put("sub_route", routeID)
            bookingLine.put("jemput_ok", "")
            bookingLine.put("antar_ok", "")
            bookingLine.put("longitude", "")
            bookingLine.put("latitude", "")
            array.put(bookingLine)
        }

        val params = JSONObject()
        params.put("booking_order_id", booking.idJam)
        params.put("booking_line", array)

        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("result")
                    val arrayData = JSONArray(jsonResult2)
                    if(arrayData.length()!=0){
//                            Toast.makeText(this,"Booking Sukses", Toast.LENGTH_SHORT).show()
//                        val kodeBook = arrayData.getJSONObject(0).getString("kode_book")
//                        booking.kodeBook=kodeBook
//                        val intent = Intent(this, TagihanActivity::class.java)
//                        intent.putExtra("passenger", passenger as ArrayList<PassengerEntity>)
//                        intent.putExtra("booking", booking)
//                        startActivity(intent)
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }

                }catch (e:Exception){
                    Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
                }

            }, {
                // Error in request
                Toast.makeText(this,it.toString(), Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(10000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    private fun doCheckSeat() {
        val url = "https://system.tiomaz.com/check_empty_seats"
        val params = JSONObject()
        params.put("booking_line_id", JSONArray(idSeatSelected))
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    if(response.has("result")) {
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        val data = arrayData.getJSONObject(0).getString("response_desc")
                        Toast.makeText(this,data.toString(), Toast.LENGTH_LONG).show()
                    }
                    else{
                        doUpdate()
                        val passengerActivity = Intent(this, PassengerActivity::class.java)
                        passengerActivity.putExtra("booking", booking)
                        passengerActivity.putExtra("seat",selected)
                        passengerActivity.putExtra("idSeat",idSeat)
                        startActivity(passengerActivity)
                    }

                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }
}