package com.tiomaz.trans.presentation.reservasi.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.icu.text.CaseMap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapter
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.JamEntity
import com.tiomaz.trans.domain.entities.LoketEntity
import com.tiomaz.trans.domain.entities.PesananEntity
import com.tiomaz.trans.presentation.pesanan.DetailPesananActivity
import com.tiomaz.trans.presentation.reservasi.TimeReservasiActivity
import com.tiomaz.trans.presentation.seat.SeatActivity
import kotlinx.android.synthetic.main.activity_reservasi.*
import java.util.ArrayList

class PesananAdapter(val context: Context, private val items: MutableList<PesananEntity>, val title:String) :
    RecyclerView.Adapter<PesananViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PesananViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_pesanan,parent,false)
        return PesananViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: PesananViewHolder, position: Int) {
        holder.bindTo(items[position])
        holder.itemView.setOnClickListener {
            val intent = Intent(context, DetailPesananActivity::class.java)
            intent.putExtra("pesanan", items[position])
            intent.putExtra("from",title)
            context.startActivity(intent)
        }
    }


}