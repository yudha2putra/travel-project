package com.tiomaz.trans.presentation.listener

interface BioDataFormListener{
    fun onBioDataFormCompleteListener()
}

interface VehicleDataFormListener{
    fun onVehicleDataFormCompleteListener()
}