package com.tiomaz.trans.presentation.tracking

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_tracking.*

class TrackingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tracking)

        cvPemesanan.setOnClickListener {
            val intent = Intent(this, DetailTrackingActivity::class.java)
            startActivity(intent)
        }
        ivBack.setOnClickListener {
            finish()
        }
    }
}