package com.tiomaz.trans.presentation.profile

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseActivity
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.presentation.verification.LoginActivity
import com.tiomaz.trans.utils.common.ResultState
import com.tiomaz.trans.utils.extentions.*
import kotlinx.android.synthetic.main.activity_create_profile.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class CreateProfileActivity : BaseActivity(){
    companion object {
        fun start(context: Context) {
            val starter = Intent(context, CreateProfileActivity::class.java)
            context.startActivity(starter)
        }
    }

    override val layout: Int = R.layout.activity_create_profile
    override fun onPreparation() {
    }

    override fun onIntent() {
    }

    override fun onObserver() {
    }

    override fun onUi() {
    }

    override fun onAction() {
        btnLogin.onSingleClickListener {
            LoginActivity.start(this)
        }
        btnDaftar.onSingleClickListener {
            inputCheck()
        }
    }

    private fun doRegister() {
        val url = "https://system.tiomaz.com/set_users"
        val params = JSONObject()
        params.put("name", etNama.text)
        params.put("email", etEmail.text)
        params.put("login", etEmail.text)
        params.put("phone", etPhone.text)
        params.put("password", etPassword.text)
        params.put("password_api", etPassword.text)
        val array = JSONArray()
        array.put(params)
        val json = JSONObject()
        json.put("data", array)
        Log.d("print",json.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("result")
                    val jsonData = JSONObject(jsonResult2).getString("data")
                    val arrayData = JSONArray(jsonData).getJSONObject(0)
                    if(arrayData.has("users_id")){
                        toLoginActivity()
                        Toast.makeText(this,"Register Sukses",Toast.LENGTH_SHORT).show()
                    }
                    else{
                        Toast.makeText(this,"Email Sudah Terdaftar",Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(10000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    private fun toLoginActivity() {
        LoginActivity.start(this)
        finishAffinity()
    }

    fun inputCheck(){
        if (etEmail.text.isNullOrEmpty()||etPhone.text.isNullOrEmpty()||etNama.text.isNullOrEmpty()){
            if (etEmail.text.toString()==""){
                etEmail.setError("Email tidak boleh kosong")
            }
            if(etPhone.text.toString()==""){
                etPhone.setError("Nomor tidak boleh kosong")
            }
            if(etNama.text.toString()==""){
                etNama.setError("Nama tidak boleh kosong")
            }
        }
        else{
            doRegister()
        }
    }

}