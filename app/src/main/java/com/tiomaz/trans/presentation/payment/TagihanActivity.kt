package com.tiomaz.trans.presentation.payment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PassengerEntity
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import kotlinx.android.synthetic.main.activity_tagihan.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat


class TagihanActivity : AppCompatActivity() {

    lateinit var booking: BookingEntity
    lateinit var passenger: MutableList<PassengerEntity>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tagihan)

        booking = intent.getSerializableExtra("booking") as BookingEntity
        passenger = intent.getSerializableExtra("passenger") as ArrayList<PassengerEntity>

        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val name = sharedPreference.getString("name","")
        var seat = ""

        tvNama.text=name
        tvKodeBooking.text=booking.kodeBook
        tvKeberangkatan.text=booking.keberangkatan
        tvTujuan.text=booking.tujuan
        tvTanggal.text=booking.tanggal
        tvjam.text=booking.jam

        for (i in 0 until passenger.size){
            seat += passenger[i].seat
        }
        tvSeat.text="Seat "+seat

        doPoint()
        doTagihan(false)


        btnNext.setOnClickListener {
            val intent = Intent(this, PaymentMethodActivity::class.java)
            intent.putExtra("passenger", passenger as ArrayList<PassengerEntity>)
            intent.putExtra("booking", booking)
            startActivity(intent)
        }

        btnPakai.setOnClickListener{
            doTagihan(true)
            btnPakai.makeGone()
            Toast.makeText(this,"Point Berhasil Ditukar", Toast.LENGTH_SHORT).show()
        }

    }

    private fun doPoint() {
        val url = "https://system.tiomaz.com/point_yang_dimiliki"
        val params = JSONObject()
        params.put("partner_id", passenger[0].partnerID)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        if(arrayData.length()!=0){
//                            Toast.makeText(this,"Point Berhasil Ditukar", Toast.LENGTH_SHORT).show()
                            val point = arrayData.getJSONObject(0).getInt("point_yang_dimiliki")
                            tvCoins.text = point.toString()
                        }
                    }
                    else{
                        Toast.makeText(this,"Email atau Password Salah",Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    fun doTagihan(point:Boolean){
        val url = "https://system.tiomaz.com/tukar_point"

        val params = JSONObject()
        params.put("booking_line_id", passenger[0].idSeat)
        params.put("partner_id", passenger[0].partnerID)
        params.put("kode_book", booking.kodeBook)
        params.put("total_penumpang", booking.penumpang.toInt())
        params.put("tukar_point", point)

        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("data")
                    val arrayData = JSONArray(jsonResult2)
                    if(arrayData.length()!=0){
//                        Toast.makeText(this,"Point Berhasil Ditukar", Toast.LENGTH_SHORT).show()
                        val hargaTiket = arrayData.getJSONObject(0).getInt("harga_tiket")
                        val jumlahPenumpang = arrayData.getJSONObject(0).getInt("jumlah_penumpang")
                        val chargeJemput = arrayData.getJSONObject(0).getInt("charge_jemput")
                        val chargeAntar = arrayData.getJSONObject(0).getInt("charge_antar")
                        val subTotal = arrayData.getJSONObject(0).getInt("sub_total")
                        val diskon = arrayData.getJSONObject(0).getInt("diskon")
                        val total = arrayData.getJSONObject(0).getInt("total")
                        val decim = DecimalFormat("Rp#,###")
                        tvHargaTiket.setText(decim.format(hargaTiket).toString())
                        tvJumlahPenumpang.setText(jumlahPenumpang.toString())
                        tvChargeJemput.setText(decim.format(chargeJemput).toString())
                        tvChargeAntar.setText(decim.format(chargeAntar).toString())
                        tvSubtotal.setText(decim.format(subTotal).toString())
                        tvDiskon.setText(decim.format(diskon).toString())
                        tvTotal.setText(decim.format(total).toString())
                        booking.total=total

//                        intent.putExtra("passenger", passenger as ArrayList<PassengerEntity>)
//                        intent.putExtra("booking", booking)
//                        startActivity(intent)
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }

                }catch (e:Exception){
                    Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
                }

            }, {
                // Error in request
                Toast.makeText(this,it.toString(), Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }


}