package com.tiomaz.trans.presentation.payment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_code_voucher.*

class CodeVoucherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code_voucher)
        ivClose.setOnClickListener {
            finish()
        }
    }
}