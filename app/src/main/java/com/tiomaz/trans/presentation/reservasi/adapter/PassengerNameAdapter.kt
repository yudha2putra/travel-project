package com.tiomaz.trans.presentation.reservasi.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapterEntity
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PassengerEntity
import com.tiomaz.trans.domain.entities.RegionalEntity
import com.tiomaz.trans.presentation.reservasi.MapsActivity
import com.tiomaz.trans.presentation.reservasi.dialog.LoketDialog
import kotlinx.android.synthetic.main.list_item_name.view.*

const val REQUEST_PIN_MAP = 1

class PassengerNameAdapter(val context: Context, private val items: MutableList<PassengerEntity>, val booking: BookingEntity) :
    RecyclerView.Adapter<PassengerNameViewHolder>() {

    var nama = items
    private lateinit var dialogRegional: LoketDialog
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PassengerNameViewHolder {

        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_name,parent,false)
        return PassengerNameViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: PassengerNameViewHolder, position: Int) {
        holder.bindTo(items[position].seat,items[position].longitude,items[position].latitude,items[position].latitudeInfo,items[position].longitudeInfo)
        holder.itemView.etNama.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                //blank
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                //blank;
            }

            override fun afterTextChanged(s: Editable) {
                nama[holder.absoluteAdapterPosition].name = s.toString()
            }
        })

        holder.itemView.etNomor.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                //blank
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                //blank;
            }

            override fun afterTextChanged(s: Editable) {
                nama[holder.absoluteAdapterPosition].nomor = s.toString()
            }
        })

        holder.itemView.cbJemput.setOnClickListener {
            if(holder.itemView.cbJemput.isChecked){
                nama[holder.absoluteAdapterPosition].jemput = true
                val intent = Intent(context, MapsActivity::class.java)
                intent.putExtra("lokasi","jemput")
                intent.putExtra("booking", booking)
                intent.putExtra("passenger", nama[holder.absoluteAdapterPosition])
                intent.putExtra("arrayke", holder.absoluteAdapterPosition)
                (context as Activity).startActivityForResult(intent, REQUEST_PIN_MAP)
            }
            else{
                nama[holder.absoluteAdapterPosition].jemput = false
                holder.itemView.etBerangkat.setText(nama[holder.absoluteAdapterPosition].berangkat)
                holder.itemView.berangkatInfo.text = "* Tersedia layanan antar, max "+booking.keberangkatanRadius+"km dari loket tujuan"
                nama[holder.absoluteAdapterPosition].longitude = nama[holder.absoluteAdapterPosition].berangkat
            }
        }

        holder.itemView.cbAntar.setOnClickListener {
            if(holder.itemView.cbAntar.isChecked) {
                nama[holder.absoluteAdapterPosition].antar = true
                val intent = Intent(context, MapsActivity::class.java)
                intent.putExtra("lokasi","antar")
                intent.putExtra("booking", booking)
                intent.putExtra("passenger", nama[holder.absoluteAdapterPosition])
                intent.putExtra("arrayke", holder.absoluteAdapterPosition)
                (context as Activity).startActivityForResult(intent, REQUEST_PIN_MAP)
            }
            else{
                nama[holder.absoluteAdapterPosition].antar = false
                holder.itemView.etTiba.setText(nama[holder.absoluteAdapterPosition].tiba)
                holder.itemView.tibaInfo.text = "* Tersedia layanan antar, max "+booking.tujuanRadius+"km dari loket antar"
                nama[holder.absoluteAdapterPosition].latitude = nama[holder.absoluteAdapterPosition].tiba
            }
        }

        if (booking.keberangkatanLat==""||booking.keberangkatanMain==""){
            holder.itemView.cbJemput.makeGone()
            holder.itemView.jemput.makeGone()
            holder.itemView.etBerangkat.setOnClickListener {
                dialogRegional = LoketDialog(context,items[0].routeID,nama[holder.absoluteAdapterPosition].longitude)
                dialogRegional.show()
                dialogRegional.setOnClickDialog(object : ActionAdapterEntity<RegionalEntity> {
                    override fun onClick(regionalEntity: RegionalEntity) {
                        holder.itemView.etBerangkat.setText(regionalEntity.regional)
                        nama[holder.absoluteAdapterPosition].longitude = regionalEntity.regional
                    }
                })
            }

        }
        if (booking.tujuanLat==""||booking.tujuanMain==""){
            holder.itemView.cbAntar.makeGone()
            holder.itemView.antar.makeGone()
            holder.itemView.etTiba.setOnClickListener {
                dialogRegional = LoketDialog(context,items[0].routeID,nama[holder.absoluteAdapterPosition].latitude)
                dialogRegional.show()
                dialogRegional.setOnClickDialog(object : ActionAdapterEntity<RegionalEntity> {
                    override fun onClick(regionalEntity: RegionalEntity) {
                        holder.itemView.etTiba.setText(regionalEntity.regional)
                        nama[holder.absoluteAdapterPosition].latitude = regionalEntity.regional
                    }
                })
            }

        }
        if(!booking.lessthan){
            holder.itemView.cbJemput.makeGone()
            holder.itemView.jemput.makeGone()
        }



//        val dialog = MapsDialog.newInstance(0.0, 0.0)
//
//        dialog.show(appCompatActivity.supportFragmentManager, "editLocation")
    }

    fun getPassenger(): MutableList<PassengerEntity>? {
        return nama
    }




}