package com.tiomaz.trans.presentation.listener

import android.location.Location

interface OnMapListener {
    fun onLocationLoadingTimeOut()
    fun onLocationUpdateSuccess(location: Location)
    fun onLocationUpdateFailed()
}