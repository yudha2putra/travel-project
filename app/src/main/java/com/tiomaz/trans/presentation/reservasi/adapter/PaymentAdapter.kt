package com.tiomaz.trans.presentation.reservasi.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PaymentEntity
import com.tiomaz.trans.presentation.payment.PaymentHowTo
import com.tiomaz.trans.presentation.payment.PaymentThanks
import com.tiomaz.trans.presentation.payment.WebviewActivity
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class PaymentAdapter(val context: Context, private val items: MutableList<PaymentEntity>, private val booking: BookingEntity) :
    RecyclerView.Adapter<PaymentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_payment,parent,false)
        return PaymentViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: PaymentViewHolder, position: Int) {
        holder.bindTo(items[position])

        holder.itemView.setOnClickListener {
            selectPayment(items[position])
        }
    }

    fun selectPayment(payment:PaymentEntity){
        val url = "https://system.tiomaz.com/update_payment_mehtod"
        val sharedPreference = context.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
        val partner = sharedPreference.getInt("partner",0)
        val params = JSONObject()
        params.put("partner_id", partner)
        params.put("kode_book", booking.kodeBook)
        params.put("payment_method_id", payment.id)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("data")
                    val arrayData = JSONArray(jsonResult2)
                    if(arrayData.length()!=0){
//                        Toast.makeText(context,"Select Payment Sukses", Toast.LENGTH_SHORT).show()
                        payment(payment)
                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }

                }catch (e:Exception){
                    Toast.makeText(context,e.toString(), Toast.LENGTH_SHORT).show()
                }

            }, {
                // Error in request
                Toast.makeText(context,it.toString(), Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context).addToRequestQueue(request)
    }

    fun payment(payment: PaymentEntity){
        val url = "https://system.tiomaz.com/payment"

        val sharedPreference = context.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
        val uid = sharedPreference.getInt("uid",0)

        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val currentDateandTime: String = sdf.format(Date())

        val params = JSONObject()
        params.put("kode_booking", booking.kodeBook)
        params.put("jam_pemesanan", currentDateandTime)
        params.put("total_pembayaran", booking.total.toString())
        params.put("user_id", uid)
        params.put("type_payment_method_id", payment.idPayment)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("data")
                    val arrayData = JSONArray(jsonResult2)
                    if(arrayData.length()!=0){
//                        Toast.makeText(context,"Payment Sukses", Toast.LENGTH_SHORT).show()
                        Log.d("print",arrayData.toString())


                        if(arrayData.getJSONObject(0).getString("total_pembayaran")!="0.00"){
                            val paymentChannel = arrayData.getJSONObject(0).getString("payment_channel")
                            val url = arrayData.getJSONObject(0).getString("redirect_url")
                            val time = arrayData.getJSONObject(0).getString("jam_expired")
                            if(paymentChannel=="812"||paymentChannel=="814"||paymentChannel=="819"||paymentChannel=="701"){
                                val intent = Intent(context, WebviewActivity::class.java)
                                intent.putExtra("url",url)
                                intent.putExtra("time",time)
                                intent.putExtra("kode_book", booking.kodeBook)
                                context.startActivity(intent)
//                                (context as Activity).finish()
                            }
                            else{
                                payment.trxID = arrayData.getJSONObject(0).getString("trx_id")
                                val intent = Intent(context, PaymentHowTo::class.java)
                                intent.putExtra("payment",payment)
                                intent.putExtra("time",time)
                                intent.putExtra("kode_book", booking.kodeBook)
                                context.startActivity(intent)
//                                (context as Activity).finish()
                            }
                        }
                        else{
                            val intent = Intent(context, PaymentThanks::class.java)
                            context.startActivity(intent)
                        }

                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }

                }catch (e:Exception){
                    Toast.makeText(context,e.toString(), Toast.LENGTH_SHORT).show()
                }

            }, {
                // Error in request
                Toast.makeText(context,it.toString(), Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(20000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context).addToRequestQueue(request)
    }


}