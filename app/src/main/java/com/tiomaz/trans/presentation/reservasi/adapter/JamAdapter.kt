package com.tiomaz.trans.presentation.reservasi.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.ActionAdapter
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.JamEntity
import com.tiomaz.trans.presentation.seat.SeatActivity
import kotlinx.android.synthetic.main.activity_reservasi.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList

class JamAdapter(val context: Context, private val items: MutableList<JamEntity>,val booking: BookingEntity) :
    RecyclerView.Adapter<JamViewHolder>() {

    lateinit var jam: ArrayList<String>
//    lateinit var action: ActionAdapter<String>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JamViewHolder {
        jam = ArrayList()
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item_jam,parent,false)
        return JamViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: JamViewHolder, position: Int) {
        holder.bindTo(items[position])

        holder.itemView.setOnClickListener {
            booking.idJam=items[position].id
            booking.jam=items[position].jam
            booking.lessthan=items[position].lessthan
            doCheckDouble(position)

        }
    }

    private fun doCheckDouble(position:Int) {
        val sharedPreference = context.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
        val uid = sharedPreference?.getInt("uid",0)
        val url = "https://system.tiomaz.com/double_order_code_booking"
        val params = JSONObject()
        params.put("id", booking.idJam)
        params.put("user_id", uid)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                    if(response.has("result")) {
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("data")
                        val arrayData = JSONArray(jsonResult2)
                        val data = arrayData.getJSONObject(0).getString("response_desc")
                        Toast.makeText(context,data.toString(), Toast.LENGTH_LONG).show()
                    }
                    else{

                        val intent = Intent(context, SeatActivity::class.java)
                        intent.putExtra("kapasitas",items[position].kursi)
                        intent.putExtra("booking", booking)
                        context.startActivity(intent)
                        notifyDataSetChanged()
                    }

                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context).addToRequestQueue(request)
    }

//    fun setOnClick(actionAdapter: ActionAdapter<String>){
//        this.action = actionAdapter
//    }

}