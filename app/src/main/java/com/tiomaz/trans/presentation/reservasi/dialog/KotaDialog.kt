package com.tiomaz.trans.presentation.reservasi.dialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.*
import com.tiomaz.trans.domain.entities.KotaEntity
import com.tiomaz.trans.presentation.reservasi.adapter.KotaAdapter
import kotlinx.android.synthetic.main.kota_dialog.*
import org.json.JSONArray
import org.json.JSONObject

class KotaDialog(context: Context) : Dialog(context), ActionAdapterEntity<KotaEntity> {

    lateinit var actionAdapter: ActionAdapterEntity<KotaEntity>
    lateinit var kotaAdapter: KotaAdapter
    lateinit var kota: MutableList<KotaEntity>
    var regionalDialog: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.kota_dialog)
        val width = (context.resources.displayMetrics.widthPixels * 0.70).toInt()

        window?.setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        kota = mutableListOf()
        kotaAdapter = KotaAdapter(context,kota)
        rvKota.apply {
            adapter = kotaAdapter
            layoutManager = LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL,false)
        }

        val url = "https://system.tiomaz.com/masterdata_list_from"
        val json = JSONObject()
        json.put("regional_id", regionalDialog)
        Log.d("print",json.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("result")
                    val arrayData = JSONArray(jsonResult2)
                    if(arrayData.length()!=0){
//                        Toast.makeText(context,"Kota Sukses", Toast.LENGTH_SHORT).show()
                        for (i in 0 until arrayData.length()){
                            val id = arrayData.getJSONObject(i).getInt("id")
                            val name = arrayData.getJSONObject(i).getString("name")
                            val latitude = arrayData.getJSONObject(i).getString("latitude")
                            val longitude = arrayData.getJSONObject(i).getString("longitude")
                            val isMain = arrayData.getJSONObject(i).getString("is_main")
                            val radius = arrayData.getJSONObject(i).getString("max_pick")
                            val time = arrayData.getJSONObject(i).getString("time")
                            kota.add(KotaEntity(id, name, latitude, longitude, isMain,radius,time))
                            kotaAdapter.notifyDataSetChanged()
                        }
                    }
                    else{
                        Toast.makeText(context,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            10000,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(context).addToRequestQueue(request)
        pb.makeVisible()

        kotaAdapter.setOnClick(this)
    }

    fun setRegional(id:Int){
        this.regionalDialog=id
    }

    override fun onClick(kota: KotaEntity) {
        dismiss()
        actionAdapter.onClick(kota)
    }
    fun setOnClickDialog(actionAdapter: ActionAdapterEntity<KotaEntity>){
        this.actionAdapter = actionAdapter
    }

}