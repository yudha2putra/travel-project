package com.tiomaz.trans.presentation.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Location
import android.util.Log
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseActivity
import com.tiomaz.trans.data.preference.KecipirPreference
import com.tiomaz.trans.presentation.beranda.BerandaFragment
import com.tiomaz.trans.presentation.call.CallFragment
import com.tiomaz.trans.presentation.pesanan.PesananFragment
import com.tiomaz.trans.presentation.profile.ProfileFragment
import com.tiomaz.trans.utils.constants.AppConstants
import com.tiomaz.trans.utils.extentions.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {

    companion object {
        const val BERANDA_PAGE = 0
        const val PESANAN_PAGE = 1
        const val NOTIFICATION_PAGE = 2
        const val PROFILE_PAGE = 3

        fun start(context: Context) {
            val starter = Intent(context, MainActivity::class.java)
            context.startActivity(starter)
        }
    }

    var activeFragment: Fragment = BerandaFragment()
    var onNavigateToTasksFragment: (() -> Unit)? = null

    private val taskFragment: BerandaFragment by lazy { BerandaFragment() }
    private val incomeFragment: PesananFragment by lazy { PesananFragment() }
    private val callFragment: CallFragment by lazy { CallFragment() }
    private val profileFragment: ProfileFragment by lazy { ProfileFragment() }

    private var isBack = false


    override val layout: Int = R.layout.activity_main

    override fun onPreparation() {

    }

    override fun onIntent() {
    }

    override fun onUi() {
        setupBottomMainMenu()
    }

    override fun onAction() {
        bnvMain.setOnNavigationItemSelectedListener(this)
    }

    override fun onObserver() {

    }

    private fun setupBottomMainMenu() {
        addFirstFragment(R.id.mainHostFragment, taskFragment)
        bnvMain.itemIconTintList = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (Activity.RESULT_OK == resultCode) {
            if (requestCode == AppConstants.REQUEST_START_TASK) {
                Log.d("GetLocation", "Get location after activate gps")
                showProgress()
            }
        }
        supportFragmentManager.fragments.forEach { _ ->
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.berandaFragment -> {
                showFragment(R.id.mainHostFragment, taskFragment, activeFragment)
                activeFragment = taskFragment
                onNavigateToTasksFragment?.invoke()
                isBack = false
            }
            R.id.pesananFragment -> {
                showFragment(R.id.mainHostFragment, incomeFragment, activeFragment)
                activeFragment = incomeFragment
                isBack = false
            }
            R.id.callFragment -> {
                showFragment(R.id.mainHostFragment, callFragment, activeFragment)
                activeFragment = callFragment
                isBack = false
            }
            R.id.profileFragment -> {
                showFragment(R.id.mainHostFragment, profileFragment, activeFragment)
                activeFragment = profileFragment
                setStatusBarColor(R.color.textBlack)
                isBack = false
            }
        }
        return true
    }

    override fun onBackPressed() {
        if (!isBack) {
            isBack = true
            showToast("Klik sekali lagi untuk keluar halaman")
        } else {
            finishAffinity()
        }
    }

}