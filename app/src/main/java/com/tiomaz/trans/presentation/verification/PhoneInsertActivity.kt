package com.tiomaz.trans.presentation.verification

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_phone_insert.*

class PhoneInsertActivity : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_insert)

        btnNext.setOnClickListener {
            val intent = Intent(this, CodeInsertActivity::class.java)
            startActivity(intent)
        }
    }
}