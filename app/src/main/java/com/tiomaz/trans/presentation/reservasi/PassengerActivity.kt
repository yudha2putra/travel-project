package com.tiomaz.trans.presentation.reservasi

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PassengerEntity
import com.tiomaz.trans.presentation.confirmation.ConfirmationActivity
import com.tiomaz.trans.presentation.reservasi.adapter.PassengerNameAdapter
import com.tiomaz.trans.presentation.reservasi.dialog.MapsDialog
import com.tiomaz.trans.utils.extentions.emptyString
import com.tiomaz.trans.utils.extentions.makeVisible
import kotlinx.android.synthetic.main.activity_passenger.*
import kotlinx.android.synthetic.main.activity_passenger.ivClose
import kotlinx.android.synthetic.main.activity_passenger.tvKeberangkatan
import kotlinx.android.synthetic.main.activity_passenger.tvPenumpang
import kotlinx.android.synthetic.main.activity_passenger.tvTanggal
import kotlinx.android.synthetic.main.activity_passenger.tvTujuan
import kotlin.collections.ArrayList

const val REQUEST_PIN_MAP = 1

class PassengerActivity : AppCompatActivity() {

    lateinit var seatSelected: MutableList<Int>
    lateinit var idSeatSelected: MutableList<Int>
    lateinit var passengerAdapter: PassengerNameAdapter
    lateinit var passenger: MutableList<PassengerEntity>
    lateinit var booking: BookingEntity
    private lateinit var mapsDialog: MapsDialog
    private var latLng = emptyString()
    private var addressGps = emptyString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_passenger)

        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)

        val partner = sharedPreference.getInt("partner",0)
        val routeID = sharedPreference.getInt("routeID",0)

        Log.d("partner",partner.toString())
        Log.d("partner",routeID.toString())

        mapsDialog = MapsDialog()


        seatSelected = mutableListOf()
        idSeatSelected = mutableListOf()
        booking = intent.getSerializableExtra("booking") as BookingEntity

        tvKeberangkatan.setText(booking.keberangkatan)
        tvjam.setText("Pukul " + booking.jam)
        tvTujuan.setText(booking.tujuan)
        tvTanggal.setText(booking.tanggal)
        tvPenumpang.setText(booking.penumpang + " Penumpang")

        val idSeat = intent.getIntArrayExtra("idSeat")!!
        val seat = intent.getIntArrayExtra("seat")
        if (seat != null) {
            for (i in 0 until seat.size) {
                if (seat[i]==1){
                    seatSelected.add(i)
                    idSeatSelected.add(idSeat[i])
                }
            }
        }
        Log.d("print", seatSelected.toString())
        Log.d("print", idSeatSelected.toString())

        passenger = mutableListOf()
        passengerAdapter = PassengerNameAdapter(this, passenger,booking)
//        intent.getStringExtra("penumpang")?.toInt()!!

        for (i in 0 until booking.penumpang.toInt()) {
            passenger.add(PassengerEntity(
                seat = seatSelected[i],
                idSeat = idSeatSelected[i],
                name = "",
                partnerID = partner,
                routeID = routeID,
                jemput = false,
                antar = false,
                longitude = booking.keberangkatan,
                latitude = booking.tujuan,
                longitudeInfo = "* Tersedia layanan antar, max "+booking.keberangkatanRadius+"km dari loket tujuan",
                latitudeInfo = "* Tersedia layanan antar, max "+booking.tujuanRadius+"km dari loket antar",
                nomor = "",
                berangkat = booking.keberangkatan,
                tiba = booking.tujuan,
                pickupPrice = 0.00,
                deliverPrice = 0.00)
            )
        }

        passengerAdapter.notifyDataSetChanged()

        rvPenumpang.apply {
            adapter = passengerAdapter
            layoutManager = LinearLayoutManager(context)
        }

        ivClose.setOnClickListener {
            finish()
        }

        btnNext.setOnClickListener {
            for (i in 0 until passenger.size) {
                if (passenger[i].name==""){
                    checklistInfo.text = "Terdapat nama yang belum diisi, harap melengkapi data terlebih dahulu"
                    checklistInfo.makeVisible()
                }
                else{
                    if(cbSetuju.isChecked){
                        passenger = passengerAdapter.getPassenger()!!
                        startActivity(toConfirmationActivity(this))
                        Log.d("print", passenger.toString())
                    }
                    else{
                        checklistInfo.text ="Harap menyetujui syarat dan ketentuan"
                        checklistInfo.makeVisible()
                    }
                }
            }


        }


    }

    fun toConfirmationActivity(context: Context): Intent {

        val intent = Intent(context, ConfirmationActivity::class.java)
        intent.putExtra("passenger", passenger as ArrayList<PassengerEntity>)
        intent.putExtra("booking", booking)
        return intent
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PIN_MAP) {
            val arrayke = data?.getIntExtra("arrayke",0)
            if(data?.getStringExtra("latitude").isNullOrBlank()){
                val longitude = data?.getStringExtra("longitude")
                val jarakJemput = data?.getStringExtra("jarakJemput")
                val pickupPrice = data?.getDoubleExtra("pickupPrice",0.00)
                Log.d("print", jarakJemput.toString())
                passenger[arrayke!!].longitude=longitude!!
                passenger[arrayke!!].longitudeInfo=jarakJemput!!
                passenger[arrayke!!].pickupPrice=pickupPrice!!
            }
            else{
                val latitude = data?.getStringExtra("latitude")
                val jarakAntar = data?.getStringExtra("jarakAntar")
                val deliverPrice = data?.getDoubleExtra("deliverPrice",0.00)
                Log.d("print", jarakAntar.toString())
                passenger[arrayke!!].latitude=latitude!!
                passenger[arrayke!!].latitudeInfo=jarakAntar!!
                passenger[arrayke!!].deliverPrice=deliverPrice!!
            }





//            addressGps = data?.getStringExtra("addressGps") ?: ""
//            Log.d("print",passenger[arrayke].latitude)
//            Log.d("print",passenger[arrayke].longitude)
            passengerAdapter.notifyDataSetChanged()
            return
        }
    }







}
