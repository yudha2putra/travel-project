package com.tiomaz.trans.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tiomaz.trans.domain.usecase.contract.NotificationUseCase
import com.tiomaz.trans.utils.common.ResultState
import kotlinx.coroutines.launch

class NotificationViewModel (private val useCase: NotificationUseCase): ViewModel() {
    
    fun postFcmToken(token:String): MutableLiveData<ResultState<Boolean>>{
        val postFcmToken = MutableLiveData<ResultState<Boolean>>()
        postFcmToken.postValue(ResultState.Loading())
        viewModelScope.launch {
            val completedTaskResponse = useCase.postFcmToken(token)
            postFcmToken.postValue(completedTaskResponse)
        }
        return postFcmToken
    }
}