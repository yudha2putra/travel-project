package com.tiomaz.trans.presentation.payment

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.PaymentEntity
import com.tiomaz.trans.presentation.main.MainActivity
import com.tiomaz.trans.presentation.order.OrderActivity
import kotlinx.android.synthetic.main.activity_payment_howto.*
import java.util.concurrent.ThreadLocalRandom.current

class PaymentThanks : AppCompatActivity() {
    lateinit var payment: PaymentEntity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_thanks)


        btnPesanan.setOnClickListener {
            val intent = Intent(this, OrderActivity::class.java)
            intent.putExtra("from","Pesanan Saya")
            startActivity(intent)
        }
        btnHome.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

}