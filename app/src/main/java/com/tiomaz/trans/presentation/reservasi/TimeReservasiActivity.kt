package com.tiomaz.trans.presentation.reservasi

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.google.gson.JsonArray
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.common.makeVisible
import com.tiomaz.trans.domain.entities.BookingEntity
import com.tiomaz.trans.domain.entities.JamEntity
import com.tiomaz.trans.presentation.reservasi.adapter.JamAdapter
import kotlinx.android.synthetic.main.activity_time_reservasi.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class TimeReservasiActivity : AppCompatActivity() {

    lateinit var jamAdapter: JamAdapter
    lateinit var jam: MutableList<JamEntity>
    lateinit var booking: BookingEntity


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_time_reservasi)
        val productID = ArrayList<Int?>()
        var routeID = ArrayList<Int?>()

        booking = intent.getSerializableExtra("booking") as BookingEntity
        jam = mutableListOf()
        jamAdapter = JamAdapter(this,jam,booking)
        rvJam.apply {
            adapter = jamAdapter
            layoutManager = GridLayoutManager(context,2)
        }
        Log.d("print",booking.keberangkatanMain)
        Log.d("print",booking.tujuanMain)
        var text = "Max. "+booking.keberangkatanRadius+"km dari Loket Dikenakan biaya tambahan Berlaku "+booking.keberangkatanTime+" jam sebelum keberangkatan"
        subtitle.text = text
        val url = "https://system.tiomaz.com/masterdata_list_products"
        val url2 = "https://system.tiomaz.com/list_transaction_bookingorder"
        val params = JSONObject()
        params.put("dari_kota", booking.keberangkatan)
        params.put("tujuan_kota", booking.tujuan)

        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    val jsonResult = response.getString("result")
                    val jsonResult2 = JSONObject(jsonResult).getString("result")
                    val arrayData = JSONArray(jsonResult2)
                    if(arrayData.length()!=0){
//                        Toast.makeText(this,"Jam(1) Sukses", Toast.LENGTH_SHORT).show()
                        for (i in 0 until arrayData.length()){
                            routeID.add(arrayData.getJSONObject(i).getInt("id"))
                            val sharedPreference =  getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
                            var editor = sharedPreference.edit()
                            editor.putInt("routeID",arrayData.getJSONObject(i).getInt("id"))
                            editor.commit()
                            val arrayProductID = JSONArray(arrayData.getJSONObject(i).getString("product_id"))
                            productID.add(arrayProductID.getJSONObject(0).getInt("id"))
                        }
                        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                        val currentDateandTime: String = sdf.format(Date())
                        val tanggal = booking.tanggal
                        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ENGLISH)
                        val dateString = LocalDate.parse(tanggal, formatter)
                        val params2 = JSONObject()
                        params2.put("less_than_3hours", currentDateandTime)
                        params2.put("tanggal_keberangkatan", dateString)
                        params2.put("sub_route_id", JSONArray(routeID))
                        params2.put("product_id", JSONArray(productID))
                        params2.put("jumlah_penumpang", booking.penumpang)
                        Log.d("print",params2.toString())

                        val request2 = JsonObjectRequest(
                            Request.Method.POST,url2,params2,
                            { response ->
                                // Process the json
                                try {
                                    pb.makeGone()
                                    Log.d("print",response.toString())
                                    val json2Result = response.getString("result")
                                    val json2Result2 = JSONObject(json2Result).getString("result")
                                    val array2Data = JSONArray(json2Result2)
                                    if(array2Data.length()!=0){
//                                        Toast.makeText(this,"Jam(2) Sukses", Toast.LENGTH_SHORT).show()
                                        for (i in 0 until array2Data.length()){
                                            val idJam = array2Data.getJSONObject(i).getInt("id")
                                            val departJam = array2Data.getJSONObject(i).getString("time_depart")
                                            val kapasitasJam = array2Data.getJSONObject(i).getString("kapasitas")
                                            val bookedJam = array2Data.getJSONObject(i).getString("jml_booking")
                                            val tersedia = kapasitasJam.toInt()-bookedJam.toInt()
                                            val lessthan = array2Data.getJSONObject(i).getBoolean("less_than_3hours")
                                            val subrouteId = array2Data.getJSONObject(i).getString("subroute_id")
                                            val arraySubroute = JSONArray(subrouteId)
                                            val etd = arraySubroute.getJSONObject(0).getInt("etd")
                                            val product = array2Data.getJSONObject(i).getString("product")
                                            val arrayProduct = JSONArray(product)
                                            val defaultCode = arrayProduct.getJSONObject(0).getString("default_code")
                                            jam.add(JamEntity(idJam,departJam,etd,"Tersedia "+tersedia+" Kursi",booking.keberangkatanMain,booking.tujuanMain,defaultCode,lessthan))
                                            jamAdapter.notifyDataSetChanged()

//                                            val name = arrayData.getJSONObject(i).getString("name")
//                                            kota.add(name)
//                                            kotaAdapter.notifyDataSetChanged()
                                        }
                                    }
                                    else{
                                        Toast.makeText(this,"Keberangkatan Tidak Tersedia", Toast.LENGTH_SHORT).show()
                                    }
                                }catch (e:Exception){
                                    Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
                                }

                            }, {
                                // Error in request
                                Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                            })


                        // Volley request policy, only one time request to avoid duplicate transaction
                        request2.retryPolicy = DefaultRetryPolicy(30000,1,1f)

                        // Add the volley post request to the request queue
                        VolleySingleton.getInstance(this).addToRequestQueue(request2)
                        pb.makeVisible()

                    }
                    else{
                        Toast.makeText(this,"Keberangkatan Tidak Tersedia", Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Toast.makeText(this,e.toString(), Toast.LENGTH_SHORT).show()
                }

            }, {
                // Error in request
                Toast.makeText(this,it.toString(), Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(30000,1,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()

        ivClose.setOnClickListener {
            finish()
        }

        tvKeberangkatan.setText(booking.keberangkatan)
        tvTujuan.setText(booking.tujuan)
        tvTanggal.setText(booking.tanggal)
        tvPenumpang.setText(booking.penumpang+" Penumpang")

    }
}