package com.tiomaz.trans.presentation.reservasi.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.PassengerEntity
import kotlinx.android.synthetic.main.list_item_confirmation.view.*

class ConfirmationViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindTo(passenger:PassengerEntity) {
        itemView.noKursi.text = "Kursi Nomor "+passenger.seat
        itemView.NamaPenumpang.text = passenger.name
        itemView.NomorPenumpang.text = passenger.nomor
        itemView.AlamatJemput.text = passenger.longitude
        itemView.AlamatAntar.text = passenger.latitude
    }
}