package com.tiomaz.trans.presentation.verification

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseActivity
import com.tiomaz.trans.presentation.main.MainActivity
import com.tiomaz.trans.presentation.profile.CreateProfileActivity
import com.tiomaz.trans.presentation.viewmodel.MembershipViewModel
import com.tiomaz.trans.utils.common.ResultState
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import com.tiomaz.trans.utils.extentions.onSingleClickListener
import com.tiomaz.trans.utils.extentions.toPhoneNumberWithDefaultPrefix
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.common.VolleySingleton
import kotlinx.android.synthetic.main.activity_login.facebook
import kotlinx.android.synthetic.main.activity_login.ig
import kotlinx.android.synthetic.main.activity_login.pb
import kotlinx.android.synthetic.main.activity_login.twitter
import kotlinx.android.synthetic.main.activity_notification.*

class LoginActivity : BaseActivity(){
    companion object {
        fun start(context: Context) {
            val starter = Intent(context, LoginActivity::class.java)
            context.startActivity(starter)
        }
    }
    private val membershipViewModel: MembershipViewModel by viewModel()
    override val layout: Int = R.layout.activity_login
    override fun onPreparation() {

    }

    override fun onIntent() {
    }

    override fun onObserver() {
    }

    override fun onUi() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        etEmail.toPhoneNumberWithDefaultPrefix()

    }

    override fun onAction() {
        btnLogin.onSingleClickListener {
            doLogin()
        }

        btnDaftar.onSingleClickListener {
            CreateProfileActivity.start(this)
        }

        btnReset.onSingleClickListener {
            ResetActivity.start(this)
        }

        ig.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/tiomaztrans/?hl=id"))
            startActivity(browserIntent)
        }
        twitter.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/tiomaztrans/?hl=id"))
            startActivity(browserIntent)
        }
        facebook.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/tiomaztrans"))
            startActivity(browserIntent)
        }
    }

    private fun doLogin() {
        val url = "https://system.tiomaz.com/web/session/authenticate"
        val params = JSONObject()
        params.put("login", etEmail.text)
        params.put("password", etPassword.text)
        params.put("db", "test")
        val json = JSONObject()
        json.put("params", params)
        Log.d("print",json.toString())

        val request = JsonObjectRequest(Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val name = JSONObject(jsonResult).getString("name")
                        val email = JSONObject(jsonResult).getString("username")
                        val partner = JSONObject(jsonResult).getInt("partner_id")
                        val uid = JSONObject(jsonResult).getString("uid")
                        val phone = JSONObject(jsonResult).getString("phone")
                        val sharedPreference =  getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
                        var editor = sharedPreference.edit()
                        editor.putInt("partner",partner)
                        editor.putString("name",name)
                        editor.putString("email",email)
                        editor.putString("phone",phone)
                        editor.putInt("uid",uid.toInt())
                        editor.commit()
                        doFirebase()
                        toMainActivity()
                        Toast.makeText(this,"Login Berhasil",Toast.LENGTH_SHORT).show()
                    }
                    else{
                        Toast.makeText(this,"Email atau Password Salah",Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    private fun doFirebase() {
        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
        val uid = sharedPreference?.getInt("uid",0)
        val fb = sharedPreference?.getString("FirebaseToken","")
        val url = "https://system.tiomaz.com/token_holder"
        val params = JSONObject()
        params.put("user_id",uid)
        params.put("token_holder", fb)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }

    private fun toMainActivity() {
        MainActivity.start(this)
        finishAffinity()
    }

    override fun onBackPressed() {
        MainActivity.start(this)
        finishAffinity()
        super.onBackPressed()
    }


}