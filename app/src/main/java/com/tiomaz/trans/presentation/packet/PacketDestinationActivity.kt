package com.tiomaz.trans.presentation.packet

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import kotlinx.android.synthetic.main.activity_packet_destination.*

class PacketDestinationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_packet_destination)
        tvTitle.text = intent.getStringExtra("title")

        ivClose.setOnClickListener {
            finish()
        }
    }
}