package com.tiomaz.trans.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tiomaz.trans.domain.entities.Income
import com.tiomaz.trans.domain.entities.Vehicle
import com.tiomaz.trans.domain.usecase.contract.IncomeUseCase
import com.tiomaz.trans.domain.usecase.contract.MasterDataUseCase
import com.tiomaz.trans.utils.common.ResultState
import kotlinx.coroutines.launch

class IncomeViewModel(private val useCase: IncomeUseCase) : ViewModel() {
    fun fetchIncomes(): MutableLiveData<ResultState<List<Income>>> {
        val fetchIncomes = MutableLiveData<ResultState<List<Income>>>()
        fetchIncomes.value = ResultState.Loading()
        viewModelScope.launch {
            val fetchIncomesResponse = useCase.fetchIncomes()
            fetchIncomes.value = fetchIncomesResponse
        }
        return fetchIncomes
    }
}