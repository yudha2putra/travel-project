package com.tiomaz.trans.presentation.rating

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import com.tiomaz.trans.R
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.common.makeGone
import com.tiomaz.trans.common.makeVisible
import com.tiomaz.trans.domain.entities.PesananEntity
import com.tiomaz.trans.presentation.reservasi.adapter.PesananAdapter
import com.tiomaz.trans.presentation.reservasi.adapter.RatingAdapter
import kotlinx.android.synthetic.main.activity_rating.*
import kotlinx.android.synthetic.main.activity_rating.pb
import org.json.JSONArray
import org.json.JSONObject

class RatingActivity : AppCompatActivity() {

    lateinit var ratingAdapter: RatingAdapter
    lateinit var pesanan: MutableList<PesananEntity>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rating)
        pesanan = mutableListOf()
        ratingAdapter = RatingAdapter(this,pesanan)
        rvRating.apply {
            adapter = ratingAdapter
            layoutManager = GridLayoutManager(context,1)
        }
        doListingRating()
        ivClose.setOnClickListener {
            finish()
        }
    }

    fun doListingRating(){
        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME", Context.MODE_PRIVATE)
        val partner = sharedPreference.getInt("partner",0)

        val url = "https://system.tiomaz.com/order_history"
        val state = JSONArray()
        state.put("Arrived")
        state.put("Closed")
        val json = JSONObject()
        json.put("partner_id", partner)
        json.put("status_pembayaran", "")
        json.put("order_by", "date_depart desc")
        json.put("state", state)
        Log.d("print",json.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,json,
            { response ->
                // Process the json
                try {
                    pb.makeGone()
                    Log.d("print",response.toString())
                    if(response.has("result")){
                        val jsonResult = response.getString("result")
                        val jsonResult2 = JSONObject(jsonResult).getString("result")
                        val arrayData = JSONArray(jsonResult2)
                        if (arrayData.length()!=0){
//                            Toast.makeText(this,"Rating Sukses", Toast.LENGTH_SHORT).show()
                            for (i in 0 until arrayData.length()){
                                val id = arrayData.getJSONObject(i).getInt("id")
                                val tanggal = arrayData.getJSONObject(i).getString("date_depart")
                                val kodeBooking = arrayData.getJSONObject(i).getString("kode_booking")
                                val dari = arrayData.getJSONObject(i).getString("dari")
                                val tujuan = arrayData.getJSONObject(i).getString("tujuan")
                                val nama = arrayData.getJSONObject(i).getString("nama_pemesan")
                                val state = arrayData.getJSONObject(i).getString("state")
                                val hargaTiket = arrayData.getJSONObject(i).getString("harga_tiket")
                                val jumlahPenumpang = arrayData.getJSONObject(i).getString("jumlah_penumpang")
                                val chargeJemput = arrayData.getJSONObject(i).getString("total_pickup_price")
                                val chargeAntar = arrayData.getJSONObject(i).getString("total_deliver_price")
                                val subtotal = arrayData.getJSONObject(i).getString("sub_total")
                                val diskon = arrayData.getJSONObject(i).getString("diskon")
                                val total = arrayData.getJSONObject(i).getString("total")
                                val bookingLine = arrayData.getJSONObject(i).getString("booking_line")
                                val bookingArray = JSONArray(bookingLine)
                                var seat = ""
                                var trx = ""
                                var statusBayar = ""
                                for (j in 0 until bookingArray.length()){
                                    seat += bookingArray.getJSONObject(j).getInt("nomor_seat").toString()
                                    trx = bookingArray.getJSONObject(j).getString("trx_id")
                                    statusBayar = bookingArray.getJSONObject(j).getString("status_pembayaran")
                                }
                                pesanan.add(PesananEntity(id,tanggal,kodeBooking,dari,tujuan,seat,total,state,nama,hargaTiket,jumlahPenumpang, chargeJemput, chargeAntar, subtotal, diskon,trx,statusBayar))
                                ratingAdapter.notifyDataSetChanged()
                            }
                        }
                        else{
                            Toast.makeText(this,"Rating Kosong", Toast.LENGTH_SHORT).show()
                        }
                    }
                    else{
                        Toast.makeText(this,response.toString(), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
//                Log.d("print",it.networkResponse.statusCode.toString())
//                Log.d("print",String(it.networkResponse.data, StandardCharsets.UTF_8))
            })



        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(20000,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
        pb.makeVisible()
    }
}