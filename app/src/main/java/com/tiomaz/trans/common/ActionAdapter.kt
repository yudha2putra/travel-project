package com.tiomaz.trans.common

interface ActionAdapter<T> {
    fun onClick(value:T)
}

interface ActionAdapterEntity<T> {
    fun onClick(value:T)
}