package com.tiomaz.trans.common

import android.view.View
import java.text.SimpleDateFormat
import java.util.*

fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeGone() {
    visibility = View.GONE
}

fun changeFormatStringDate(date:String,formatNew:String,formatOld:String="yyyy-MM-dd") :String{
    val oldFormat = SimpleDateFormat(formatOld, Locale("en"))
    val oldDate = oldFormat.parse(date)
    val newFormat = SimpleDateFormat(formatNew,Locale("in"))
    return newFormat.format(oldDate)
}