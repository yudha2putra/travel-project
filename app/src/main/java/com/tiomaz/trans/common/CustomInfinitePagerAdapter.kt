package com.tiomaz.trans.common

import android.database.DataSetObserver
import android.os.Parcelable
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

class CustomInfinitePagerAdapter(val adapter: PagerAdapter) : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return adapter.isViewFromObject(view, `object`)
    }

    override fun getCount(): Int {
        if (getRealCount() == 0) return 0
        return Integer.MAX_VALUE
    }

    fun getRealCount():Int{
        return adapter.count
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val virtualPosition = position % getRealCount()
        return adapter.instantiateItem(container, virtualPosition)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val virtualPosition = position % getRealCount()
//        adapter.destroyItem(container, virtualPosition, `object`)
        (container as ViewPager).removeView(`object` as View)
    }


    override fun finishUpdate(container: ViewGroup) {
        adapter.finishUpdate(container)
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {
        return adapter.restoreState(state, loader)
    }

    override fun saveState(): Parcelable? {
        return adapter.saveState()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        val virtualPosition = position % getRealCount()
        return adapter.getPageTitle(virtualPosition)
    }

    override fun getPageWidth(position: Int): Float {
        return adapter.getPageWidth(position)
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        adapter.setPrimaryItem(container, position, `object`)
    }

    override fun unregisterDataSetObserver(observer: DataSetObserver) {
        adapter.unregisterDataSetObserver(observer)
    }

    override fun registerDataSetObserver(observer: DataSetObserver) {
        adapter.registerDataSetObserver(observer)
    }

    override fun notifyDataSetChanged() {
        adapter.notifyDataSetChanged()
    }

    override fun getItemPosition(`object`: Any): Int {
        return adapter.getItemPosition(`object`)
    }

}