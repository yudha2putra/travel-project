package com.tiomaz.trans.common

import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

object AESUtils {
    private val keyValue = byteArrayOf(
        'k'.toByte(),
        'e'.toByte(),
        'c'.toByte(),
        'i'.toByte(),
        'p'.toByte(),
        'i'.toByte(),
        'r'.toByte(),
        'a'.toByte(),
        'n'.toByte(),
        'd'.toByte(),
        'r'.toByte(),
        'o'.toByte(),
        'i'.toByte(),
        'd'.toByte(),
        '1'.toByte(),
        '2'.toByte()
    )

//    @Throws(Exception::class)
//    fun decrypt(encrypted: String): String {
//        val enc = toByte(encrypted)
//        val result = decrypt(enc)
//        return String(result)
//    }

    @get:Throws(Exception::class)
    private val rawKey: ByteArray
        private get() {
            val key: SecretKey = SecretKeySpec(keyValue, "AES")
            return key.encoded
        }

    @Throws(Exception::class)
    fun decrypt(encrypted: ByteArray): ByteArray {
        val skeySpec: SecretKey =
            SecretKeySpec(keyValue, "AES")
        val cipher = Cipher.getInstance("AES/ECB/NoPadding")
        cipher.init(Cipher.DECRYPT_MODE, skeySpec)
        return cipher.doFinal(encrypted)
    }

    fun toByte(hexString: String): ByteArray {
        val len = hexString.length / 2
        val result = ByteArray(len)
        for (i in 0 until len) result[i] = Integer.valueOf(
            hexString.substring(2 * i, 2 * i + 2),
            16
        ).toByte()
        return result
    }

}