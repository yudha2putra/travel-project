package com.tiomaz.trans.utils.enum

enum class State {
    LOADING,
    LOADING_PAGING,
    SUCCESS,
    SUCCESS_PAGING,
    ERROR,
    ERROR_PAGING,
    EMPTY,
    EMPTY_PAGING,
    INVALID,
    FAILED,
}