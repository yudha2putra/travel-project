package com.tiomaz.trans.utils.common

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.*
import android.os.Build
import android.util.DisplayMetrics
import android.view.*
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tiomaz.trans.R
import com.tiomaz.trans.utils.extentions.emptyString
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import com.tiomaz.trans.utils.extentions.onSingleClickListener
import kotlinx.android.synthetic.main.layout_alert_dialog.view.*
import kotlinx.android.synthetic.main.layout_alert_dialog.view.tvMessage
import kotlinx.android.synthetic.main.layout_alert_dialog.view.tvTitle
import kotlinx.android.synthetic.main.layout_dialog.*
import kotlinx.android.synthetic.main.layout_dialog.btnAction
import kotlinx.android.synthetic.main.layout_dialog.imgIcon
import kotlinx.android.synthetic.main.layout_dialog.tvMessage
import kotlinx.android.synthetic.main.layout_dialog.tvTitle
import kotlinx.android.synthetic.main.layout_error_bottomsheet_dialog.view.*


fun showKecipirCustomAlertDialog(
    context: Context,
    layoutId: Int,
    cancelable: Boolean = false
): AlertDialog {
    return AlertDialog.Builder(context).create().apply {
        val view = View.inflate(context, layoutId, null)
        setView(view)
        val background = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(background, 16)
        window?.apply {
            setBackgroundDrawable(inset)
            setWindowAnimations(R.style.SlidingDialogAnimation)
        }
        setCancelable(cancelable)
        if (!isShowing) {
            show()
        }
    }
}

fun showKecipirFullScreenCustomDialog(
    context: Context,
    layoutId: Int,
    cancelable: Boolean = false,
    isBlurBackground: Boolean = false,
    activity: Activity? = null,
    color: Int = Color.TRANSPARENT
): Dialog {
    return Dialog(context, R.style.FullScreenDialogTheme).apply {
        val view = View.inflate(context, layoutId, null)
        setContentView(view)

        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(window?.attributes)
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT

        val background = ColorDrawable(color)
        val inset = InsetDrawable(background, 0)
        window?.setBackgroundDrawable(inset)

        if (isBlurBackground) {
            Thread(Runnable {
                activity?.let {
                    ScreenShootUtils.screenShot(it) { bitmap ->
                        val blurBitmap: Bitmap? = BlurImageUtils.blurBackground(context, bitmap, 10)
                        it.runOnUiThread {
                            setAlertDialogBackground(this, context, blurBitmap)
                        }
                    }
                }
            }).start()
        }

        setCancelable(cancelable)
        show()
        window?.attributes = layoutParams
    }
}

fun showKecipirDialog(
    context: Context,
    title: String,
    message: String,
    icon: Int,
    positive: String = emptyString(),
    positiveListener: (() -> Unit)? = null,
    isCancelable: Boolean = false,
    isBlurBackground: Boolean = false,
    activity: Activity? = null,
    color: Int = Color.TRANSPARENT
) {

    val dialog = Dialog(context, R.style.FullScreenDialogTheme)
    dialog.apply {
        val view = View.inflate(context, R.layout.layout_dialog, null)
        setContentView(view)

        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(window?.attributes)
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT

        val background = ColorDrawable(color)
        val inset = InsetDrawable(background, 0)
        window?.setBackgroundDrawable(inset)

        if (isBlurBackground) {
            Thread(Runnable {
                activity?.let {
                    ScreenShootUtils.screenShot(it) { bitmap ->
                        val blurBitmap: Bitmap? = BlurImageUtils.blurBackground(context, bitmap, 10)
                        it.runOnUiThread {
                            setAlertDialogBackground(dialog, context, blurBitmap)
                        }
                    }
                }
            }).start()
        }

        with(view) {

            if (positive.isNotEmpty()) {
                btnAction.makeVisible()
                btnAction.text = positive
            } else {
                btnAction.makeGone()
            }

            tvTitle.text = title
            tvMessage.text = message

            imgIcon.setImageResource(icon)

            btnAction.text = positive

            btnAction.onSingleClickListener {
                positiveListener?.invoke()
                dialog.dismiss()
            }

            setCancelable(isCancelable)
            if (!dialog.isShowing) {
                show()
            }
            window?.attributes = layoutParams
        }
    }
}

private fun setAlertDialogBackground(alert: Dialog, context: Context, result: Bitmap?) {
    val draw = BitmapDrawable(context.resources, result)
    val window = alert.window
    window?.setBackgroundDrawable(draw)
    alert.show()
}


fun showKecipirAlertDialog(
    context: Context,
    title: String,
    message: String,
    positive: String = emptyString(),
    positiveListener: (() -> Unit)? = null,
    negative: String = emptyString(),
    negativeListener: (() -> Unit)? = null,
    dismissListener: (() -> Unit)? = null,
    isCancelable: Boolean = false
) {
    val dialog = AlertDialog.Builder(context).create()
    val view = LayoutInflater.from(context).inflate(R.layout.layout_alert_dialog, null)

    with(view) {

        if (positive.isNotEmpty()) {
            btnPositive.makeVisible()
            btnPositive.text = positive
        } else {
            btnPositive.makeGone()
        }

        if (negative.isNotEmpty()) {
            btnNegative.makeVisible()
            btnNegative.text = negative
        } else {
            btnNegative.makeGone()
        }

        tvTitle.text = title
        tvMessage.text = message

        imgClose.visibility = if(isCancelable) View.VISIBLE else View.GONE

        imgClose.onSingleClickListener {
            dialog.dismiss()
        }

        btnNegative.onSingleClickListener {
            dialog.dismiss()
            negativeListener?.invoke()
        }

        btnPositive.onSingleClickListener {
            dialog.dismiss()
            positiveListener?.invoke()
        }

        dialog.setOnDismissListener {
            dismissListener?.invoke()
        }

    }

    dialog.apply {
        setView(view)
        setCancelable(isCancelable)
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if( this != null && !isShowing) show()
    }
}

fun showKecipirCustomBottomSheetDialog(
    context: Context,
    layoutId: Int,
    cancelable: Boolean = false,
    peekHeight: Int = 0,
    isFullScreen: Boolean = false
): BottomSheetDialog {
    return BottomSheetDialog(context, R.style.AppBottomSheetDialogTheme).apply {
        val view = LayoutInflater.from(context).inflate(layoutId, null)
        setContentView(view)
        if (peekHeight != 0) {
            val mBehavior = BottomSheetBehavior.from(view.parent as View)
            mBehavior.peekHeight = peekHeight
        }

        window?.apply {
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            val dimDrawable = GradientDrawable()
            val navigationBarDrawable = GradientDrawable()
            navigationBarDrawable.shape = GradientDrawable.RECTANGLE
            navigationBarDrawable.setColor(Color.WHITE)
            val layers = arrayOf<Drawable>(dimDrawable, navigationBarDrawable)
            val windowBackground = LayerDrawable(layers)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                windowBackground.setLayerInsetTop(1, metrics.heightPixels)
            }
            setBackgroundDrawable(windowBackground)
        }

        if (isFullScreen) {
            setOnShowListener { dialog ->
                val d = dialog as BottomSheetDialog
                val bottomSheet =
                    d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                BottomSheetBehavior.from<FrameLayout?>(bottomSheet!!).state =
                    BottomSheetBehavior.STATE_EXPANDED
            }
        }

        setCancelable(cancelable)
        show()
    }
}

fun showKecipirBottomSheetDialog(
    context: Context,
    cancelable: Boolean = false,
    peekHeight: Int = 0,
    isFullScreen: Boolean = false,
    title: String,
    message: String,
    icon: Int,
    positive: String = emptyString(),
    positiveListener: (() -> Unit)? = null,
    dismissListener: (() -> Unit)? = null
) {
    val dialog = BottomSheetDialog(context, R.style.AppBottomSheetDialogTheme)
    val view = LayoutInflater.from(context).inflate(R.layout.layout_error_bottomsheet_dialog, null)

    with(view) {

        imgIcon.setImageResource(icon)

        if (positive.isNotEmpty()) {
            btnAction.makeVisible()
            btnAction.text = positive
        } else {
            btnAction.makeGone()
        }

        tvTitle.text = title
        tvMessage.text = message

        btnAction.onSingleClickListener {
            positiveListener?.invoke()
            dialog.dismiss()
        }

        dialog.setOnDismissListener { dismissListener?.invoke() }

    }

    dialog.apply {
        setContentView(view)

        if (peekHeight != 0) {
            val mBehavior = BottomSheetBehavior.from(view.parent as View)
            mBehavior.peekHeight = peekHeight
        }

        window?.apply {
            val metrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(metrics)
            val dimDrawable = GradientDrawable()
            val navigationBarDrawable = GradientDrawable()
            navigationBarDrawable.shape = GradientDrawable.RECTANGLE
            navigationBarDrawable.setColor(Color.WHITE)
            val layers = arrayOf<Drawable>(dimDrawable, navigationBarDrawable)
            val windowBackground = LayerDrawable(layers)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                windowBackground.setLayerInsetTop(1, metrics.heightPixels)
            }
            setBackgroundDrawable(windowBackground)
        }

        if (isFullScreen) {
            setOnShowListener { dialog ->
                val d = dialog as BottomSheetDialog
                val bottomSheet =
                    d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                BottomSheetBehavior.from<FrameLayout?>(bottomSheet!!).state =
                    BottomSheetBehavior.STATE_EXPANDED
            }
        }

        setCancelable(cancelable)
        show()
    }

}