package com.tiomaz.trans.utils.di

import com.tiomaz.trans.data.preference.KecipirPreference
import com.tiomaz.trans.data.preference.KecipirPreferenceImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val preferenceModule = module {
    single<KecipirPreference> { KecipirPreferenceImpl(androidContext()) }
}