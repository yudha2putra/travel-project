package com.tiomaz.trans.utils.custom

import android.content.Context
import android.graphics.Typeface
import android.text.TextUtils
import android.util.AttributeSet
import android.view.Gravity
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.tiomaz.trans.R

class CustomCenteredToolbar(context: Context, attrs:AttributeSet) : Toolbar(context, attrs) {

    private var centeredTitleTextView: TextView? = null

    override fun setTitle(resId: Int) {
        val title = resources.getString(resId)
        setTitle(title)
    }

    override fun setTitle(title: CharSequence?) {
        getCenteredTitleTextView()?.text = title
    }

    override fun getTitle(): CharSequence {
        return getCenteredTitleTextView()?.text.toString()
    }


    fun setTypeface(font: Typeface) {
        getCenteredTitleTextView()?.typeface = font;
    }

    override fun setTitleTextColor(color: Int) {
        getCenteredTitleTextView()?.setTextColor(color)
    }

    private fun getCenteredTitleTextView():TextView? {
        if (centeredTitleTextView == null) {
            centeredTitleTextView = TextView(context)
            centeredTitleTextView?.setSingleLine()
            centeredTitleTextView?.ellipsize = TextUtils.TruncateAt.END
            centeredTitleTextView?.gravity = Gravity.CENTER
            centeredTitleTextView?.setTextAppearance(context, R.style.TextAppearance_AppCompat_Widget_ActionBar_Title)

            val lp: Toolbar.LayoutParams =  LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            lp.gravity = Gravity.CENTER;
            centeredTitleTextView?.layoutParams = lp

            addView(centeredTitleTextView);
        }
        return centeredTitleTextView;
    }
}