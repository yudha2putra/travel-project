package com.tiomaz.trans.utils.enum

enum class AddressStatusType(val type:String){
    VERIFIED("Y"),
    UNVERIFIED("N")
}
