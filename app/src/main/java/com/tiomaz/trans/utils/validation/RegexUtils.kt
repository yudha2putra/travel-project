package com.tiomaz.trans.utils.validation

object RegexUtils {

    const val EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})\$"

    const val NUMBER = "^[0-9]{0,}\$"

    const val PHONE_NUMBER = "^[0-9-]+\$"

    const val PASSWORD = "^[@#&-+()/?!;:'\"*~`|•√π÷×¶∆£¢€¥^°={}\\%©®™✓[]><∆#.0-9a-zA-Z\\s,_-]+\$]+\$"

    const val ALPHABET_ONLY = "^[a-zA-Z]{0,}\$"

    const val ALPHANUMERIC = "^[a-zA-Z0-9]*\$"

    const val NOT_EMPTY = "^[A-Za-z0-9,-_.\\s]+\$"
}