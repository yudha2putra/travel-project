package com.tiomaz.trans.utils.notification

import android.app.Notification.*
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.os.Build
import android.os.Vibrator
import android.provider.Settings
import androidx.core.app.NotificationCompat
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.Notification


class KecipirNotificationBuilder(val context: Context) {

    private val CHANNEL_ID = "KECIPIR"

    fun showNotification(notification: Notification, destinationClass: Class<*>) {
        val intent = Intent(context, destinationClass).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val mNotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        mNotificationManager?.notify(
            System.currentTimeMillis().toInt(),
            createNotificationBuilder(context, notification, intent).build()
        )
    }

    private fun createNotificationBuilder(
        context: Context,
        notification: Notification,
        intent: Intent
    ): NotificationCompat.Builder {
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_reg_header)
            .setContentTitle(notification.title)
            .setContentText(notification.body)
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .setLights(Color.RED, 3000, 3000)
            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDefaults(DEFAULT_ALL)
            .setContentIntent(pendingIntent)
//            .setFullScreenIntent(pendingIntent,true)
            .setAutoCancel(true)
            .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)

        val v = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        v.vibrate(500)

        createNotificationChannel(context)

        return builder
    }

    private fun createNotificationChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(R.string.app_name)
            val descriptionText = context.getString(R.string.app_name)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
                vibrationPattern = longArrayOf(1000, 1000, 1000, 1000, 1000)
                shouldVibrate()
                enableVibration(true)
                enableLights(true)
                setShowBadge(true)
                lockscreenVisibility = android.app.Notification.VISIBILITY_PUBLIC

                val audioAttributes = AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build()
                setSound(Settings.System.DEFAULT_NOTIFICATION_URI, audioAttributes)
            }
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

}