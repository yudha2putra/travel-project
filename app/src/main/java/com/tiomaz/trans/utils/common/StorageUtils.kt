package com.tiomaz.trans.utils.common

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.widget.Toast
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import com.tiomaz.trans.utils.extentions.emptyString
import java.io.*
import java.text.DecimalFormat


private val format: DecimalFormat = DecimalFormat("#.##")
private const val mB = 1024 * 1024.toLong()
private const val kB: Long = 1024

fun String.getLastPath():String{
    val uri: Uri = Uri.parse(this)
    return uri.lastPathSegment.orEmpty()
}

fun getBaseDirectoryFile():File{
    return File(Environment.getExternalStorageDirectory(), "Kecipir Delivery")
}

fun getBaseDirectoryPath(context: Context, title: String?=null):String{
    return if (title != null) "${context.getExternalFilesDir("Kecipir Delivery")}/$title"
    else "${context.getExternalFilesDir("Kecipir Delivery")}"
}

fun getBaseImageDirectory(context: Context, fileName:String):File{
    return File(getBaseDirectoryPath(context,fileName))
}

fun getFileName(fileName:String):String{
    return "${fileName.replace(
        " ",
        "_"
    )}.jpeg"
}

fun saveToStorage(directory: File, fileName: String): File {
    if (!directory.exists()) {
        directory.mkdirs()
    }
    return File(directory, fileName)
}

fun deleteFile(path: String) {
    val file = File(path)
    if (file.exists()) {
        file.deleteRecursively()
    }
}

fun File.getCompressedImageOutput(quality:Int = 80):ByteArrayOutputStream{
    val bmp = getImageFromPath(absolutePath)
    val bos = ByteArrayOutputStream()
    bmp?.compress(Bitmap.CompressFormat.JPEG, quality, bos)
    return bos
}

fun getImageFromPath(photoPath: String): Bitmap? {
    val bitmap = BitmapFactory.decodeFile(photoPath)
    var rotatedBitmap = bitmap
    var ei: ExifInterface? = null
    try {
        ei = ExifInterface(photoPath)
        val orientation: Int =
            ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED)
        rotatedBitmap = when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap, 90)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap, 180)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap, 270)
            ExifInterface.ORIENTATION_NORMAL -> bitmap
            else -> bitmap
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return rotatedBitmap
}

fun getFileSize(file: File): String {
    var fileSize = emptyString()
    if (file.isFile){
        val length = file.length().toDouble()
        if (length > mB) {
            return format.format(length / mB).toString() + " mb"
        }
        fileSize = if (length > kB) {
            format.format(length / kB).toString() + " kb"
        } else {
            format.format(length).toString() + " b"
        }
    }
    return fileSize
}

fun saveToInternalStorage(context: Context, fileName: String, bitmapImage: Bitmap, quality: Int): File? {
    val file = File(getBaseDirectoryPath(context, fileName))
    var fos: FileOutputStream? = null
    try {
        fos = FileOutputStream(file)
        bitmapImage.compress(Bitmap.CompressFormat.JPEG, quality, fos)
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        try {
            fos!!.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
    return file
}

fun createImageFile(context: Context, folderName:String, fileName: String, bitmap: Bitmap, onFileCreatedListener:((file:File)->Unit)? = null) {
    val path =  getBaseImageDirectory(context, folderName)
    val file = File(path, fileName)
    try {
        // Make sure the Pictures directory exists.
        if (path.mkdirs()) {
            Toast.makeText(context, "Not exist :" + path.name, Toast.LENGTH_SHORT).show()
        }
        val os: OutputStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
        os.flush()
        os.close()
        Log.i("ExternalStorage", "Writed " + path + file.name)
        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(
            context, arrayOf(file.toString()), null
        ) { path, uri ->
            Log.i("ExternalStorage", "Scanned $path:")
            Log.i("ExternalStorage", "-> uri=$uri")
        }
        Toast.makeText(context, file.name, Toast.LENGTH_SHORT).show()
        onFileCreatedListener?.invoke(file)
    } catch (e: java.lang.Exception) {
        // Unable to create file, likely because external storage is
        // not currently mounted.
        Log.w("ExternalStorage", "Error writing $file", e)
    }
}