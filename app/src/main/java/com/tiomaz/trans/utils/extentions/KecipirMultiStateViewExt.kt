package com.tiomaz.trans.utils.extentions

import com.tiomaz.trans.utils.custom.KecipirMultiStateView
import kotlinx.android.synthetic.main.layout_dialog.view.*


fun KecipirMultiStateView.showLoadingView(){
    this.viewState = KecipirMultiStateView.ViewState.LOADING
}

fun KecipirMultiStateView.showEmptyView(icon:Int? = null, title:String? = null, message:String? = null, action:String? = null, actionListener:(()->Unit)? = null){
    this.viewState = KecipirMultiStateView.ViewState.EMPTY

    this.getView(KecipirMultiStateView.ViewState.EMPTY)?.apply{
        if (icon != null) {
            imgIcon.setImageResource(icon)
        }
        if (title != null) {
            tvTitle.text = title
        }
        if (message != null) {
            tvMessage.text = message
        }
        if (action != null) {
            btnAction.text = action
            btnAction.makeVisible()
        }else{
            btnAction.makeGone()
        }

        btnAction.onSingleClickListener { actionListener?.invoke() }
    }
}

fun KecipirMultiStateView.showErrorView(icon:Int? = null, title:String? = null, message:String? = null, action:String? = null, actionListener:(()->Unit)? = null){
    this.viewState = KecipirMultiStateView.ViewState.ERROR

    this.getView(KecipirMultiStateView.ViewState.ERROR)?.apply{
        if (icon != null) {
            imgIcon.setImageResource(icon)
        }
        if (title != null) {
            tvTitle.text = title
        }
        if (message != null) {
            tvMessage.text = message
        }
        if (action != null) {
            btnAction.text = action
            btnAction.makeVisible()
            btnAction.onSingleClickListener { actionListener?.invoke() }
        }
    }
}

fun KecipirMultiStateView.showContentView(){
    this.viewState = KecipirMultiStateView.ViewState.CONTENT
}