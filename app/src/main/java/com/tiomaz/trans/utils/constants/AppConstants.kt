package com.tiomaz.trans.utils.constants

object AppConstants {

    const val PREFERENCE_NAME = "Kecipir_preference"

    const val HEADER_INTERCEPTOR_NAME = "header_interceptor"

    const val DATABASE_NAME = "Kecipir_database"

    const val REQUEST_OPEN_TASK_DETAIL = 50

    const val REQUEST_OPEN_TASK_LIST = 20

    const val REQUEST_START_TASK = 40

    const val REQUEST_FINISH_TASK = 60

    const val PERMISSIONS_REQUEST_ACCESS_LOCATION = 226

    const val PERMISSIONS_REQUEST_ACCESS_STORAGE = 229

    const val DEFAULT_ZOOM = 100
}