package com.tiomaz.trans.utils.common

import java.util.*

sealed class ResultState<T> {

    data class Loading<T>(val data: T?=null) : ResultState<T>()

    data class Empty<T>(val data: T?=null) : ResultState<T>()

    data class Success<T>(val data: T?) : ResultState<T>()

    data class NoInternetConnection<T>(val throwable: Throwable, val data: T?=null) : ResultState<T>()

    data class ServerError<T>(val throwable: Throwable, val data: T?=null) : ResultState<T>()

    data class Unauthorized<T>(val throwable: Throwable, val data: T?=null) : ResultState<T>()

    data class Error<T>(val throwable: Throwable, val data: T?=null) : ResultState<T>()

}