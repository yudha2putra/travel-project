package com.tiomaz.trans.utils.extentions

import android.animation.LayoutTransition
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.text.Editable
import android.text.Spannable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.TranslateAnimation
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.target.Target
import com.tiomaz.trans.BuildConfig
import com.tiomaz.trans.R
import com.tiomaz.trans.base.BaseActivity
import com.tiomaz.trans.base.BaseFragment
import com.tiomaz.trans.utils.common.BlurImageUtils

//region activity

fun showToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
}

fun BaseActivity.showToast(message: String?) {
    Toast.makeText(this, message ?: "Error Tidak Diketahui", Toast.LENGTH_SHORT).show()
}

fun AppCompatActivity.showToolbar(
    toolbar: androidx.appcompat.widget.Toolbar,
    title: String,
    isBack: Boolean
) {
    setSupportActionBar(toolbar)
    supportActionBar?.title = title
    if (isBack) {
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}

fun AppCompatActivity.replaceFragment(
    frameId: Int, fragment: Fragment,
    isBackStack: Boolean = true
) {
    supportFragmentManager.doTransaction {
        replace(frameId, fragment)
            .addToBackStack(if (isBackStack) fragment.tag else null)
    }
}

fun AppCompatActivity.addFragment(frameId: Int, fragment: Fragment, activeFragment: Fragment) {
    supportFragmentManager.beginTransaction().add(frameId, fragment, fragment.tag)
        .hide(activeFragment)
        .commit()
}

fun AppCompatActivity.addFirstFragment(frameId: Int, fragment: Fragment) {
    supportFragmentManager.beginTransaction().add(frameId, fragment, fragment.tag)
        .commit()
}

fun AppCompatActivity.showFragment(frameId: Int, fragment: Fragment, activeFragment: Fragment) {
    if (!fragment.isAdded){
        addFragment(frameId, fragment,activeFragment)
    }else{
        supportFragmentManager.beginTransaction()
            .hide(activeFragment)
            .show(fragment)
            .commit()
    }
}


fun AppCompatActivity.setStatusBarColor(color: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window?.apply {
            statusBarColor = ContextCompat.getColor(this@setStatusBarColor, color)
        }
    }
}

fun AppCompatActivity.openAppInGooglePlay() {
    val appId = BuildConfig.APPLICATION_ID
    try {
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appId")))
    } catch (anfe: ActivityNotFoundException) {
        this.startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=$appId")
            )
        )
    }
}

//region fragment

inline fun FragmentManager.doTransaction(
    func: FragmentTransaction.() ->
    FragmentTransaction
) {
    beginTransaction().func()
        .commit()
}

fun Fragment.replaceFragment(frameId: Int, fragment: Fragment) {
    childFragmentManager.doTransaction { replace(frameId, fragment) }
}

fun BaseFragment.showToast(message: String?) {
    Toast.makeText(requireContext(), message ?: "Error Tidak Diketahui", Toast.LENGTH_SHORT).show()
}

fun BaseFragment.setStatusBarColor(color: Int) {
    activity?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window: Window? = activity?.window
            window?.apply {
                statusBarColor = ContextCompat.getColor(requireContext(), color)
            }
        }
    }
}

fun <T> BaseFragment.getNavigationResult(key: String = "result") =
    findNavController().currentBackStackEntry?.savedStateHandle?.get<T>(key)

fun <T> BaseFragment.getNavigationResultLiveData(key: String = "result") =
    findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<T>(key)

fun <T> BaseFragment.setNavigationResult(result: T, key: String = "result") {
    findNavController().previousBackStackEntry?.savedStateHandle?.set(key, result)
}

fun FragmentManager.navigationFragments(): List<Fragment>? {
    return primaryNavigationFragment?.childFragmentManager?.fragments
}

//region ui view
fun EditText.toPhoneNumberWithDefaultPrefix() {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            s?.let { text ->
                if (text.startsWith("0")) {
                    this@toPhoneNumberWithDefaultPrefix.setText("")
                    this@toPhoneNumberWithDefaultPrefix.setSelection(this@toPhoneNumberWithDefaultPrefix.text.toString().length)
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

    })
}

fun AppCompatTextView.setAppendTextColor(text: String, color: Int, isBold: Boolean) {
    val word: Spannable = SpannableString(text)

    word.apply {
        setSpan(
            ForegroundColorSpan(color),
            0,
            length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        if (isBold) {
            setSpan(
                StyleSpan(Typeface.BOLD), 0, length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
    append(word)
}

fun View.makeInvisible() {
    visibility = View.INVISIBLE
}

fun View.makeVisible() {
    visibility = View.VISIBLE
}

fun View.makeGone() {
    visibility = View.GONE
}

fun ImageView.loadImageRound(context: Context, url: String, radius: Float, placeHolder: Int) {
    val requestOptions = RequestOptions.placeholderOf(placeHolder)
    Glide.with(context)
        .asBitmap()
        .load(url)
        .apply(requestOptions)
        .into(getRoundedImageTarget(context, this, radius))
}

fun ImageView.toGrayScale() {
    val colorMatrix = ColorMatrix()
    colorMatrix.setSaturation(0.0f)
    val filter = ColorMatrixColorFilter(colorMatrix)
    this.colorFilter = filter
}

fun ImageView.setUnlocked() {
    this.colorFilter = null
    this.imageAlpha = 255
}

fun ImageView.loadImageNoPlaceholder(url: String, placeHolder: Drawable? = null) {
    Glide.with(context)
        .load(url)
        .error(placeHolder)
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)
        .into(this)
}

fun ImageView.loadImage(url: String, progressBar: ProgressBar, placeHolder: Drawable?) {
    progressBar.makeVisible()
    Glide.with(context)
        .load(url)
        .placeholder(placeHolder)
        .error(placeHolder)
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                progressBar.makeGone()
                return true
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                progressBar.makeGone()
                setImageDrawable(resource)
                return true
            }

        })
        .into(this)
}

fun getRoundedImageTarget(
    context: Context, imageView: ImageView,
    radius: Float
): BitmapImageViewTarget {
    return object : BitmapImageViewTarget(imageView) {
        override fun setResource(resource: Bitmap?) {
            val circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.resources, resource)
            circularBitmapDrawable.cornerRadius = radius
            imageView.setImageDrawable(circularBitmapDrawable)
        }
    }
}

fun ImageView.loadImageCircle(url: String, placeHolder: Int) {
    val requestOptions = RequestOptions.placeholderOf(placeHolder)
    Glide.with(context)
        .load(url)
        .apply(requestOptions)
        .apply(RequestOptions.circleCropTransform())
        .into(this)
}

fun ViewGroup.setAnimationLayoutChanges() {
    val transition = LayoutTransition()
    transition.setAnimateParentHierarchy(false)
    this.layoutTransition = transition
}

fun View.animateFadeOut(duration: Int = 500) {
    alpha = 0f
    visibility = View.VISIBLE
    animate()
        .alpha(1f).duration = duration.toLong()
}

fun View.animateSlideUp(duration: Int = 500) {
    visibility = View.VISIBLE
    val animate = TranslateAnimation(0f, 0f, this.height.toFloat(), 0f)
    animate.duration = duration.toLong()
    animate.fillAfter = true
    this.startAnimation(animate)
}

fun View.animateSlideDown(duration: Int = 500) {
    visibility = View.VISIBLE
    val animate = TranslateAnimation(0f, 0f, 0f, this.height.toFloat())
    animate.duration = duration.toLong()
    animate.fillAfter = true
    this.startAnimation(animate)
}

fun AppCompatTextView.leftDrawable(@DrawableRes id: Int = 0) {
    this.setCompoundDrawablesWithIntrinsicBounds(id, 0, 0, 0)
}

fun AppCompatTextView.showError(message: String?) {
    makeVisible()
    text = message ?: "Error tidak diketahui"
}

fun AppCompatTextView.showValid(message: String) {
    makeVisible()
    text = message
}

fun View.onSingleClickListener(action: () -> Unit) {
    this.setOnClickListener {
        action.invoke()
        this.isEnabled = false
        postDelayed({
            this.isEnabled = true
        }, 500)
    }
}
