package com.tiomaz.trans.utils.di

import com.tiomaz.trans.data.api.remote.*
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    single { provideMembershipApi(get()) }

}

fun provideMembershipApi(retrofit: Retrofit):MembershipApi = retrofit.create(MembershipApi::class.java)
