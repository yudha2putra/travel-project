package com.tiomaz.trans.utils.constants

object IntentKeys {

    const val KEY_PATH = "path"
    const val KEY_URL = "url"
    const val KEY_FILE = "file"
    const val KEY_INCOME = "income"
    const val KEY_TASK = "task"
    const val KEY_LOCATION = "location"
    const val KEY_ATTACHMENT = "attachment"
    const val KEY_POSITION = "position"
    const val KEY_USER = "user"
    const val KEY_PARK_TICKET = "park ticket"
    const val KEY_IS_CARD = "is_card"
}