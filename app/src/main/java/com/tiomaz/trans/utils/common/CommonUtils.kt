package com.tiomaz.trans.utils.common

import CustomBlurDrawable
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.provider.Settings
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StyleSpan
import android.view.View
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tiomaz.trans.BuildConfig
import com.tiomaz.trans.utils.extentions.showToast
import java.net.URLEncoder

fun getAppInfo(context: Context): PackageInfo? {
    val manager = context.packageManager
    return manager.getPackageInfo(context.packageName, PackageManager.GET_ACTIVITIES)
}

fun getVersionCode():Int{
    return BuildConfig.VERSION_CODE
}

fun turnOnGps(context: Context){
    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
    context.startActivity(intent)
}

fun openWhatsApp(context: Context, phoneNumber:String = "+628128271890"){
    try {
        val packageManager: PackageManager = context.packageManager
        val i = Intent(Intent.ACTION_VIEW)
        val url =
            "https://api.whatsapp.com/send?phone=$phoneNumber&text=" + URLEncoder.encode(
                "",
                "UTF-8"
            )
        i.setPackage("com.whatsapp")
        i.data = Uri.parse(url)
        if (i.resolveActivity(packageManager) != null) {
            context.startActivity(i)
        } else {
            showToast(
                context,
                context.getString(com.tiomaz.trans.R.string.app_name)
            )
        }
    } catch (e: Exception) {
        showToast(
            context,
            e.localizedMessage ?: context.getString(com.tiomaz.trans.R.string.app_name)
        )
    }

}

fun openMapDirection(context: Context, currentPosition:LatLng, destinationPosition:LatLng){
    val uri =
        "http://maps.google.com/maps?f=d&hl=en&saddr=" + currentPosition.latitude.toString() + "," + currentPosition.longitude.toString() + "&daddr=" + destinationPosition.latitude.toString() + "," + destinationPosition.longitude
    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
    context.startActivity(Intent.createChooser(intent, "Select an application"))
}

fun toBlurView(beneathView:View, blurView:View, radius:Int){
    val blurDrawable = CustomBlurDrawable(beneathView, radius)
    blurView.setBackgroundDrawable(blurDrawable)
}

fun setBoldOnKeywords(text: String,
                      searchKeywords: Array<String>): SpannableStringBuilder {
    val span = SpannableStringBuilder(text)

    for (keyword in searchKeywords) {

        var offset = 0
        var start: Int
        val len = keyword.length

        start = text.indexOf(keyword, offset, true)
        while (start >= 0) {
            val spanStyle = StyleSpan(Typeface.BOLD)
            span.setSpan(spanStyle, start, start + len, Spanned.SPAN_INCLUSIVE_INCLUSIVE)

            offset = start + len
            start = text.indexOf(keyword, offset, true)
        }
    }
    return span
}

val gson = Gson()

//convert a map to a data class
inline fun <reified T> Map<String, Any>.toDataClass(): T {
    return convert()
}

//convert a data class to a map
fun <T> T.serializeToMap(): Map<String, Any> {
    return convert()
}

//convert an object of type I to type O
inline fun <I, reified O> I.convert(): O {
    val json = gson.toJson(this)
    return gson.fromJson(json, object : TypeToken<O>() {}.type)
}