package com.tiomaz.trans.utils.validation

enum class RulesValidationEnum (){
    EMAIL,
    NUMBER,
    PHONE_NUMBER,
    ALPHABET,
    PASSWORD,
    NOT_EMPTY,
    RETYPE_PASSWORD,
    MAX_MIN_CHARACTER
}