package com.tiomaz.trans.utils.extentions

import android.content.Context
import android.graphics.Typeface.BOLD
import android.graphics.Typeface.NORMAL
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.tiomaz.trans.R
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.ByteArrayOutputStream
import java.io.File

fun emptyString() = ""

fun getDeviceId(context: Context):String{
    return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
}

fun String.getColoredSpanned(color: String): String? {
    return "<font color=$color>$this</font>"
}

fun String.getColorSpanned(color: Int, style: Int, startIndex:Int, endIndex:Int):Spannable {
    val spannable = SpannableString(this)
    spannable.setSpan(
        ForegroundColorSpan(color),
        startIndex, endIndex,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    spannable.setSpan(
        StyleSpan(style),
        startIndex, endIndex,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    return spannable
}

fun String.getInitialName(): String {
    return this
        .split(' ')
        .mapNotNull { it.firstOrNull()?.toString() }
        .reduce { acc, s -> acc + s }
}

fun Context.generateBottomSheetDialog(
    layoutId: Int,
    cancelable: Boolean = false
): BottomSheetDialog {
    return BottomSheetDialog(this, R.style.AppBottomSheetDialogTheme).apply {
        val view = View.inflate(context, layoutId, null)
        setContentView(view)
        setCancelable(cancelable)
        setCanceledOnTouchOutside(cancelable)
        show()
    }
}

fun String.toRequestBody(): RequestBody {
    return this.toRequestBody("text/plain".toMediaTypeOrNull())
}

fun File.toRequestBody(content:ByteArrayOutputStream): RequestBody {
    return RequestBody.create("image/jpeg".toMediaTypeOrNull(),content.toByteArray())
}

class SpannableBuilder {
    lateinit var what: Any
    var flags: Int = 0
}

fun SpannableString.spanWith(target: String, styleSpan: StyleSpan = StyleSpan(NORMAL), apply: SpannableBuilder.() -> Unit) {
    val builder = SpannableBuilder()
    apply(builder)

    val start = this.indexOf(target)
    val end =  start + target.length

    setSpan(builder.what, start, end, builder.flags)
    setSpan(styleSpan, start, end, builder.flags)
}