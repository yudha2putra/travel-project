package com.tiomaz.trans.utils.enum

enum class TaskStatus(val type:String){
    ASSIGNED("assigned"),
    ACCEPTED("accepted"),
    CANCELED("cancel"),
    ONGOING("ongoing"),
    FINISH("completed")
}
