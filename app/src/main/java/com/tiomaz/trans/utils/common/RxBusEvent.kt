package com.tiomaz.trans.utils.common

class RxBusEvent {
    data class OnFcmTokenEvent(val token:String)

    data class OnTaskAssignEvent(val isAssign:Boolean)
}