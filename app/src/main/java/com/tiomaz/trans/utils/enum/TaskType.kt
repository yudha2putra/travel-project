package com.tiomaz.trans.utils.enum

enum class TaskType(val type:String){
    PICK_DROP("Antar-Ambil"),
    PICK("Ambil"),
    DROP("Antar")
}
