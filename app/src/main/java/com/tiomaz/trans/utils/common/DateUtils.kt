package com.tiomaz.trans.utils.common

import java.text.SimpleDateFormat
import java.util.*

fun getCurrentDate(format: String): String {
    val date = Calendar.getInstance().time
    val newFormat = SimpleDateFormat(format)
    return newFormat.format(date)
}

fun changeFormatLongDate(date: Long, format: String): String {
    val date = Date(date)
    val df2 = SimpleDateFormat(format, Locale("in"))
    return df2.format(date)
}

fun changeFormatStringDate(
    date: String,
    formatNew: String,
    formatOld: String = "yyyy-MM-dd"
): String {
    val oldFormat = SimpleDateFormat(formatOld, Locale("in"))
    val oldDate = oldFormat.parse(date)
    val newFormat = SimpleDateFormat(formatNew, Locale("in"))
    return newFormat.format(oldDate)
}

fun changeStringDateToLong(dateString: String, format: String): Long {
    val sdf = SimpleDateFormat(format, Locale("in"))
    val date = sdf.parse(dateString)

    return date.time
}

fun getElapsedtTime(time: Long, timeString: String): String? {
    var timeElapsed = (System.currentTimeMillis() - time) / 1000
    return if (timeElapsed < 60) {
        timeString
    } else if (timeElapsed < 3600) {
        timeElapsed /= 60
        timeString
    } else if (time < 86400) {
        timeElapsed /= 3600
        timeString
    } else {
        timeElapsed /= 86400
        if (timeElapsed == 1L) {
            return "Kemarin"
        }
        return changeFormatLongDate(time, "dd/MM/yyyy")
    }
}

fun getLastWeek(): String {
    var calendar = Calendar.getInstance()
    // Monday
    calendar.add(Calendar.DAY_OF_YEAR, -13)
    val mDateMonday = calendar.time

    // Sunday
    calendar.add(Calendar.DAY_OF_YEAR, 6)
    val mDateSunday = calendar.time

    // Date format
    val strDateFormat = "yyyy-MM-dd"
    val sdf = SimpleDateFormat(strDateFormat)
    val startDate = sdf.format(mDateMonday)
    val endDate = sdf.format(mDateSunday)


    return "$startDate:$endDate"
}

fun getDaysInterval(daysAgo: Int): String {
    val calendar = Calendar.getInstance()
    calendar.add(Calendar.DAY_OF_YEAR, daysAgo)
    val strDateFormat = "yyyy-MM-dd"
    val sdf = SimpleDateFormat(strDateFormat)
    val formatDate = sdf.format(calendar.time)
    return formatDate
}