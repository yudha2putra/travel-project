package com.tiomaz.trans.utils.validation

import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.doOnTextChanged
import com.google.android.material.textfield.TextInputLayout
import com.tiomaz.trans.utils.extentions.emptyString
import com.tiomaz.trans.utils.extentions.showValid
import java.lang.IllegalArgumentException
import java.util.regex.Pattern

typealias ViewRuleForm = Triple<View, List<Pair<RulesValidationEnum, String>>, Pair<Any, Any>?>

var password = emptyString()

fun isPasswordMatch(password: String?, confirmPassword: String?): Boolean {
    return confirmPassword == password
}

fun setMinMaxCharacter(min: Int?, max: Int?): String {
    return "^[a-zA-Z0-9]{$min,$max}\$"
}

fun doExtractValue(view: View): String {
    return when (view) {
        is EditText -> {
            view.text.toString().trim()
        }
        is TextInputLayout -> {
            view.editText!!.text.toString().trim()
        }
        else -> {
            throw IllegalArgumentException("Not applicable view")
        }
    }
}

fun getErrorMessage(rule: RulesValidationEnum, minMax: Pair<Int, Int>? = null): String {
    return when (rule) {
        RulesValidationEnum.EMAIL -> "Invalid Email"
        RulesValidationEnum.PASSWORD -> "Invalid Password"
        RulesValidationEnum.ALPHABET -> "Alphabet only"
        RulesValidationEnum.NUMBER -> "Invalid Number"
        RulesValidationEnum.PHONE_NUMBER -> "Invalid Phone Number"
        RulesValidationEnum.NOT_EMPTY -> "Field Required"
        RulesValidationEnum.RETYPE_PASSWORD -> "Password not match"
        RulesValidationEnum.MAX_MIN_CHARACTER -> "Field should be less than ${minMax?.first}"
    }
}

fun setupErrorView(isValid: Boolean, view: View, errorMessage: String) {
    when (view) {
        is TextInputLayout -> {
            if (!isValid) view.error = errorMessage else view.error = null
        }
        is AppCompatEditText -> {
            if (!isValid) view.error = errorMessage else view.error = null
        }
        is EditText -> {
            if (!isValid) view.error = errorMessage else view.error = null
        }
    }
}

fun doCheckRule(viewRuleForm: ViewRuleForm): Pair<Boolean, String> {
    val valids = mutableListOf<Boolean>()
    var errorMessage = emptyString()
    val view = viewRuleForm.first
    val input = doExtractValue(view)
    val rules = viewRuleForm.second
    val additionalVariableOne = viewRuleForm.third?.first
    val additionalVariableTwo = viewRuleForm.third?.second
    var isValid = false
    rules.forEach {
        when (it.first) {
            RulesValidationEnum.EMAIL -> {
                isValid = doCheckPattern(input, RegexUtils.EMAIL)
                valids.add(isValid)
                if (!isValid) errorMessage = it.second
            }
            RulesValidationEnum.PASSWORD -> {
                password = input
                isValid = true
                valids.add(isValid)
                if (!isValid) errorMessage = it.second
            }
            RulesValidationEnum.ALPHABET -> {
                isValid = doCheckPattern(input, RegexUtils.ALPHANUMERIC)
                valids.add(isValid)
                if (!isValid) errorMessage = it.second
            }
            RulesValidationEnum.NUMBER -> {
                isValid = doCheckPattern(input, RegexUtils.NUMBER)
                valids.add(isValid)
                if (!isValid) errorMessage = it.second
            }
            RulesValidationEnum.PHONE_NUMBER -> {
                isValid = doCheckPattern(input, RegexUtils.PHONE_NUMBER)
                valids.add(isValid)
                if (!isValid) errorMessage = it.second
            }
            RulesValidationEnum.NOT_EMPTY -> {
                isValid = doCheckPattern(input, RegexUtils.NOT_EMPTY)
                valids.add(isValid)
                if (!isValid) errorMessage = it.second
            }
            RulesValidationEnum.RETYPE_PASSWORD -> {
                isValid = isPasswordMatch(
                    password,
                    input
                )
                valids.add(isValid)
                if (!isValid) errorMessage = it.second
            }
            RulesValidationEnum.MAX_MIN_CHARACTER -> {
                isValid = doCheckPattern(
                    input, setMinMaxCharacter(
                        additionalVariableOne as Int?,
                        additionalVariableTwo as Int?
                    )
                )
                valids.add(isValid)
                if (!isValid) errorMessage = it.second
            }
        }
    }

    return Pair(valids.none { !it }, errorMessage)
}

fun doCheckPattern(input: String, regex: String): Boolean {
    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(input)
    return matcher.matches()
}

fun doValidateForm(viewRuleForm: List<ViewRuleForm>, button: Button, views: List<View> = listOf()) {
    val isValid = mutableListOf<Boolean>()

    viewRuleForm.forEachIndexed { index, rule ->
        when (val view = rule.first) {
            is EditText -> {
                isValid.add(view.text.isNotEmpty())
                view.doOnTextChanged { text, start, count, after ->
                    val valid = doCheckRule(rule)
                    setupErrorView(valid.first, view, valid.second)
                    isValid[index] = valid.first

                    doEnableButton(
                        isValid.none { !it },
                        button,
                        views
                    )
                }
            }

            is TextInputLayout -> {
                isValid.add(!view.editText?.text.isNullOrEmpty())
                view.editText?.doOnTextChanged { text, start, count, after ->
                    val valid = doCheckRule(rule)
                    setupErrorView(valid.first, view, valid.second)
                    isValid[index] = valid.first

                    doEnableButton(
                        isValid.none { !it },
                        button,
                        views
                    )
                }
            }
        }
    }
}

fun doEnableButton(isValid: Boolean, button: Button, views: List<View>) {
    button.isEnabled = if (views.isEmpty()) isValid else isValid && isFullFormValidate(views)
}

fun doShowPasswordMatch(tvValid: AppCompatTextView) {
    tvValid.showValid("Sandi sudah sesuai")
}

fun isFormValidate(views: List<TextInputLayout>): Boolean {
    return views.none { it.editText!!.text.isEmpty() } && views.none { it.error != null }
}

fun isFullFormValidate(views: List<View>): Boolean {
    return views.none {
        when (it) {
            is EditText -> it.text.isEmpty()
            is TextInputLayout -> it.editText!!.text.isEmpty()
            else -> false
        }

    } && views.none {
        when (it) {
            is EditText -> it.error != null
            is TextInputLayout -> it.error != null
            else -> false
        }
    }
}

fun isFormComplete(views: List<TextInputLayout>): Boolean {
    return views.none { it.editText!!.text.isEmpty() }
}