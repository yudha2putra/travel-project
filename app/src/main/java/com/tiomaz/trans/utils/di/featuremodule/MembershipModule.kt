package com.tiomaz.trans.utils.di.featuremodule

import com.tiomaz.trans.domain.repository.MembershipRepository
import com.tiomaz.trans.data.repository.MembershipRepositoryImpl
import com.tiomaz.trans.domain.usecase.contract.MembershipUseCase
import com.tiomaz.trans.domain.usecase.implementation.MembershipUseCaseImpl
import com.tiomaz.trans.presentation.viewmodel.MembershipViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val membershipModule = module {
    single<MembershipRepository> {
        MembershipRepositoryImpl(
            get(),
            get(),
            get(),
            get()
        )
    }

    single<MembershipUseCase> { MembershipUseCaseImpl(get()) }
//    single { MembershipRepositoryImpl(get(),get(),get()) as MembershipUseCase }
    viewModel { MembershipViewModel(get()) }
}