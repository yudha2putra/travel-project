package com.tiomaz.trans.utils.constants

object PreferenceKeys {

    const val KEY_DEVICE_ID = "key_device_id"

    const val KEY_LOGIN = "key_login"

    const val KEY_TOKEN = "key_token"

    const val KEY_USERNAME = "key_username"

    const val KEY_IS_DELIVERY_PROOF = "key_delivery_proof"

}