package com.tiomaz.trans.utils.enum

enum class AttachmentType (val type:String){
    AVATAR("photo"),
    ID_CARD("KTP"),
    DRIVER_LICENSE("SIM"),
    VEHICLE("kendaraan")
}