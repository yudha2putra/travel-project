package com.tiomaz.trans.utils.di

import com.tiomaz.trans.data.db.KecipirDatabase
import org.koin.dsl.module

val dbModule = module {
    single { KecipirDatabase.database(get()) }

    single { get<KecipirDatabase>().orderDao() }
}