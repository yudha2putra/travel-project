package com.tiomaz.trans.utils.constants

import com.tiomaz.trans.utils.enum.TaskType

fun TaskType.readableTaskType():String{
    return when(this){
        TaskType.PICK_DROP -> "Antar Ambil"
        TaskType.PICK -> "Ambil"
        TaskType.DROP -> "Antar"
    }
}
