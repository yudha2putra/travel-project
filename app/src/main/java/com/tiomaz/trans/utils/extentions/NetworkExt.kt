package com.tiomaz.trans.utils.extentions

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.tiomaz.trans.utils.common.*
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.http.Body
import java.util.*

fun hasNetwork(context: Context): Boolean {
    var result = false
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as
            ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        connectivityManager.run {
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.run {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            }
        }
    } else {
        connectivityManager.run {
            connectivityManager.activeNetworkInfo?.run {
                if (type == ConnectivityManager.TYPE_WIFI) {
                    if (this.isConnected) {
                        result = true

                    }
                } else if (type == ConnectivityManager.TYPE_MOBILE) {
                    if (this.isConnected) {
                        result = true

                    }
                }
            }
        }
    }
    return result
}

fun <T : Any> handleApiSuccess(jsonrpc: String? = null, id: String?, result: T?): ResultState<T> {
    return when (jsonrpc) {
        "2.0" -> {
                ResultState.Success(result)
        }
        else -> ResultState.Error(Throwable())
    }
}

fun <T : Any> handleApiError(exception: java.lang.Exception): ResultState<T> {
    return when (exception) {
        is HttpException -> {
            when (exception.code()) {
                504 -> {
                    ResultState.NoInternetConnection(throwable = Throwable("Tidak ada koneksi internet. Silahkan coba lagi"))
                }
                500 -> {
                    ResultState.ServerError(throwable = Throwable("Tidak ada koneksi internet. Silahkan coba lagi"))
                }
                else -> {
                    ResultState.Error(throwable = exception)
                }
            }
        }
        else -> {
            ResultState.Error(throwable = exception)
        }
    }
}

fun <T : Any> handleApiError(errorBody: ResponseBody?): ResultState.Error<T> {
    val error = ApiErrorOperator.parseError(errorBody)
    return if(!error.status_code.isNullOrEmpty()){
        ResultState.Error(Throwable(error.error))
    }else{
        ResultState.Error(Throwable(error.message))
    }
}

fun <T : Any> handleApiError(errorCode:Int, errorBody: ResponseBody?): ResultState<T> {
    return when (errorCode) {
        504 -> {
            ResultState.NoInternetConnection(throwable = Throwable("Tidak ada koneksi internet. Silahkan coba lagi"))
        }
        500 -> {
            ResultState.ServerError(throwable = Throwable("Server Bermasalah. Tidak perlu khawatir, silahkan tunggu beberapa saat."))
        }
        404 -> {
            ResultState.Error(throwable = Throwable("Error tidak diketahui"))
        }
        401 -> {
            ResultState.Unauthorized(throwable = Throwable("Sesi anda telah berakhir. Silahkan login kembali"))
        }
        else -> {
            if (errorBody != null){
                val error = ApiErrorOperator.parseError(errorBody)
                ResultState.Error(Throwable(error.message))
            }else{
                ResultState.Error(Throwable("Error tidak diketahui"))
            }
        }
    }
}


