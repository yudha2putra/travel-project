package com.tiomaz.trans.base

import com.google.gson.annotations.SerializedName
import java.util.*

data class BaseDataSourceApi(
    @SerializedName("jsonrpc")
    val jsonrpc:String?,
    @SerializedName("id")
    val id:String?,
    @SerializedName("result")
    val result: Objects?
)