package com.tiomaz.trans.base

open class BaseSelectionModel(
    open var isSelected:Boolean = false
)