package com.tiomaz.trans.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.tiomaz.trans.R
import com.tiomaz.trans.utils.common.showKecipirBottomSheetDialog
import com.tiomaz.trans.utils.custom.CustomProgressDialog

abstract class BaseFragment :Fragment(){

    abstract val layout:Int

    private var progressBar: CustomProgressDialog? = null

    abstract fun onPreparation()
    abstract fun onIntent()
    abstract fun onUi()
    abstract fun onAction()
    abstract fun onObserver()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(layout, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onPreparation()
        onIntent()
        onUi()
        onAction()
        onObserver()
    }

    fun showProgress() {
        progressBar?.dismiss()
        progressBar = null
        progressBar = CustomProgressDialog(requireContext())
        progressBar?.show()
    }

    fun dismissProgress() {
        progressBar?.dismiss()
        progressBar = null
    }


    fun showOnNoInternetConnection(onRetry:(()-> Unit)? = null){
        showKecipirBottomSheetDialog(
            context = requireContext(),
            title = "Tidak Terhubung ke Internet",
            message = "Aktifkan koneksi internet Anda untuk melanjutkan perjalanan bersama Deliveri.id",
            icon = R.drawable.ic_reg_header,
            positive = "Coba Lagi",
            positiveListener = {
                onRetry?.invoke()
            }
        )
    }

    fun showOnServerError(onRetry:(()-> Unit)? = null){
        showKecipirBottomSheetDialog(
            context = requireContext(),
            title = "Upss, Ada yang Salah",
            message = "Tidak perlu khawatir, silahkan tunggu beberapa saat.",
            icon = R.drawable.ic_reg_header,
            positive = "Coba Lagi",
            positiveListener = {
                onRetry?.invoke()
            }
        )
    }

}