package com.tiomaz.trans.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseSelectionAdapter<T : BaseSelectionModel, Holder: BaseSelectionViewHolder<T>>(
    private var datas:List<T>):
    BaseRecyclerViewAdapter<T, Holder>(datas)
{
    fun notifySelectionChanged(position: Int){
        datas.forEachIndexed { index, data ->
            data.isSelected = index == position
        }
        notifyDataSetChanged()
    }
}