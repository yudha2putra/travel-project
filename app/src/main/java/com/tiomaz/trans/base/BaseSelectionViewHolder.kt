package com.tiomaz.trans.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseSelectionViewHolder<T : BaseSelectionModel>(val view:View):RecyclerView.ViewHolder(view){
    abstract fun bind(data:T)
}