package com.tiomaz.trans.base

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.tiomaz.trans.utils.di.apiModule
import com.tiomaz.trans.utils.di.dbModule
import com.tiomaz.trans.utils.di.featuremodule.*
import com.tiomaz.trans.utils.di.networkModule
import com.tiomaz.trans.utils.di.preferenceModule
import org.koin.android.ext.koin.androidContext
import org.koin.java.KoinJavaComponent.inject
import org.koin.core.context.startKoin

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@BaseApplication)
            modules(
                listOf(
                    networkModule,
                    dbModule,
                    preferenceModule,
                    apiModule,
                    membershipModule
                )
            )
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

}