package com.tiomaz.trans.base

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tiomaz.trans.R
import com.tiomaz.trans.data.preference.KecipirPreference
import com.tiomaz.trans.presentation.verification.LoginActivity
import com.tiomaz.trans.utils.common.*
import com.tiomaz.trans.utils.constants.PreferenceKeys
import com.tiomaz.trans.utils.custom.CustomProgressDialog
import com.tiomaz.trans.utils.extentions.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

abstract class BaseActivity : AppCompatActivity() {

    private val preference: KecipirPreference by inject()

    private var progressBar: CustomProgressDialog? = null

    abstract val layout: Int

    abstract fun onPreparation()
    abstract fun onIntent()
    abstract fun onUi()
    abstract fun onAction()
    abstract fun onObserver()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)

        onPreparation()
        onIntent()
        onUi()
        onAction()
        onObserver()
    }

    fun showProgress() {
        progressBar?.dismiss()
        progressBar = null
        progressBar = CustomProgressDialog(this)
        progressBar?.show()
    }

}