package com.tiomaz.trans.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<T, Holder: RecyclerView.ViewHolder>(
    private var datas:List<T>):
    RecyclerView.Adapter<Holder>()
{

    fun notifyDataUpdate(newData:List<T>){
        datas = newData
        notifyDataSetChanged()
    }

    fun notifyDataItemUpdate(position: Int, newData: T){
        datas.toMutableList()[position] = newData
        notifyItemChanged(position,newData)
    }
}