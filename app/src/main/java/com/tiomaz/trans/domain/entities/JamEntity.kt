package com.tiomaz.trans.domain.entities

data class JamEntity(
    val id:Int,
    val jam:String,
    val etd:Int,
    val kursi:String,
    val keberangkatanMain:String,
    var tujuanMain:String,
    val defaultCode:String,
    val lessthan:Boolean
)