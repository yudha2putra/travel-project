package com.tiomaz.trans.domain.usecase.contract

import com.tiomaz.trans.domain.entities.Task
import com.tiomaz.trans.utils.common.ResultState
import java.io.File

interface HistoryUseCase {
    suspend fun fetchDailyHistory(taskDate:String): ResultState<List<Task>>
    suspend fun fetchDetailHistory(taskId:String):ResultState<Task>
    suspend fun postParkingTicket(taskId:String,image: File):ResultState<Boolean>
}