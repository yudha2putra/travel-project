package com.tiomaz.trans.domain.entities

import com.google.gson.annotations.SerializedName

data class Notification(
    @SerializedName("title")
    val title:String,
    @SerializedName("message")
    val body:String
)