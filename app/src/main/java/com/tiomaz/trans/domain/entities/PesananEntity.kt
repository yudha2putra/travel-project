package com.tiomaz.trans.domain.entities

import java.io.Serializable

data class PesananEntity(
    val id:Int,
    val tanggal:String,
    val kodeBooking:String,
    val dari:String,
    val tujuan:String,
    var seat:String,
    val total:String,
    val state:String,
    val name: String,
    val hargaTiket: String,
    val jumlahPenumpang: String,
    val chargeJemput: String,
    val chargeAntar: String,
    val subtotal: String,
    val diskon: String,
    val trx:String,
    val statusBayar:String,
) : Serializable