package com.tiomaz.trans.domain.entities

import com.tiomaz.trans.base.BaseSelectionModel
import com.tiomaz.trans.utils.extentions.emptyString

data class Vehicle(
    val type:String = emptyString(),
    val platNumber:String = emptyString(),
    val description:String = emptyString()
):BaseSelectionModel()