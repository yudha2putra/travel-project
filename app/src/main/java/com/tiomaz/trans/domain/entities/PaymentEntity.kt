package com.tiomaz.trans.domain.entities

import java.io.Serializable

data class PaymentEntity(
    var id:Int,
    var name:String,
    var image:String,
    var explanation:String,
    var idPayment:Int,
    var trxID:String
): Serializable