package com.tiomaz.trans.domain.usecase.contract

import com.tiomaz.trans.utils.common.ResultState

interface NotificationUseCase {

    suspend fun postFcmToken(token:String): ResultState<Boolean>

}