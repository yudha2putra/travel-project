package com.tiomaz.trans.domain.entities

import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

data class BookingEntity(
    var keberangkatan:String,
    var tujuan:String,
    var tanggal:String,
    var penumpang:String,
    var jam:String,
    var idJam:Int,
    var lessthan:Boolean,
    var kodeBook:String,
    var total:Int,
    val keberangkatanTime:String,
    var keberangkatanLat:String,
    var keberangkatanLong:String,
    var keberangkatanMain:String,
    var keberangkatanRadius:String,
    var tujuanLat:String,
    var tujuanLong:String,
    var tujuanMain:String,
    var tujuanRadius:String,

): Serializable