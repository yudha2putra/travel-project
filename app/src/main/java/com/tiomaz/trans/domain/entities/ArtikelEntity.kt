package com.tiomaz.trans.domain.entities

import java.io.Serializable

data class ArtikelEntity(
    val id:Int,
    val name:String,
    val image:String,
    val content:String,
):Serializable