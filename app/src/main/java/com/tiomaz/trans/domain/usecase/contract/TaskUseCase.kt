package com.tiomaz.trans.domain.usecase.contract

import com.google.android.gms.maps.model.LatLng
import com.tiomaz.trans.domain.entities.Reason
import com.tiomaz.trans.domain.entities.Task
import com.tiomaz.trans.domain.entities.User
import com.tiomaz.trans.utils.common.ResultState
import com.tiomaz.trans.utils.extentions.emptyString
import java.io.File

interface TaskUseCase {

    suspend fun fetchUnconfirmedTasks(): ResultState<List<Task>>

    suspend fun postConfirmTask(
        taskId: Int,
        status: String,
        location: List<String>,
        reason: String
    ): ResultState<Boolean>

    suspend fun fetchTask(taskId: String): ResultState<Task>

    suspend fun fetchAcceptedTasks(): ResultState<List<Task>>

    suspend fun postCompletedTask(
        taskId: Int, status: String, location: List<String>,
        image: File, reason: String = emptyString()
    ): ResultState<Boolean>

    suspend fun fetchReasonDeclineTasks():ResultState<List<Reason>>

}