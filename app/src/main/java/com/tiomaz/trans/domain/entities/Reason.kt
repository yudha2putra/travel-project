package com.tiomaz.trans.domain.entities

import com.tiomaz.trans.base.BaseSelectionModel

data class Reason(
    var name:String
): BaseSelectionModel()