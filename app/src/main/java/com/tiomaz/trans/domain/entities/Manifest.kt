package com.tiomaz.trans.domain.entities

import android.os.Parcelable
import com.tiomaz.trans.utils.extentions.emptyString
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Manifest(
    val icon: String = emptyString(),
    val name: String = emptyString(),
    val qty: Int = 0
) : Parcelable