package com.tiomaz.trans.domain.usecase.contract

import com.tiomaz.trans.domain.entities.Income
import com.tiomaz.trans.domain.entities.Vehicle
import com.tiomaz.trans.utils.common.ResultState

interface IncomeUseCase {
    suspend fun fetchIncomes(): ResultState<List<Income>>
}