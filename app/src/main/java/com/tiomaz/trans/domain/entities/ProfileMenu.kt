package com.tiomaz.trans.domain.entities

data class ProfileMenu(
    val id:Int,
    val icon:Int,
    val title:String
)