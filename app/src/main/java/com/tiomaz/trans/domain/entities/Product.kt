package com.tiomaz.trans.domain.entities

data class Product(
    val type: String,
    val quantity:Int,
    val iconUrl:String = "https://staging.kecipir.com/delivery/api/v1/image/item/Box.png"
)