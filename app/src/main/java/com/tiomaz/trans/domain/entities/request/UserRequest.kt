package com.tiomaz.trans.domain.entities.request

import java.io.File

data class UserRequest(
    val email: String,
    val name: String,
    val phoneNumber: String,
    val address: String,
    val password: String,
    val plateNo: String,
    val vehicle: String,
    val vehicleDescription: String
)