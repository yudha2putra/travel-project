package com.tiomaz.trans.domain.entities

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.tiomaz.trans.utils.enum.TaskStatus
import com.tiomaz.trans.utils.extentions.emptyString
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Task(
    val id:Int = 0,
    val addressVerified:String = emptyString(),
    val address:String = emptyString(),
    val isAddressVerified:Boolean = false,
    val distance:String = emptyString(),
    val memberName:String = emptyString(),
    val memberPhoneNumber:String = "+628998501067",
    val taskType:String = emptyString(),
    val rate:String = emptyString(),
    val note:String = "Rumah pojok, cat tembok coklat dengan pagar warna hitam.",
    val status:String = emptyString(),
    val invoice: String = emptyString(),
    val manifest: List<Manifest> = listOf(),
    val parkingValue: String = emptyString(),
    val penaltyValue: String = emptyString(),
    val taskDate: String = emptyString(),
    val taskPhoto: String = emptyString(),
    val totalRate: String = emptyString(),
    val destinationLocation:LatLng = LatLng(0.0,0.0),
    val declineReason:String = emptyString(),
    var parkTicketPhoto:String = emptyString(),
    var timeCompleted:String = emptyString()
) : Parcelable {

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }
}