package com.tiomaz.trans.domain.entities

import java.io.Serializable

data class PassengerEntity(
    val seat:Int,
    val idSeat:Int,
    var name:String,
    val partnerID:Int,
    val routeID:Int,
    var jemput:Boolean,
    var antar:Boolean,
    var longitude:String,
    var latitude:String,
    var longitudeInfo:String,
    var latitudeInfo:String,
    var nomor:String,
    val berangkat:String,
    val tiba:String,
    var pickupPrice:Double,
    var deliverPrice:Double,
):Serializable