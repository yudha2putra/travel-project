package com.tiomaz.trans.domain.entities

import android.content.Context
import com.tiomaz.trans.R

data class Step(
    val step:String,
    var isActive:Boolean = false
)

fun getRegisterSteps(context: Context):List<Step>{
    return  mutableListOf(
        Step("1. Data Diri", true),
        Step("2. Data Kendaraan"),
        Step("3. Berkas")
    )
}