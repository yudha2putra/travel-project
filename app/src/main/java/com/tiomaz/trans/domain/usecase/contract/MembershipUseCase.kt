package com.tiomaz.trans.domain.usecase.contract

import com.tiomaz.trans.utils.common.ResultState
import java.util.*

interface MembershipUseCase {

    suspend fun postLogin(login:String, password:String, db:String): ResultState<Objects>

    suspend fun postRegister(email:String, password:String, telp:String, nama:String): ResultState<Objects>

    suspend fun getKota(jsonrpc:String): ResultState<Objects>

}