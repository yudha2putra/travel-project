package com.tiomaz.trans.domain.entities

data class BannerEntity(
    val id:String,
    val name:String,
    val image:String,
    var isSelected:Boolean = false
)