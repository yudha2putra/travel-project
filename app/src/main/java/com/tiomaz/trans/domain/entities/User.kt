package com.tiomaz.trans.domain.entities

import android.content.Context
import android.os.Parcelable
import com.tiomaz.trans.R
import com.tiomaz.trans.domain.entities.request.UserRequest
import com.tiomaz.trans.utils.enum.AttachmentType
import com.tiomaz.trans.utils.extentions.emptyString
import com.tiomaz.trans.utils.validation.password
import kotlinx.android.parcel.Parcelize
import java.io.File

@Parcelize
data class User(
    var email: String= emptyString(),
    val id: Int = 0,
    var name: String= emptyString(),
    var phoneNumber: String= emptyString(),
    val status: String= emptyString(),
    val token: String= emptyString(),
    val agent: String= emptyString(),
    val totalTrip: Int= 0,
    var avatar:String = emptyString(),
    var address: String  = emptyString(),
    val addressAgent: String  = emptyString(),
    var plateNo: String  = emptyString(),
    var vehicle: String  = emptyString(),
    var password: String  = emptyString(),
    var vehicleDescription: String = emptyString(),
    var userPhoto:File = File(emptyString()),
    var isAgreeTnc:Boolean = false
) : Parcelable{

    fun toUserRequest():UserRequest{
        return UserRequest(
            email = email,
            name = name,
            phoneNumber = phoneNumber,
            address = address,
            password = password,
            plateNo = plateNo,
            vehicleDescription = vehicleDescription,
            vehicle = vehicle
        )
    }

}
