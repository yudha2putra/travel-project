package com.tiomaz.trans.domain.usecase.contract

import com.tiomaz.trans.domain.entities.Vehicle
import com.tiomaz.trans.utils.common.ResultState

interface MasterDataUseCase {
    suspend fun fetchVehicles(): ResultState<List<Vehicle>>
}