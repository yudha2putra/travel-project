package com.tiomaz.trans.domain.entities

data class LoketEntity(
    val id:Int,
    val name:String,
    val alamat:String,
    val telp:String
)