package com.tiomaz.trans.domain.entities

data class RegionalEntity(
    val id:Int,
    val regional:String
)