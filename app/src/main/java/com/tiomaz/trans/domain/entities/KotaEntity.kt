package com.tiomaz.trans.domain.entities

data class KotaEntity(
    val id:Int,
    val name:String,
    val latitude:String,
    val longitude:String,
    val isMain:String,
    val radius:String,
    val time:String,
)