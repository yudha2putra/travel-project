package com.tiomaz.trans.domain.repository

import com.tiomaz.trans.domain.entities.User
import com.tiomaz.trans.utils.common.ResultState
import java.util.*
import kotlin.collections.HashMap

interface MembershipRepository {

    fun isHasNetwork():Boolean

    fun isLogin():Boolean

    fun retrieveToken() : String

    fun isDeliveryProof():Boolean

    suspend fun postLogin(param: HashMap<String, String>) : ResultState<Objects>

    suspend fun postRegister(param:HashMap<String,String>): ResultState<Objects>

    suspend fun getKota(param:HashMap<String,String>): ResultState<Objects>
}