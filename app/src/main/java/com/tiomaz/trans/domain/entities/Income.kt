package com.tiomaz.trans.domain.entities

import android.os.Parcelable
import com.tiomaz.trans.base.BaseSelectionModel
import com.tiomaz.trans.utils.extentions.emptyString
import kotlinx.android.parcel.Parcelize

@Parcelize
open class Income(
    open val day:String = emptyString(),
    open val dayName:String = emptyString(),
    open val month:String = emptyString(),
    open val date:String = emptyString(),
    open val startPoint:String = emptyString(),
    open val rangeDate:String = emptyString(),
    open val totalIncome:String,
    open val totalCompletedTasks:String,
    open val totalRejectedTasks:String,
    val tasks:List<DailyIncome> = listOf(),
    override var isSelected: Boolean = false
): BaseSelectionModel(), Parcelable

@Parcelize
data class DailyIncome(
    override val day:String = emptyString(),
    override val dayName:String = emptyString(),
    override val month:String = emptyString(),
    override val date:String,
    override val totalIncome:String,
    override val totalCompletedTasks:String,
    override val totalRejectedTasks:String,
    override val startPoint:String = emptyString(),
    override val rangeDate:String = emptyString()
): Income(day, dayName, month, date, startPoint, rangeDate, totalIncome, totalCompletedTasks, totalRejectedTasks),
    Parcelable