package com.tiomaz.trans.domain.usecase.implementation

import com.tiomaz.trans.domain.entities.request.UserRequest
import com.tiomaz.trans.domain.entities.User
import com.tiomaz.trans.domain.repository.MembershipRepository
import com.tiomaz.trans.domain.usecase.contract.MembershipUseCase
import com.tiomaz.trans.utils.common.ResultState
import com.tiomaz.trans.utils.extentions.toRequestBody
import okhttp3.RequestBody
import java.util.*
import kotlin.collections.HashMap

class MembershipUseCaseImpl (private val repository: MembershipRepository):MembershipUseCase{

    override suspend fun postLogin(login: String, password: String, db:String): ResultState<Objects> {
        val param = HashMap<String, String>()
        param["login"] = login
        param["password"] = password
        param["db"] = db
        return repository.postLogin(param)
    }

    override suspend fun postRegister(email: String, nama:String, telp:String, password: String): ResultState<Objects> {
        val param = HashMap<String, String>()
        param["name"] = nama
        param["phone"] = telp
        param["email"] = email
        param["login"] = email
        param["password"] = password
        param["password_api"] = password

        return repository.postRegister(param)
    }

    override suspend fun getKota(jsonrpc: String): ResultState<Objects> {
        val param = HashMap<String, String>()
        param["jsonrpc"] = jsonrpc

        return repository.getKota(param)
    }


}