package com.tiomaz.trans.data.api.entities

import com.google.gson.annotations.SerializedName

data class ResultSourceApi<T>(
    @SerializedName("result")
    val result: T? = null
)