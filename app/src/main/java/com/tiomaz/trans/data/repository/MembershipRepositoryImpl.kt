package com.tiomaz.trans.data.repository

import android.content.Context
import com.tiomaz.trans.data.api.remote.MembershipApi
import com.tiomaz.trans.data.db.dao.CityDao
import com.tiomaz.trans.data.preference.KecipirPreference
import com.tiomaz.trans.domain.entities.KotaEntity
import com.tiomaz.trans.domain.entities.User
import com.tiomaz.trans.domain.repository.MembershipRepository
import com.tiomaz.trans.utils.common.ResultState
import com.tiomaz.trans.utils.constants.PreferenceKeys.KEY_IS_DELIVERY_PROOF
import com.tiomaz.trans.utils.constants.PreferenceKeys.KEY_LOGIN
import com.tiomaz.trans.utils.constants.PreferenceKeys.KEY_TOKEN
import com.tiomaz.trans.utils.extentions.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.HashMap

class MembershipRepositoryImpl(
    private val context: Context,
    private val api: MembershipApi,
    private val preference: KecipirPreference,
    private val dbCity: CityDao
) : MembershipRepository {

    override fun isHasNetwork(): Boolean {
        return hasNetwork(context)
    }

    override fun isLogin(): Boolean {
        return preference.getBoolean(KEY_LOGIN)
    }

    override fun retrieveToken(): String {
        return preference.getString(KEY_TOKEN)
    }

    override fun isDeliveryProof(): Boolean {
        return preference.getBoolean(KEY_IS_DELIVERY_PROOF)
    }


    override suspend fun postLogin(param: HashMap<String, String>): ResultState<Objects> {

        return try {
                val response = api.postLogin(param)
                if (response.isSuccessful) {
                    response.body()?.let {
                        withContext(Dispatchers.IO) {
                            handleApiSuccess(
                                jsonrpc = it.jsonrpc,
                                id = it.id,
                                result = it.result
                            )
                        }
                    } ?: handleApiError(response.errorBody())
                } else {
                    handleApiError(response.raw().code, response.errorBody())
                }

        } catch (e: Exception) {
            handleApiError(e)
        }
    }

    override suspend fun postRegister(param: HashMap<String, String>): ResultState<Objects> {
        return try {
            val response = api.postRegister(param)
            if (response.isSuccessful) {
                response.body()?.let {
                    withContext(Dispatchers.IO) {
                        handleApiSuccess(
                            jsonrpc = it.jsonrpc,
                            id = it.id,
                            result = it.result
                        )
                    }
                } ?: handleApiError(response.errorBody())
            } else {
                handleApiError(response.raw().code, response.errorBody())
            }
        } catch (e: Exception) {
            handleApiError(e)
        }
    }

    override suspend fun getKota(param: HashMap<String, String>): ResultState<Objects> {
        return try {
            val response = api.getKota(param)
            if (response.isSuccessful) {
                response.body()?.let {
                    withContext(Dispatchers.IO) {
                        handleApiSuccess(
                            jsonrpc = it.jsonrpc,
                            id = it.id,
                            result = it.result
                        )
                    }
                } ?: handleApiError(response.errorBody())
            } else {
                handleApiError(response.raw().code, response.errorBody())
            }
        } catch (e: Exception) {
            handleApiError(e)
        }
    }

}

