package com.tiomaz.trans.data.api.remote

import com.tiomaz.trans.base.BaseDataSourceApi
import retrofit2.Response
import retrofit2.http.*
import java.util.*


interface MembershipApi {

    @POST("web/session/authenticate")
    suspend fun postLogin(@Body param: HashMap<String, String>) : Response<BaseDataSourceApi>

    @POST("set_users")
    suspend fun postRegister(@Body param: HashMap<String, String>) : Response<BaseDataSourceApi>

    @POST("masterdata_list_locationkota")
    suspend fun getKota(@Body param: HashMap<String, String>) : Response<BaseDataSourceApi>
}