package com.tiomaz.trans.data.api.interceptor

import android.app.Application
import com.tiomaz.trans.BuildConfig
import com.tiomaz.trans.data.preference.KecipirPreference
import com.tiomaz.trans.utils.constants.PreferenceKeys
import com.tiomaz.trans.utils.extentions.hasNetwork
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.HashMap

class HeaderInterceptor(
    private val application: Application,
    val preference: KecipirPreference,
    val basicAuth: String
) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = mapHeaders(chain)
        return chain.proceed(request)
    }

    private fun mapHeaders(chain: Interceptor.Chain): Request {
        val original = chain.request()
        val requestBuilder = original.newBuilder()

        val token = preference.getString(PreferenceKeys.KEY_TOKEN)
        if (token.isNotEmpty()) {
            requestBuilder.addHeader("Authorization", "Bearer $token")
        }

        if (hasNetwork(application))
            requestBuilder.addHeader("Cache-Control", "public, max-age=" + 5).build()
        else
            requestBuilder.addHeader(
                "Cache-Control",
                "public, only-if-cached, max-stale=" + 60 * 60 * 2
            ).build()


//        for ((key, value) in headers) {
//            requestBuilder.addHeader(key, value)
//        }
        return requestBuilder.build()
    }


}