package com.tiomaz.trans.data.api.entities


import com.google.gson.annotations.SerializedName
import com.tiomaz.trans.domain.entities.DailyIncome
import com.tiomaz.trans.domain.entities.Income
import com.tiomaz.trans.utils.common.changeFormatStringDate

data class IncomeSourceApi(
    @SerializedName("subtitle")
    val subtitle: String,
    @SerializedName("task_cancel")
    val taskCancel: Int,
    @SerializedName("task_completed")
    val taskCompleted: Int,
    @SerializedName("title")
    val startPoint: String,
    @SerializedName("title_top")
    val titleTop: String,
    @SerializedName("week_detail")
    val weekDetail: List<DailyIncomeSourceApi>,
    @SerializedName("week_value")
    val weekValue: String
){

    fun toIncome():Income{
        return Income(
            day = changeFormatStringDate(
                date = titleTop,
                formatNew = "dd",
                formatOld = "MMM dd EEEE"
            ),
            dayName = changeFormatStringDate(
                date = titleTop,
                formatNew = "EEEE",
                formatOld = "MMM dd EEEE"
            ),
            month = changeFormatStringDate(
                date = titleTop,
                formatNew = "MMM",
                formatOld = "MMM dd EEEE"
            ),
            totalRejectedTasks = taskCancel.toString(),
            totalCompletedTasks = taskCompleted.toString(),
            totalIncome = weekValue,
            rangeDate = subtitle,
            startPoint = startPoint,
            tasks = weekDetail.map { it.toDailyIncome() }
        )
    }
}

data class DailyIncomeSourceApi(
    @SerializedName("task_success")
    val taskSuccess: Int,
    @SerializedName("daily_value")
    val dailyValue: String,
    @SerializedName("task_cancel")
    val taskCancel: Int,
    @SerializedName("task_date")
    val taskDate: String
){

    fun toDailyIncome():DailyIncome{
        return DailyIncome(
            date = taskDate,
            totalRejectedTasks = taskCancel.toString(),
            totalCompletedTasks = taskSuccess.toString(),
            totalIncome = dailyValue
        )
    }

}