package com.tiomaz.trans.data.api.entities


import com.google.gson.annotations.SerializedName
import com.tiomaz.trans.domain.entities.User
import com.tiomaz.trans.utils.enum.AttachmentType

data class UserIdSourceApi(
    @SerializedName("id")
    val id: Int? = null
)

data class UserSourceApi(
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("phone_number")
    val phoneNumber: String? = null
){

    fun toUser():User{
        return User(
            email = email.orEmpty(),
            id = id ?: 0,
            name = name.orEmpty(),
            phoneNumber = phoneNumber.orEmpty()
        )
    }

}