package com.tiomaz.trans.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.tiomaz.trans.data.db.dao.CityDao
import com.tiomaz.trans.data.db.entities.CityEntity
import com.tiomaz.trans.utils.constants.AppConstants.DATABASE_NAME

@Database(
    entities = [CityEntity::class],
    version = 1,
    exportSchema = false
)
abstract class KecipirDatabase: RoomDatabase() {
    companion object {
        @Volatile
        private var INSTANCE: KecipirDatabase? = null

        fun database(context: Context): KecipirDatabase {
            context.let {
                return INSTANCE ?: synchronized(this) {
                    val instance = Room.databaseBuilder(
                        context.applicationContext,
                        KecipirDatabase::class.java,
                        DATABASE_NAME
                    )
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance
                    instance
                }
            }
        }
    }

    abstract fun orderDao():CityDao
}