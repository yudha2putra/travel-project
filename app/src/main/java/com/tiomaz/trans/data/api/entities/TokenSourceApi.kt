package com.tiomaz.trans.data.api.entities

import com.google.gson.annotations.SerializedName

data class TokenSourceApi(
    @SerializedName("token")
    val token:String
)