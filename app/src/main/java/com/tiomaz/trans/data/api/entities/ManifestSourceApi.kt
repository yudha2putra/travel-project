package com.tiomaz.trans.data.api.entities


import com.google.gson.annotations.SerializedName
import com.tiomaz.trans.domain.entities.Manifest

data class ManifestSourceApi(
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("qty")
    val qty: Int?
){

    fun toManifest():Manifest{
        return Manifest(
            icon = icon.orEmpty(),
            name = name.orEmpty(),
            qty = qty ?: 0
        )
    }
}