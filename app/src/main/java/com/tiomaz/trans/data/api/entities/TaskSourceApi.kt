package com.tiomaz.trans.data.api.entities


import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import com.tiomaz.trans.domain.entities.Task
import com.tiomaz.trans.utils.enum.AddressStatusType

data class TaskSourceApi(
    @SerializedName("address_detail")
    val addressDetail: String? = null,
    @SerializedName("address_gps")
    val addressLocation: String? = null,
    @SerializedName("address_note")
    val note: String? = null,
    @SerializedName("address_distance")
    val addressDistance: String? = null,
    @SerializedName("address_verified")
    val addressVerified: String? = null,
    @SerializedName("id_task")
    val idTask: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("invoice")
    val invoice: String? = null,
    @SerializedName("manifest")
    val manifestSourceApi: List<ManifestSourceApi>? = null,
    @SerializedName("member_name")
    val memberName: String? = null,
    @SerializedName("member_phone")
    val memberPhone: String? = null,
    @SerializedName("parking_value")
    val parkingValue: String? = null,
    @SerializedName("penalty_value")
    val penaltyValue: String? = null,
    @SerializedName("task_date")
    val taskDate: String? = null,
    @SerializedName("task_photo")
    val taskPhoto: String? = null,
    @SerializedName("task_status")
    val taskStatus: String? = null,
    @SerializedName("task_type")
    val taskType: String? = null,
    @SerializedName("task_value")
    val taskValue: String? = null,
    @SerializedName("total_task_value")
    val totalTaskValue: String? = null,
    @SerializedName("address_status")
    val addressStatus: String? = null,
    @SerializedName("parking_photo")
    val parkingTicketPhoto: String? = null,
    @SerializedName("time_completed")
    val timeCompleted: String? = null
){

    private fun getLocation():LatLng{
        val location = addressLocation?.split(",")?.toTypedArray()
        return LatLng(
            location?.get(0)?.toDouble() ?: 0.0,
            location?.get(1)?.toDouble() ?: 0.0
        )
    }

    fun toTask():Task{
        return Task(
            address =  if(addressDetail.isNullOrEmpty()) "-" else addressDetail,
            distance =  if(addressDistance.isNullOrEmpty()) "-" else addressDistance,
            memberName =  if(memberName.isNullOrEmpty()) "-" else memberName,
            taskType =  if(taskType.isNullOrEmpty()) "-" else taskType,
            rate = taskValue ?: "0",
            id = idTask ?: 0,
            addressVerified =  if(addressVerified.isNullOrEmpty()) "-" else addressVerified,
            isAddressVerified = addressStatus == AddressStatusType.VERIFIED.type,
            status = taskStatus.orEmpty(),
            memberPhoneNumber =  if(memberPhone.isNullOrEmpty()) "-" else memberPhone,
            note = if(note.isNullOrEmpty()) "-" else note,
            invoice = invoice.orEmpty(),
            manifest = manifestSourceApi?.map { it.toManifest() }.orEmpty(),
            parkingValue = parkingValue.orEmpty(),
            penaltyValue = penaltyValue.orEmpty(),
            taskDate = taskDate.orEmpty(),
            taskPhoto = taskPhoto.orEmpty(),
            totalRate = totalTaskValue.orEmpty(),
            destinationLocation = getLocation(),
            parkTicketPhoto = parkingTicketPhoto.orEmpty(),
            timeCompleted =  if(timeCompleted.isNullOrEmpty()) "-" else timeCompleted
        )
    }
}