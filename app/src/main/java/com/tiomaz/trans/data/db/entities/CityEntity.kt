package com.tiomaz.trans.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.tiomaz.trans.domain.entities.User

@Entity(tableName = "city")
data class CityEntity(
    @PrimaryKey
    val id:Int,
    val name:String
){

    fun toCity():User{
        return User(
            id = id,
            name = name
        )
    }
}