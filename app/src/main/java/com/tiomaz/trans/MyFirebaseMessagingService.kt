package com.tiomaz.trans

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.tiomaz.trans.common.VolleySingleton
import com.tiomaz.trans.utils.extentions.makeGone
import com.tiomaz.trans.utils.extentions.makeVisible
import io.reactivex.internal.fuseable.QueueFuseable
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import java.nio.charset.StandardCharsets


class MyFirebaseMessagingService : FirebaseMessagingService() {

    val FIREBASE = "FirebaseService"
    private var count = 0

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        getSharedPreferences("PREFERENCE_NAME", MODE_PRIVATE).edit().putString("FirebaseToken", token).apply();
        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
        val uid = sharedPreference?.getInt("uid",0)
        val fb = sharedPreference?.getString("FirebaseToken","")
        if (uid!=0 && fb!=""){
            doFirebase()
        }
        Log.d(FIREBASE, "FirebaseToken: $token")

    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        Log.d(FIREBASE, "From: ${remoteMessage.from}")

        if (remoteMessage.data.isNotEmpty()) {
            Log.d(FIREBASE, "Message data payload: ${remoteMessage.data}")
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
//            sendNotification(it.title,it.body,remoteMessage.data.get("qr"))
            Log.d(FIREBASE, "Message Notification Body: ${remoteMessage.data.get("qr")}")

        }
    }

//    private fun sendNotification(title: String?, messageBody: String?, qr: String?) {
//        val intent = Intent(applicationContext, MapsActivity::class.java)
//        //you can use your launcher Activity insted of SplashActivity, But if the Activity you used here is not launcher Activty than its not work when App is in background.
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        intent.setAction((Constants.ACTIVITY_STUFF))
//        intent.putExtra(Constants.MESSAGE,qr)
//        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
//
//        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
//        val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//        val mNotifyManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//        //For Android Version Orio and greater than orio.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val importance = NotificationManager.IMPORTANCE_LOW
//            val mChannel = NotificationChannel("Sesame", "Sesame", importance)
//            mChannel.description = messageBody
//            mChannel.enableLights(true)
//            mChannel.lightColor = Color.RED
//            mChannel.enableVibration(true)
//            mChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
//            mNotifyManager.createNotificationChannel(mChannel)
//        }
//        //For Android Version lower than orio.
//        val mBuilder = NotificationCompat.Builder(this, "Seasame")
//        mBuilder.setContentTitle(title)
//            .setContentText(messageBody)
//            .setSmallIcon(R.mipmap.ic_launcher)
//            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
//            .setAutoCancel(true)
//            .setSound(defaultSoundUri)
//            .setColor(Color.parseColor("#FFD600"))
//            .setContentIntent(pendingIntent)
//            .setChannelId("Sesame")
//            .setPriority(NotificationCompat.PRIORITY_LOW)
//
//        mNotifyManager.notify(count, mBuilder.build())
//        count++
//    }

    private fun doFirebase() {
        val sharedPreference = this.getSharedPreferences("PREFERENCE_NAME",Context.MODE_PRIVATE)
        val uid = sharedPreference?.getInt("uid",0)
        val fb = sharedPreference?.getString("FirebaseToken","")
        val url = "https://system.tiomaz.com/token_holder"
        val params = JSONObject()
        params.put("user_id",uid)
        params.put("token_holder", fb)
        Log.d("print",params.toString())

        val request = JsonObjectRequest(
            Request.Method.POST,url,params,
            { response ->
                // Process the json
                try {
                    Log.d("print",response.toString())
                }catch (e:Exception){
                    Log.d("print",e.toString())
                }

            }, {
                // Error in request
                Log.d("print",it.toString())
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,0,1f)

        // Add the volley post request to the request queue
        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }



    private fun createPostBody(params: Map<String, String?>): String {
        val sbPost = StringBuilder()
        for (key in params.keys) {
            if (params[key] != null) {
                sbPost.append(
                    """
                        
                        --${QueueFuseable.BOUNDARY}
                        
                        """.trimIndent()
                )
                sbPost.append("Content-Disposition: form-data; name=\"$key\"\r\n\r\n")
                sbPost.append(params[key])
            }
        }
        return sbPost.toString()
    }

    object Constants {
        const val ACTIVITY_STUFF = "push_notification"
        const val MESSAGE = "message"
    }

}